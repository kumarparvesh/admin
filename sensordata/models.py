from django.db import models
from users.models import User
from trip.models import Trip
from django.utils import timezone


# Create your models here.

class Sensordata(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE
    )
    # trip = models.ForeignKey(
    #     Trip, on_delete=models.CASCADE,
    #     null=True
    # )
    trip_id = models.IntegerField(default=0)
    device_id = models.CharField(max_length=255)
    latitude = models.CharField(max_length=255)
    longitude = models.CharField(max_length=255)
    acc_x = models.CharField(max_length=255)
    acc_y = models.CharField(max_length=255)
    acc_z = models.CharField(max_length=255)
    acc_magnitude = models.DecimalField(max_digits=7, decimal_places=2, default=0)
    gyro_x = models.CharField(max_length=255, default=0)
    gyro_y = models.CharField(max_length=255, default=0)
    gyro_z = models.CharField(max_length=255, default=0)
    magneto_x = models.CharField(max_length=255, default=0)
    magneto_y = models.CharField(max_length=255, default=0)
    magneto_z = models.CharField(max_length=255, default=0)
    bearing = models.CharField(max_length=255, default=0)
    speed = models.CharField(max_length=255)
    altitude = models.CharField(max_length=255)
    timeInMillis = models.CharField(max_length=255)
    accuracy = models.CharField(max_length=255)
    type = models.CharField(max_length=255)
    is_user_device = models.CharField(max_length=255, default=0)
    created_at = models.DateTimeField(default=timezone.now)
