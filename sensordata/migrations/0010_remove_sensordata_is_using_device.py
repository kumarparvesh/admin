# Generated by Django 2.1.3 on 2019-09-16 15:56

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sensordata', '0009_sensordata_is_using_device'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sensordata',
            name='is_using_device',
        ),
    ]
