from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, permission_classes
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_200_OK,
)
from rest_framework.response import Response


# Create your views here.
@csrf_exempt
@api_view(["POST"])
def heart_beat(request):
    latitude = request.data.get("latitude", '')
    longitude = request.data.get("longitude", '')
    speed = request.data.get("speed", '')
    timestamp = request.data.get("timestamp", '')

    if not latitude:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter Valid Latitude"
            },
            status=HTTP_400_BAD_REQUEST
        )
    if not longitude:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter Valid Longitude"
            },
            status=HTTP_400_BAD_REQUEST
        )
    if not speed:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter Valid Speed"
            },
            status=HTTP_400_BAD_REQUEST
        )
    if not timestamp:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter Current Timestamp"
            },
            status=HTTP_400_BAD_REQUEST
        )

    return Response(
        {
            "Code": HTTP_200_OK,
            "status": "New Trip",
            "message": "New Trip Detected",
            "data": request.data
        },
        status=HTTP_200_OK
    )
