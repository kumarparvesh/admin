-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 07, 2019 at 03:32 PM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbehaviour`
--

-- --------------------------------------------------------

--
-- Table structure for table `authtoken_token`
--

CREATE TABLE `authtoken_token` (
  `key` varchar(40) NOT NULL,
  `created` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authtoken_token`
--

INSERT INTO `authtoken_token` (`key`, `created`, `user_id`) VALUES
('27b905d773b71145b21218223644c088c9314a2d', '2019-01-30 06:09:03.924238', 2);

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add content type', 4, 'add_contenttype'),
(14, 'Can change content type', 4, 'change_contenttype'),
(15, 'Can delete content type', 4, 'delete_contenttype'),
(16, 'Can view content type', 4, 'view_contenttype'),
(17, 'Can add session', 5, 'add_session'),
(18, 'Can change session', 5, 'change_session'),
(19, 'Can delete session', 5, 'delete_session'),
(20, 'Can view session', 5, 'view_session'),
(21, 'Can add Token', 6, 'add_token'),
(22, 'Can change Token', 6, 'change_token'),
(23, 'Can delete Token', 6, 'delete_token'),
(24, 'Can view Token', 6, 'view_token'),
(25, 'Can add companies', 7, 'add_companies'),
(26, 'Can change companies', 7, 'change_companies'),
(27, 'Can delete companies', 7, 'delete_companies'),
(28, 'Can view companies', 7, 'view_companies'),
(29, 'Can add user', 8, 'add_user'),
(30, 'Can change user', 8, 'change_user'),
(31, 'Can delete user', 8, 'delete_user'),
(32, 'Can view user', 8, 'view_user'),
(33, 'Can add trip', 9, 'add_trip'),
(34, 'Can change trip', 9, 'change_trip'),
(35, 'Can delete trip', 9, 'delete_trip'),
(36, 'Can view trip', 9, 'view_trip'),
(37, 'Can add sensordata', 10, 'add_sensordata'),
(38, 'Can change sensordata', 10, 'change_sensordata'),
(39, 'Can delete sensordata', 10, 'delete_sensordata'),
(40, 'Can view sensordata', 10, 'view_sensordata'),
(41, 'Can add social', 11, 'add_social'),
(42, 'Can change social', 11, 'change_social'),
(43, 'Can delete social', 11, 'delete_social'),
(44, 'Can view social', 11, 'view_social'),
(45, 'Can add digi', 12, 'add_digi'),
(46, 'Can change digi', 12, 'change_digi'),
(47, 'Can delete digi', 12, 'delete_digi'),
(48, 'Can view digi', 12, 'view_digi');

-- --------------------------------------------------------

--
-- Table structure for table `companies_companies`
--

CREATE TABLE `companies_companies` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created_date` datetime(6) NOT NULL,
  `published_date` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies_companies`
--

INSERT INTO `companies_companies` (`id`, `name`, `created_date`, `published_date`) VALUES
(1, 'Ryku Insurence', '2019-01-30 06:05:45.000000', '2019-01-30 06:05:52.000000');

-- --------------------------------------------------------

--
-- Table structure for table `digi_digi`
--

CREATE TABLE `digi_digi` (
  `id` int(11) NOT NULL,
  `lat` varchar(20) NOT NULL,
  `long` varchar(20) NOT NULL,
  `type` varchar(40) NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `digi_digi`
--

INSERT INTO `digi_digi` (`id`, `lat`, `long`, `type`, `user_id`) VALUES
(1, '1213121', '1213121', '1213121', 2);

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2019-01-30 06:05:54.190249', '1', 'Ryku Insurence', 1, '[{\"added\": {}}]', 7, 1),
(2, '2019-01-30 06:06:39.166476', '1', 'driver', 2, '[{\"changed\": {\"fields\": [\"first_name\", \"last_name\", \"mobile\", \"avatar\", \"company\"]}}]', 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(6, 'authtoken', 'token'),
(7, 'companies', 'companies'),
(4, 'contenttypes', 'contenttype'),
(12, 'digi', 'digi'),
(10, 'sensordata', 'sensordata'),
(5, 'sessions', 'session'),
(11, 'social', 'social'),
(9, 'trip', 'trip'),
(8, 'users', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'companies', '0001_initial', '2019-01-30 05:59:57.575200'),
(2, 'contenttypes', '0001_initial', '2019-01-30 05:59:58.234441'),
(3, 'contenttypes', '0002_remove_content_type_name', '2019-01-30 05:59:59.424476'),
(4, 'auth', '0001_initial', '2019-01-30 06:00:03.835147'),
(5, 'auth', '0002_alter_permission_name_max_length', '2019-01-30 06:00:03.961037'),
(6, 'auth', '0003_alter_user_email_max_length', '2019-01-30 06:00:04.006488'),
(7, 'auth', '0004_alter_user_username_opts', '2019-01-30 06:00:04.048413'),
(8, 'auth', '0005_alter_user_last_login_null', '2019-01-30 06:00:04.093778'),
(9, 'auth', '0006_require_contenttypes_0002', '2019-01-30 06:00:04.136639'),
(10, 'auth', '0007_alter_validators_add_error_messages', '2019-01-30 06:00:04.181867'),
(11, 'auth', '0008_alter_user_username_max_length', '2019-01-30 06:00:04.223658'),
(12, 'auth', '0009_alter_user_last_name_max_length', '2019-01-30 06:00:04.267288'),
(13, 'users', '0001_initial', '2019-01-30 06:00:10.488708'),
(14, 'admin', '0001_initial', '2019-01-30 06:00:12.923072'),
(15, 'admin', '0002_logentry_remove_auto_add', '2019-01-30 06:00:12.979184'),
(16, 'admin', '0003_logentry_add_action_flag_choices', '2019-01-30 06:00:13.046178'),
(17, 'authtoken', '0001_initial', '2019-01-30 06:00:14.354157'),
(18, 'authtoken', '0002_auto_20160226_1747', '2019-01-30 06:00:15.258255'),
(19, 'trip', '0001_initial', '2019-01-30 06:00:16.406013'),
(20, 'trip', '0002_auto_20190117_1155', '2019-01-30 06:00:17.183329'),
(21, 'trip', '0003_auto_20190125_0850', '2019-01-30 06:00:22.991615'),
(22, 'trip', '0004_trip_finished', '2019-01-30 06:00:23.853463'),
(23, 'sensordata', '0001_initial', '2019-01-30 06:00:26.103678'),
(24, 'sensordata', '0002_auto_20190130_0535', '2019-01-30 06:00:33.968760'),
(25, 'sessions', '0001_initial', '2019-01-30 06:00:34.631488'),
(26, 'users', '0002_auto_20181128_0653', '2019-01-30 06:00:36.739686'),
(27, 'users', '0003_user_avatar', '2019-01-30 06:00:37.718286'),
(28, 'users', '0004_auto_20190125_0852', '2019-01-30 06:00:39.024407'),
(29, 'trip', '0005_auto_20190207_1212', '2019-02-07 06:42:24.616636'),
(30, 'trip', '0006_trip_status', '2019-02-07 07:03:34.110282'),
(31, 'trip', '0007_remove_trip_driving_score', '2019-02-07 07:14:03.257969'),
(32, 'sensordata', '0003_sensordata_acc_magnitude', '2019-02-07 07:30:45.112586'),
(33, 'social', '0001_initial', '2019-02-13 10:54:01.820034'),
(34, 'social', '0002_auto_20190213_1053', '2019-02-13 10:54:04.397958'),
(35, 'trip', '0008_auto_20190213_1512', '2019-02-13 15:12:39.238705'),
(36, 'trip', '0009_auto_20190214_1123', '2019-02-14 11:23:38.701310'),
(37, 'sensordata', '0004_auto_20190214_1136', '2019-02-14 11:36:32.554093'),
(38, 'trip', '0010_auto_20190214_1529', '2019-02-14 15:29:32.194607'),
(39, 'social', '0003_auto_20190214_1612', '2019-02-14 16:12:20.616594'),
(40, 'users', '0005_auto_20190301_1411', '2019-03-01 14:11:25.365445'),
(41, 'users', '0006_auto_20190301_1540', '2019-03-01 15:41:04.481882'),
(42, 'trip', '0011_auto_20190305_1701', '2019-03-05 17:02:10.790224'),
(43, 'digi', '0001_initial', '2019-03-07 12:02:05.988379'),
(44, 'digi', '0002_auto_20190308_1428', '2019-03-08 14:28:41.951707'),
(45, 'users', '0007_user_reset_token', '2019-03-28 09:13:13.756797'),
(46, 'trip', '0012_auto_20190415_1113', '2019-04-15 11:14:12.128710');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('a8004df8q75j0lmcqgjyetuwkcvc20f2', 'NDBlNmNlNTE3ZWRiOWU0ZjlmNTdjNGZhNzEwYmU4NzgzYWEyOTczOTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2Y2I1MjA4ZjE1N2E4YjQyYWQ0NDhhOThmODBjYzA0NjY0NzJlOTA1In0=', '2019-02-20 08:49:36.642961'),
('nc9snt5vosq6imb0sfnggve6pifivzzk', 'NDBlNmNlNTE3ZWRiOWU0ZjlmNTdjNGZhNzEwYmU4NzgzYWEyOTczOTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2Y2I1MjA4ZjE1N2E4YjQyYWQ0NDhhOThmODBjYzA0NjY0NzJlOTA1In0=', '2019-03-07 18:10:48.803344'),
('wk7yomotfsrirdlxcyu3z7kxu01v1wh7', 'NDBlNmNlNTE3ZWRiOWU0ZjlmNTdjNGZhNzEwYmU4NzgzYWEyOTczOTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2Y2I1MjA4ZjE1N2E4YjQyYWQ0NDhhOThmODBjYzA0NjY0NzJlOTA1In0=', '2019-03-06 15:17:10.364713');

-- --------------------------------------------------------

--
-- Table structure for table `sensordata_sensordata`
--

CREATE TABLE `sensordata_sensordata` (
  `id` int(11) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `acc_x` varchar(255) NOT NULL,
  `acc_y` varchar(255) NOT NULL,
  `acc_z` varchar(255) NOT NULL,
  `speed` varchar(255) NOT NULL,
  `altitude` varchar(255) NOT NULL,
  `timeInMillis` varchar(255) NOT NULL,
  `accuracy` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  `gyro_x` varchar(255) NOT NULL,
  `gyro_y` varchar(255) NOT NULL,
  `gyro_z` varchar(255) NOT NULL,
  `magneto_x` varchar(255) NOT NULL,
  `magneto_y` varchar(255) NOT NULL,
  `magneto_z` varchar(255) NOT NULL,
  `trip_id` int(11) DEFAULT NULL,
  `acc_magnitude` varchar(255) NOT NULL,
  `usertype` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sensordata_sensordata`
--

INSERT INTO `sensordata_sensordata` (`id`, `device_id`, `latitude`, `longitude`, `acc_x`, `acc_y`, `acc_z`, `speed`, `altitude`, `timeInMillis`, `accuracy`, `type`, `created_at`, `user_id`, `gyro_x`, `gyro_y`, `gyro_z`, `magneto_x`, `magneto_y`, `magneto_z`, `trip_id`, `acc_magnitude`, `usertype`) VALUES
(2, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-30 15:03:56.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 14, '0', ''),
(3, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-30 15:04:45.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(4, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-30 15:05:39.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(5, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-30 15:05:52.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(6, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-30 15:05:53.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(7, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-30 15:05:54.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(8, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-30 15:05:54.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(9, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-30 15:05:55.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(10, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '22', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-30 15:05:55.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(11, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-30 15:05:55.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(12, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-30 15:05:55.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(13, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-30 15:05:55.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(14, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-30 15:05:56.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(15, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-30 15:05:56.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(16, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-30 15:05:56.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(17, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-30 15:05:56.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(18, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-30 15:05:56.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(19, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-30 15:05:56.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(20, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-30 15:06:54.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(21, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-30 15:07:20.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(22, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-31 00:00:00.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(23, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-31 11:10:34.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(24, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-31 11:11:23.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(25, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-31 11:12:38.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(26, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-01-31 11:13:15.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 13, '0', ''),
(30, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-06-06 18:22:10.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 6, '52', 'Driver'),
(31, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-06-06 18:22:42.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 6, '52', 'Driver'),
(32, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-06-06 18:26:35.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 6, '52', 'Driver'),
(33, '1232215', '10', '20', '124.5454', '12544.23', '1.2121354', '5', '10', '5', '100', '6', '2019-06-06 19:13:14.381853', 2, '1.2131', '1.122121', '1.23564656', '10', '5', '3', 40, '11', 'Passenger'),
(34, '1232215', '10', '20', '124.5454', '12544.23', '1.2121354', '5', '10', '5', '100', '6', '2019-06-06 19:13:14.435866', 2, '1.2131', '1.122121', '1.23564656', '10', '5', '3', 40, '11', 'Passenger'),
(35, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-06-06 20:45:50.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 16, '52', 'Driver'),
(36, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-06-06 20:52:18.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 16, '52', 'Driver'),
(37, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-06-06 20:53:56.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 16, '52', 'Driver'),
(38, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-06-06 20:55:31.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 16, '52', 'Driver'),
(39, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-06-06 21:05:24.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 16, '52', 'Driver'),
(40, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-06-06 21:05:26.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 16, '52', 'Driver'),
(42, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-06-06 21:06:29.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 16, '52', 'Driver'),
(45, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-06-06 22:32:29.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 30, '52', 'Driver'),
(46, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-06-06 22:34:57.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 31, '52', 'Driver'),
(47, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-06-06 22:35:01.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 31, '52', 'Driver'),
(48, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-06-06 22:53:25.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 47, '52', 'Driver'),
(49, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-06-06 23:23:34.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 48, '52', 'Driver'),
(50, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-06-06 23:23:39.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 47, '52', 'Driver'),
(51, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-06-06 23:24:05.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 47, '52', 'Driver'),
(52, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-06-06 23:24:15.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 48, '52', 'Driver'),
(53, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-06-06 23:26:58.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 47, '52', 'Driver'),
(54, '1232215', '30.6940171', '76.7091051', '0.7905884', '9.9722595', '-0.22546387', '18.88', '262.50091552734375', '1542793633721', '7.504', 'Moving', '2019-06-06 23:28:10.000000', 2, '0.07998657', '0.083618164', '0.039871216', '-14.756775', '-11.843872', '31.983948', 48, '52', 'Driver'),
(55, '1232215', '10', '20', '124.5454', '12544.23', '1.2121354', '5', '10', '5', '100', '6', '2019-06-06 23:49:51.659479', 2, '1.2131', '1.122121', '1.23564656', '10', '5', '3', 0, '11', 'Passenger'),
(56, '1232215', '10', '20', '124.5454', '12544.23', '1.2121354', '5', '10', '5', '100', '6', '2019-06-06 23:49:51.699140', 2, '1.2131', '1.122121', '1.23564656', '10', '5', '3', 0, '11', 'Passenger');

-- --------------------------------------------------------

--
-- Table structure for table `social_social`
--

CREATE TABLE `social_social` (
  `id` int(11) NOT NULL,
  `status` varchar(10) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `invitation` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_social`
--

INSERT INTO `social_social` (`id`, `status`, `user_id`, `invitation`) VALUES
(1, 'pending', 2, '9041000001'),
(2, 'pending', 2, '9041638284'),
(3, 'pending', 2, '9041638284'),
(4, 'pending', 2, '9041638284'),
(5, 'pending', 2, '9041638284');

-- --------------------------------------------------------

--
-- Table structure for table `trip_trip`
--

CREATE TABLE `trip_trip` (
  `id` int(11) NOT NULL,
  `trip_start_time` datetime(6) DEFAULT NULL,
  `trip_end_time` datetime(6) DEFAULT NULL,
  `distance` decimal(7,2) NOT NULL,
  `duration` varchar(10) NOT NULL,
  `time_in_traffic` varchar(10) NOT NULL,
  `created_date` datetime(6) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `excellent_driving` decimal(5,2) NOT NULL,
  `fair_driving` decimal(5,2) NOT NULL,
  `risky_driving` decimal(5,2) NOT NULL,
  `speed_avg` decimal(5,2) NOT NULL,
  `speed_max` decimal(5,2) NOT NULL,
  `speed_min` decimal(5,2) NOT NULL,
  `finished` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `acceleration_avg` decimal(5,2) NOT NULL,
  `acceleration_max` decimal(5,2) NOT NULL,
  `hard_acceleration` decimal(5,2) NOT NULL,
  `hard_braking` decimal(5,2) NOT NULL,
  `over_speeding` decimal(5,2) NOT NULL,
  `phone_distraction` decimal(5,2) NOT NULL,
  `road_slope` decimal(5,2) NOT NULL,
  `road_type` varchar(40) NOT NULL,
  `acceleration` decimal(5,2) NOT NULL,
  `highway` decimal(5,2) NOT NULL,
  `lane_change` decimal(5,2) NOT NULL,
  `non_highway_inside_city` decimal(5,2) NOT NULL,
  `non_highway_outside_city` decimal(5,2) NOT NULL,
  `rural` decimal(5,2) NOT NULL,
  `turns` decimal(5,2) NOT NULL,
  `frequent_braking` decimal(5,2) NOT NULL,
  `left_turns` decimal(5,2) NOT NULL,
  `rainy_curve` decimal(5,2) NOT NULL,
  `right_turns` decimal(5,2) NOT NULL,
  `speed_limit` decimal(5,2) NOT NULL,
  `speed_too_high_curve` decimal(5,2) NOT NULL,
  `flag` varchar(20) NOT NULL,
  `usertype` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trip_trip`
--

INSERT INTO `trip_trip` (`id`, `trip_start_time`, `trip_end_time`, `distance`, `duration`, `time_in_traffic`, `created_date`, `user_id`, `excellent_driving`, `fair_driving`, `risky_driving`, `speed_avg`, `speed_max`, `speed_min`, `finished`, `status`, `acceleration_avg`, `acceleration_max`, `hard_acceleration`, `hard_braking`, `over_speeding`, `phone_distraction`, `road_slope`, `road_type`, `acceleration`, `highway`, `lane_change`, `non_highway_inside_city`, `non_highway_outside_city`, `rural`, `turns`, `frequent_braking`, `left_turns`, `rainy_curve`, `right_turns`, `speed_limit`, `speed_too_high_curve`, `flag`, `usertype`) VALUES
(14, '2019-01-31 15:29:06.000000', '2019-01-31 15:33:43.000000', '5.23', '0:04:37', '0:15:00', '2019-02-04 15:29:06.000000', 2, '0.00', '0.00', '100.00', '18.88', '18.88', '0.00', 1, 1, '0.00', '0.00', '0.00', '100.00', '0.00', '0.00', '262.50', 'District Road', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', ''),
(15, '2019-02-07 14:51:13.000000', '2019-02-12 00:00:00.000000', '138.30', '1:55:15', '0:00:00', '2019-02-07 14:51:13.000000', 2, '80.00', '10.00', '10.00', '20.00', '56.00', '0.00', 1, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'District Road', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', ''),
(16, '2019-06-06 18:30:00.000000', '2018-11-21 15:17:13.000000', '0.00', '0', '0:00:00', '2019-06-06 05:35:59.445604', 2, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'District Road', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', ''),
(48, '2019-06-06 23:27:31.000000', NULL, '0.00', '0', '0', '2019-06-06 23:27:31.000000', 2, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'District Road', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Driver');

-- --------------------------------------------------------

--
-- Table structure for table `users_user`
--

CREATE TABLE `users_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(100) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  `mobile` varchar(16) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `distance` double NOT NULL,
  `score` double NOT NULL,
  `styled` varchar(16) NOT NULL,
  `e_email` varchar(255) NOT NULL,
  `e_number` varchar(10) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_email_password` varchar(255) NOT NULL,
  `wifi_id` varchar(255) NOT NULL,
  `wifi_password` varchar(255) NOT NULL,
  `reset_token` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_user`
--

INSERT INTO `users_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`, `mobile`, `company_id`, `avatar`, `distance`, `score`, `styled`, `e_email`, `e_number`, `user_email`, `user_email_password`, `wifi_id`, `wifi_password`, `reset_token`) VALUES
(1, 'pbkdf2_sha256$120000$2TUj1ZavxINB$A0e/LxSex7G5L2mMemKiRFjdlnmmx48iJz3nkEew1Pc=', '2019-02-21 18:10:48.777114', 1, 'driver', 'Akhilesh', 'Singh', 'driver@gmail.com', 1, 1, '2019-01-30 06:04:18.000000', '9041638284', 1, 'images/avatar1.png', 0, 0, '', '', '', '', '', '', '', ''),
(2, 'pbkdf2_sha256$120000$2TUj1ZavxINB$A0e/LxSex7G5L2mMemKiRFjdlnmmx48iJz3nkEew1Pc=', NULL, 0, 'A24', '', '', 'a19@gmail.com', 0, 1, '2019-01-30 06:08:16.855788', '9041000001', 1, 'images/9041000001profile_pic_yFq4xI3.jpeg', 0, 68.75, 'Fair', 'abc@gmail.com', '9041638284', 'abc@gmail.com', 'abcdf', 'mywifi', 'xyz', '');

-- --------------------------------------------------------

--
-- Table structure for table `users_user_groups`
--

CREATE TABLE `users_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users_user_user_permissions`
--

CREATE TABLE `users_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authtoken_token`
--
ALTER TABLE `authtoken_token`
  ADD PRIMARY KEY (`key`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `companies_companies`
--
ALTER TABLE `companies_companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `digi_digi`
--
ALTER TABLE `digi_digi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `digi_digi_user_id_2d9aade9_fk_users_user_id` (`user_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_users_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `sensordata_sensordata`
--
ALTER TABLE `sensordata_sensordata`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sensordata_sensordata_user_id_75669da7_fk_users_user_id` (`user_id`),
  ADD KEY `sensordata_sensordata_trip_id_29af71ab_fk_trip_trip_id` (`trip_id`);

--
-- Indexes for table `social_social`
--
ALTER TABLE `social_social`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_social_user_id_94d6c7e2_fk_users_user_id` (`user_id`);

--
-- Indexes for table `trip_trip`
--
ALTER TABLE `trip_trip`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trip_trip_user_id_75fa8286_fk_users_user_id` (`user_id`);

--
-- Indexes for table `users_user`
--
ALTER TABLE `users_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `mobile` (`mobile`),
  ADD UNIQUE KEY `users_user_email_243f6e77_uniq` (`email`),
  ADD KEY `users_user_company_id_14799323_fk_companies_companies_id` (`company_id`);

--
-- Indexes for table `users_user_groups`
--
ALTER TABLE `users_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_user_groups_user_id_group_id_b88eab82_uniq` (`user_id`,`group_id`),
  ADD KEY `users_user_groups_group_id_9afc8d0e_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `users_user_user_permissions`
--
ALTER TABLE `users_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_user_user_permissions_user_id_permission_id_43338c45_uniq` (`user_id`,`permission_id`),
  ADD KEY `users_user_user_perm_permission_id_0b93982e_fk_auth_perm` (`permission_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `companies_companies`
--
ALTER TABLE `companies_companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `digi_digi`
--
ALTER TABLE `digi_digi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `sensordata_sensordata`
--
ALTER TABLE `sensordata_sensordata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `social_social`
--
ALTER TABLE `social_social`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `trip_trip`
--
ALTER TABLE `trip_trip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `users_user`
--
ALTER TABLE `users_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users_user_groups`
--
ALTER TABLE `users_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users_user_user_permissions`
--
ALTER TABLE `users_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `authtoken_token`
--
ALTER TABLE `authtoken_token`
  ADD CONSTRAINT `authtoken_token_user_id_35299eff_fk_users_user_id` FOREIGN KEY (`user_id`) REFERENCES `users_user` (`id`);

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `digi_digi`
--
ALTER TABLE `digi_digi`
  ADD CONSTRAINT `digi_digi_user_id_2d9aade9_fk_users_user_id` FOREIGN KEY (`user_id`) REFERENCES `users_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_users_user_id` FOREIGN KEY (`user_id`) REFERENCES `users_user` (`id`);

--
-- Constraints for table `sensordata_sensordata`
--
ALTER TABLE `sensordata_sensordata`
  ADD CONSTRAINT `sensordata_sensordata_user_id_75669da7_fk_users_user_id` FOREIGN KEY (`user_id`) REFERENCES `users_user` (`id`);

--
-- Constraints for table `social_social`
--
ALTER TABLE `social_social`
  ADD CONSTRAINT `social_social_user_id_94d6c7e2_fk_users_user_id` FOREIGN KEY (`user_id`) REFERENCES `users_user` (`id`);

--
-- Constraints for table `trip_trip`
--
ALTER TABLE `trip_trip`
  ADD CONSTRAINT `trip_trip_user_id_75fa8286_fk_users_user_id` FOREIGN KEY (`user_id`) REFERENCES `users_user` (`id`);

--
-- Constraints for table `users_user`
--
ALTER TABLE `users_user`
  ADD CONSTRAINT `users_user_company_id_14799323_fk_companies_companies_id` FOREIGN KEY (`company_id`) REFERENCES `companies_companies` (`id`);

--
-- Constraints for table `users_user_groups`
--
ALTER TABLE `users_user_groups`
  ADD CONSTRAINT `users_user_groups_group_id_9afc8d0e_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `users_user_groups_user_id_5f6f5a90_fk_users_user_id` FOREIGN KEY (`user_id`) REFERENCES `users_user` (`id`);

--
-- Constraints for table `users_user_user_permissions`
--
ALTER TABLE `users_user_user_permissions`
  ADD CONSTRAINT `users_user_user_perm_permission_id_0b93982e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `users_user_user_permissions_user_id_20aca447_fk_users_user_id` FOREIGN KEY (`user_id`) REFERENCES `users_user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
