from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_200_OK
)
from .models import Digi
from rest_framework.response import Response
from django.core.exceptions import (
    ValidationError,
)
from datetime import datetime, timedelta
# from django.contrib.gis.measure import D
# from django.contrib.gis.geos import *
from django.http import HttpResponse
from django.core import serializers
@csrf_exempt
@api_view(["POST"])
def add_new(request):
    user_id = request.data.get("user_id", "")
    lat = request.data.get("lat", "")
    long = request.data.get("longX", "")
    type = request.data.get("type", "")
    address = request.data.get("address", "")
    if not lat:
        return Response(
            {'Code': HTTP_400_BAD_REQUEST, "status": "error", "message": "Please Send the Latitude"},
            status=HTTP_400_BAD_REQUEST)
    if not long:
        return Response(
            {'Code': HTTP_400_BAD_REQUEST, "status": "error", "message": "Please Send the Longitude"},
            status=HTTP_400_BAD_REQUEST)
    if not type:
        return Response(
            {'Code': HTTP_400_BAD_REQUEST, "status": "error", "message": "Please Send the Alert Type"},
            status=HTTP_400_BAD_REQUEST)
    if not address:
        return Response(
            {'Code': HTTP_400_BAD_REQUEST, "status": "error", "message": "Please Send the Alert Type"},
            status=HTTP_400_BAD_REQUEST)
    try:
        alert = Digi.objects.create(
            user_id=user_id,
            lat=lat,
            long=long,
            type=type,
            address= address
        )
        print(alert)
        return Response(
            {
                "Code": HTTP_200_OK,
                "status": "success",
                "message": "Alert Added Successfully ",
            }
        )
    except ValidationError as e:
        return Response(data=str(e), status=HTTP_400_BAD_REQUEST)



@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def get_attributes_distance(request):
    # pnt = fromstr('POINT(48.796777 2.371140 )', srid=4326)
    # qs = Digi.objects.filter(type__distance_lte=(pnt, D(km=20)))
    lng = request.data.get('longitude',76.7107286)
    lat = request.data.get('latitude',30.7070296)
    limit = 50

    radius = float(5000) / 1000.0
    time_threshold = datetime.now() - timedelta(hours=3)
    time_threshold_two = datetime.now()
    query = """SELECT id, (6367*acos(cos(radians(%2f))*cos(radians(Digi_Digi.lat))*cos(radians(Digi_Digi.long)-radians(%2f))+sin(radians(%2f))*sin(radians(Digi_Digi.lat))))AS distance FROM Digi_Digi HAVING distance < %2f ORDER BY distance LIMIT 0 , %d""" % (
        float(lat),
        float(lng),
        float(lat),
        radius,
        limit
    )


    new_queryset = Digi.objects.filter(created_date__gt = time_threshold).filter(created_date__lt=time_threshold_two)
    print(new_queryset)
    #new_queryset = Digi.objects.raw(_queryset)

    digi_objects = []
    for digi_data in new_queryset:
        digi_objects.append(digi_data.pk)

    get_query = Digi.objects.filter(id__in=digi_objects)
    last_query = new_queryset | get_query
    list_data = []
    for object_data in last_query:
        short_list = {"id":0.0, "type":"", "user_id":object_data.user_id, "address":object_data.address}
        short_list['id'] = object_data.pk
        short_list['type'] = object_data.type
        list_data.append(short_list)

    #making the loop to get the data

    return Response(
        {
            "Code": HTTP_200_OK,
            "status": "success",
            "message": "Alert Added Successfully ",
            "data":list_data
        }
    )