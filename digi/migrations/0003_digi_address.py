# Generated by Django 2.1.3 on 2020-02-10 14:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('digi', '0002_auto_20190308_1428'),
    ]

    operations = [
        migrations.AddField(
            model_name='digi',
            name='address',
            field=models.CharField(blank=True, default=None, max_length=255),
        ),
    ]
