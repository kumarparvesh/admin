# Generated by Django 2.1.3 on 2020-02-10 17:03

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('digi', '0003_digi_address'),
    ]

    operations = [
        migrations.AddField(
            model_name='digi',
            name='created_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
