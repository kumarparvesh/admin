from django.db import models
from users.models import User
from django.utils import timezone

# Create Trips Models
class Digi(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True)
    lat = models.CharField(max_length=20, blank=True, default='0')
    long = models.CharField(max_length=20, blank=True, default='0')
    type = models.CharField(max_length=40, blank=True, default=None)
    address = models.CharField(max_length= 255 , blank = True, default= None)
    created_date = models.DateTimeField(default=timezone.now)
