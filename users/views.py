from django.contrib.auth.models import User, Group
from random import randint
from django.core.mail import EmailMessage
from rest_framework import viewsets
from users.serializers import UserSerializer, GroupSerializer
from django.contrib.auth.hashers import make_password
from rest_framework.authtoken.models import Token
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.hashers import check_password
from django.contrib.auth import authenticate
from django.apps import apps
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
import base64
from django.core.files.base import ContentFile
from django.conf import settings
import os

from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
from django.contrib.auth import get_user_model
from django.core.exceptions import (
    ValidationError,
)
from django.db import IntegrityError

from trip.models import Trip

Social = apps.get_model('social', 'Social')

# from sklearn.externals import joblib
User = get_user_model()


# API endpoint that allows users to be viewed or edited.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


# API endpoint that allows users to be viewed or edited.
class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


# Login API @params { username, password}, @method { post}

@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
    mobile = request.data.get("mobile")
    password = request.data.get("password")
    if mobile is None or password is None:
        return Response(
            {
                'error': 'Please provide both username and password'
            },
            status=HTTP_400_BAD_REQUEST
        )

    user = authenticate(username=mobile, password=password)

    if not user:
        return Response(
            {
                "Code": HTTP_404_NOT_FOUND,
                "status": "error", "message": "Invalid Credentials"
            },
            status=HTTP_404_NOT_FOUND
        )

    token, _ = Token.objects.get_or_create(user=user)

    if user.avatar:
        current_user = {
            'id': user.id,
            'username': user.username,
            'email': user.email,
            'mobile': user.mobile,
            'avatar': settings.SITE_URL + user.avatar.url
        }
    else:
        current_user = {
            'id': user.id,
            'username': user.username,
            'email': user.email,
            'mobile': user.mobile,
            'avatar': ''
        }

    return Response(
        {
            'data':
                {
                    'token': token.key,
                    'userdetails': current_user
                },
            'Code': HTTP_200_OK,
            "status": "success",
            "message": "Login Successfully"
        },
        status=HTTP_200_OK
    )


# Register API For User @params { username, password, email, mobile, avatar }, @method { POST }


@api_view(["POST"])
@csrf_exempt
@permission_classes((AllowAny,))
def signup(request):
    username = request.data.get("username", "")
    password = request.data.get("password", "")
    email = request.data.get("email", "")
    mobile = request.data.get("mobile", "")
    image_data = request.data.get("avatar", "")
    if image_data:
        format, imgstr = image_data.split(';base64,')
        ext = format.split('/')[-1]
        data = ContentFile(base64.b64decode(imgstr))
        file_name = mobile + "profile_pic." + ext
        avatar = file_name
    else:
        avatar = ""

    if not username:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter username"
            },
            status=HTTP_400_BAD_REQUEST
        )

    if not password:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter password"
            },
            status=HTTP_400_BAD_REQUEST
        )

    if not email:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter email"
            },
            status=HTTP_400_BAD_REQUEST
        )

    if not mobile:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter mobile number"
            },
            status=HTTP_400_BAD_REQUEST
        )

    try:
        new_user = User.objects.create_user(
            username=username,
            password=password,
            email=email,
            mobile=mobile,
            company_id=1,
            avatar=avatar
        )

    except ValidationError as e:
        return Response(data=str(e), status=HTTP_400_BAD_REQUEST)
    except IntegrityError as e:

        if 'mobile' in str(e):
            return Response(
                {
                    "Code": HTTP_400_BAD_REQUEST,
                    "status": "error",
                    "message": "Mobile Number Already Exist"
                },
                status=HTTP_400_BAD_REQUEST
            )

        if 'username' in str(e):
            return Response(
                {
                    "Code": HTTP_400_BAD_REQUEST,
                    "status": "error",
                    "message": "Username Already Exist"
                },
                status=HTTP_400_BAD_REQUEST
            )

        if 'email' in str(e):
            return Response(
                {
                    "Code": HTTP_400_BAD_REQUEST,
                    "status": "error",
                    "message": "Email Already Exist"
                },
                status=HTTP_400_BAD_REQUEST
            )

    if image_data:
        new_user.avatar.save(file_name, data, save=True)

    if new_user.avatar:
        current_user = {
            'id': new_user.id,
            'username': new_user.username,
            'email': new_user.email,
            'mobile': new_user.mobile,
            'avatar': settings.SITE_URL + new_user.avatar.url
        }
    else:
        current_user = {
            'id': new_user.id,
            'username': new_user.username,
            'email': new_user.email,
            'mobile': new_user.mobile,
            'avatar': ''
        }
    try:
        update = Social.objects.filter(invitation=mobile).update(
            status='circle'
        )
    except ValidationError as e:
        print('Error in Updating')
    return Response(
        {
            "Code": HTTP_200_OK,
            "status": "success",
            "message": "Successfully Registered",
            "userdetails": current_user
        },
        status=HTTP_200_OK
    )


# Invite API For User @params { contactList }, @method { POST }


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def invite(request):
    data = request.data.get("contactList", "")
    contact_list = []
    if not data:
        return Response(
            {'Code': HTTP_400_BAD_REQUEST, "status": "error", "message": "Please Send the contact list"},
            status=HTTP_400_BAD_REQUEST)
    if data:
        for json_dict in data:
            for key, value in json_dict.items():
                contact_list.append(value)
        print('Contact List', contact_list)
        try:
            records = User.objects.filter(mobile__in=contact_list).values('avatar', 'mobile', 'id')
            print('Query:', records.query)
            for record in records:
                for key, value in record.items():
                    if record[key] == record['avatar']:
                        if value:
                            record[key] = settings.SITE_URL + '/' + value
                        else:
                            record[key] = ''
        except User.DoesNotExist:
            records = None

    return Response(
        {
            "Code": HTTP_200_OK,
            "status": "success",
            "message": "contacts synced successfully",
            "contactList": records
        }
    )


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def circle_list(request):
    select = request.data.get("type", "")
    if select == 'Global' or select == '':
        try:
            user_ids = []
            records = User.objects.values('username', 'date_joined', 'avatar', 'score', 'styled', 'distance', 'id')
            records = list(records)
            for record in records:
                user_ids.append(record['id'])
                # for key, value in record.items():
                #     if record[key] == record['avatar']:
                #         if value:
                #             record[key] = settings.SITE_URL + '/' + value
                #         else:
                #             record[key] = ''
            global_Details = Trip.objects.filter(user_id__in=user_ids).order_by('-trip_start_time')
            ids_des = Trip.objects.filter(user_id__in=user_ids).order_by('-trip_start_time').values_list('id', flat=True)

            print("IDS DESCENDING")
            print(ids_des)
            asc_id = []
            records = []


            for detail in global_Details:
                if detail.user_id not in asc_id:
                    asc_id.append(detail.user_id)

            for id_user in asc_id:
                user_data = User.objects.filter(id = id_user).values('username', 'date_joined', 'avatar', 'score',
                                                          'styled', 'distance', 'id')
                #print(user_data[0])
                records.append(user_data[0])



            #records = User.objects.filter(id__in=asc_id).values('username', 'date_joined', 'avatar', 'score',
            #                                                        'styled', 'distance', 'id')

            for record in records:
                user_ids.append(record['id'])
                for key, value in record.items():
                    if record[key] == record['avatar']:
                        if value:
                            record[key] = settings.SITE_URL + '/' + value
                        else:
                            record[key] = ''
                        return Response(
                            {
                                'Code': HTTP_200_OK,
                                "status": "Success",
                                "message": "Users List",
                                "users": records
                            },
                            status=HTTP_200_OK)
        except User.DoesNotExist:
            return Response(
                {
                    'Code': HTTP_404_NOT_FOUND,
                    "status": "error",
                    "message": "No Records Founds",
                    "users": []
                },
                status=HTTP_404_NOT_FOUND)

    elif select == 'Friend':
        mobile = []
        try:
            records = Social.objects.filter(user_id=request.user).filter(status='circle').values('invitation')
            for record in records:
                for key, value in record.items():
                    mobile.append(value)
            user_records = User.objects.filter(mobile__in=mobile).values('username', 'date_joined', 'avatar', 'score',
                                                                         'styled', 'distance')
            if user_records:
                for user_record in user_records:
                    for key, value in user_record.items():
                        if user_record[key] == user_record['avatar']:
                            if value:
                                user_record[key] = settings.SITE_URL + '/' + value
                            else:
                                user_record[key] = ''
                return Response(
                    {
                        'Code': HTTP_200_OK,
                        'status': 'success',
                        "message": "Users List",
                        'users': user_records
                    },
                    status=HTTP_200_OK
                )
            else:
                return Response(
                    {
                        'Code': HTTP_200_OK,
                        "status": "error",
                        "message": "No Records Founds",
                        "users": []
                    },
                    status=HTTP_200_OK
                )
        except User.DoesNotExist:
            return Response(
                {
                    'Code': HTTP_404_NOT_FOUND,
                    "status": "error",
                    "message": "No Records Founds",
                    "users": []
                },
                status=HTTP_404_NOT_FOUND
            )


@csrf_exempt
@api_view(["GET"])
def user_list(request):


    #get the id of the user which have done the recent trips
    user_ids = Trip.objects.values_list('user_id',flat=True).order_by('-id').distinct()
    #Entity.objects.order_by('foreign_key').values('foreign_key').distinct()
    user_id = Remove(user_ids)
    final_list = []
    for user in user_id:
        records = User.objects.filter(id = user).values('username', 'date_joined', 'avatar', 'score', 'mobile', 'id')
        final_list.append(records[0])
    #records = User.objects.values('username', 'date_joined', 'avatar', 'score', 'mobile', 'id')
    if final_list:
        for record in final_list:
            for key, value in record.items():
                if record[key] == record['avatar']:
                    if value:
                        record[key] = settings.SITE_URL + '/' + value
                    else:
                        record[key] = ''
            return Response(
                {
                    'Code': HTTP_200_OK,
                    "status": "error",
                    "message": "Users List",
                    "users": final_list
                },
                status=HTTP_200_OK)
    else:
        return Response(
            {
                'Code': HTTP_404_NOT_FOUND,
                "status": "error",
                "message": "No Records Founds",
                "users": []
            },
            status=HTTP_404_NOT_FOUND)


@csrf_exempt
@api_view(["POST"])
def emergency_details(request):
    e_number = request.data.get("e_number", "")
    e_email = request.data.get("e_email", "")
    wifi_id = request.data.get("wifi_id", "")
    wifi_password = request.data.get("wifi_password", "")
    user_email = request.data.get("user_email", "")
    user_email_password = request.data.get("user_email_password", "")

    if not e_number:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter Emergency Number"
            },
            status=HTTP_400_BAD_REQUEST
        )

    if not e_email:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter Emergency Email"
            },
            status=HTTP_400_BAD_REQUEST
        )

    if not user_email:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter User Email"
            },
            status=HTTP_400_BAD_REQUEST
        )

    if not user_email_password:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter Email Password"
            },
            status=HTTP_400_BAD_REQUEST
        )
    email_details = User.objects.filter(email=e_email).values()
    enumber_details = User.objects.filter(mobile=e_number).values()
    if email_details:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "This Email is Already Used as Primary Email"
            },
            status=HTTP_200_OK
        )
    if enumber_details:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "This Mobile number is Already Used as Primary mobile number"
            },
            status=HTTP_200_OK
        )
    else:
        try:
            emergency_details = User.objects.filter(mobile=request.user).update(
                e_number=e_number,
                e_email=e_email,
                wifi_id=wifi_id,
                wifi_password=wifi_password,
                user_email=user_email,
                user_email_password=user_email_password
            )
            return Response(
                {
                    "Code": HTTP_200_OK,
                    "status": "success",
                    "message": "Emergency Details Updated Successfully",
                },
                status=HTTP_200_OK
            )
        except ValidationError as e:
            return Response(
                {
                    "Code": HTTP_400_BAD_REQUEST,
                    "status": "error",
                    "message": "Emergency Details Updating Error"
                },
                status=HTTP_400_BAD_REQUEST
            )


@csrf_exempt
@api_view(["GET"])
def get_emergency_details(request):
    try:
        details = User.objects.filter(mobile=request.user).values('e_number', 'e_email', 'wifi_id', 'wifi_password',
                                                                  'user_email', 'user_email_password')
        if details:
            return Response(
                {
                    "Code": HTTP_200_OK,
                    "status": "success",
                    "message": "Emergency Details",
                    "data": details
                },
                status=HTTP_200_OK
            )
        else:
            return Response(
                {
                    "Code": HTTP_200_OK,
                    "status": "success",
                    "message": "Emergency Details",
                    "data": []
                },
                status=HTTP_400_BAD_REQUEST
            )
    except User.DoesNotExist:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Emergency Details Updating Error"
            },
            status=HTTP_400_BAD_REQUEST
        )


@csrf_exempt
@api_view(["POST"])
def update_profile(request):
    email = request.data.get("email", "")
    username = request.data.get("username", "")
    image_data = request.data.get("avatar", "")
    if image_data:
        format, imgstr = image_data.split(';base64,')
        ext = format.split('/')[-1]
        data = ContentFile(base64.b64decode(imgstr))
        file_name = str(request.user) + "profile_pic." + ext
        request.user.avatar.save(file_name, data, save=True)
        avatar = request.user.avatar

    if not email:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter Email"
            },
            status=HTTP_400_BAD_REQUEST
        )

    if not username:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter Username"
            },
            status=HTTP_400_BAD_REQUEST
        )
    if image_data:
        try:
            update_profile = User.objects.filter(mobile=request.user).update(
                email=email,
                username=username,
                avatar=avatar
            )
        except IntegrityError as e:
            if 'email' in str(e):
                return Response(
                    {
                        "Code": HTTP_400_BAD_REQUEST,
                        "status": "error",
                        "message": "Email Already Exist"
                    },
                    status=HTTP_400_BAD_REQUEST
                )
            if 'username' in str(e):
                return Response(
                    {
                        "Code": HTTP_400_BAD_REQUEST,
                        "status": "error",
                        "message": "Username Already Exist"
                    },
                    status=HTTP_400_BAD_REQUEST
                )
        except ValidationError as e:
            return Response(
                {
                    "Code": HTTP_400_BAD_REQUEST,
                    "status": "error",
                    "message": "Emergency Details Updating Error"
                },
                status=HTTP_400_BAD_REQUEST
            )
    else:
        try:
            update_profile = User.objects.filter(mobile=request.user).update(
                email=email,
                username=username
            )
        except IntegrityError as e:
            if 'email' in str(e):
                return Response(
                    {
                        "Code": HTTP_400_BAD_REQUEST,
                        "status": "error",
                        "message": "Email Already Exist"
                    },
                    status=HTTP_400_BAD_REQUEST
                )
            if 'username' in str(e):
                return Response(
                    {
                        "Code": HTTP_400_BAD_REQUEST,
                        "status": "error",
                        "message": "Username Already Exist"
                    },
                    status=HTTP_400_BAD_REQUEST
                )
        except ValidationError as e:
            return Response(
                {
                    "Code": HTTP_400_BAD_REQUEST,
                    "status": "error",
                    "message": "Profile Details Updating Error"
                },
                status=HTTP_400_BAD_REQUEST
            )

    return Response(
        {
            "Code": HTTP_200_OK,
            "status": "success",
            "message": "Successfully Updated",
        },
        status=HTTP_200_OK
    )


@csrf_exempt
@api_view(["POST"])
def change_password(request):
    old_password = request.data.get("old_password", "")
    new_password = request.data.get("new_password", "")

    if not old_password:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Current Password Required"
            },
            status=HTTP_400_BAD_REQUEST
        )
    if not new_password:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter New Password"
            },
            status=HTTP_400_BAD_REQUEST
        )
    user_details = User.objects.get(mobile=request.user)
    if check_password(old_password, user_details.password):
        print('Okay')
        user_details.set_password(new_password)
        user_details.save()
        return Response(
            {
                "Code": HTTP_200_OK,
                "status": "success",
                "message": "Successfully Updated",
            },
            status=HTTP_200_OK
        )
    else:
        return Response(
            {
                "Code": HTTP_200_OK,
                "status": "success",
                "message": "Invalid Password",
            },
            status=HTTP_200_OK
        )


@csrf_exempt
@api_view(["GET"])
def get_user_details(request):
    try:
        details = User.objects.filter(mobile=request.user).values('id', 'mobile', 'username', 'email', 'avatar')
        for detail in details:
            for key, value in detail.items():
                if detail[key] == detail['avatar']:
                    if value:
                        detail[key] = settings.SITE_URL + '/' + value
                    else:
                        detail[key] = ''
        return Response(
            {
                "Code": HTTP_200_OK,
                "status": "success",
                "message": "Emergency Details",
                "data": detail
            },
            status=HTTP_200_OK
        )

    except User.DoesNotExist:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "No record Found"
            },
            status=HTTP_400_BAD_REQUEST
        )


@csrf_exempt
@api_view(["POST"])
def get_search_user_details(request):
    mobile = request.data.get("mobile", "")
    if not mobile:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Mobile number Is Required"
            },
            status=HTTP_400_BAD_REQUEST
        )
    try:
        details = User.objects.filter(mobile=mobile).values('id', 'mobile', 'username', 'email', 'avatar')
        if details:
            for detail in details:
                for key, value in detail.items():
                    if detail[key] == detail['avatar']:
                        if value:
                            detail[key] = settings.SITE_URL + '/' + value
                        else:
                            detail[key] = ''
            return Response(
                {
                    "Code": HTTP_200_OK,
                    "status": "success",
                    "message": "User Details",
                    "data": detail
                },
                status=HTTP_200_OK
            )
        else:
            return Response(
                {
                    "Code": HTTP_200_OK,
                    "status": "success",
                    "message": "No Records Found For this User",
                    "data": []
                },
                status=HTTP_200_OK
            )

    except User.DoesNotExist:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "No record Found"
            },
            status=HTTP_400_BAD_REQUEST
        )


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def reset(request):
    email = request.data.get("email", "")
    if not email:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Email Required"
            },
            status=HTTP_400_BAD_REQUEST
        )
    user_details = User.objects.filter(email=email).values()
    if user_details:
        random_token = randint(000000, 999999)
        msg = 'Your Reset Password Token is : ' + str(random_token)
        print(msg)
        email_token = EmailMessage('Password Reset Token', msg, to=[email])
        email_token.send()
        update = User.objects.filter(email=email).update(
            reset_token=random_token
        )
        return Response(
            {
                'Code': HTTP_200_OK,
                "status": "success",
                "message": "Mail Send To Your Email"
            },
            status=HTTP_200_OK
        )
    else:
        return Response(
            {
                'Code': HTTP_200_OK,
                "status": "success",
                "message": "Mail Not Found"
            },
            status=HTTP_200_OK
        )


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def reset_token(request):
    mobile = request.data.get("mobile", "")
    password = request.data.get("password", "")
    if not mobile:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Valid Mobile Required"
            },
            status=HTTP_400_BAD_REQUEST
        )
    if not password:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Password Required"
            },
            status=HTTP_400_BAD_REQUEST
        )
    user_details = User.objects.filter(mobile=mobile).values()
    if user_details:
        update = User.objects.filter(mobile=mobile).update(
            password=make_password(password)
        )
        return Response(
            {
                'Code': HTTP_200_OK,
                "status": "success",
                "message": "Password Reset Successfully"
            },
            status=HTTP_200_OK
        )
    else:
        return Response(
            {
                'Code': HTTP_404_NOT_FOUND,
                "status": "success",
                "message": "Invalid Token"
            },
            status=HTTP_200_OK
        )

def Remove(duplicate):
    final_list = []
    for num in duplicate:
        if num not in final_list:
            final_list.append(num)
    return final_list

@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def user_driving_style(request):
    print("Api called User driving style!")

    from_date = request.data.get("from_date", "")
    to_date = request.data.get("to_date", "")
    user_id = request.data.get("user_id", "")



    if not from_date:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter Start Date and Time"
            },
            status=HTTP_400_BAD_REQUEST
        )
    if not to_date:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter To Date and Time"
            },
            status=HTTP_400_BAD_REQUEST
        )
    else:
        d = date.today()
        # from_date = get_first_day(d)
        # to_date = get_last_day(d)
    if not id:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "User ID Required"
            },
            status=HTTP_400_BAD_REQUEST
        )
    try:
        print("inside try user style")
        user = Users.objects.filter(id=user_id).values()
        stats = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).aggregate(
            speed=Round(Avg('speed_avg')),
            total_distance=Round(Sum('distance')),
            timein_traffic=Round(Max('time_in_traffic')),
            excelent_driving=Round(Avg('excellent_driving')),
            fair_driving=Round(Avg('fair_driving')),
            risky_driving=Round(Avg('risky_driving'))
        )
        if stats:
            today = timezone.now()

            ## Graph values for Excellent Driving
            date_diff = get_date_Difference(from_date, to_date)
            if not date_diff:
                params = {
                    'today': today,
                    'users': user,
                    'record': stats,
                    'from': from_date,
                    'to': to_date,
                    'type': report_type,
                    'email_type': email_type,
                }
                print('params', params)

                return Render.render('smartreport.html', params)

            if (date_diff == 'years'):
                excellent_data = Trip.objects.filter(user_id=user_id,
                                                     created_date__date__range=[from_date, to_date]).annotate(
                    year=TruncYear('created_date')).values('year').annotate(
                    average=Avg('excellent_driving')).values('year', 'average')

            elif (date_diff == 'months'):
                excellent_data = Trip.objects.filter(user_id=user_id,
                                                     created_date__date__range=[from_date, to_date]).annotate(
                    month=TruncMonth('created_date')).values('month').annotate(
                    average=Avg('excellent_driving')).values('month', 'average')

            elif (date_diff == 'weeks'):
                excellent_data = Trip.objects.filter(user_id=user_id,
                                                     created_date__date__range=[from_date, to_date]).annotate(
                    week=TruncWeek('created_date')).values('week').annotate(
                    average=Avg('excellent_driving')).values('week', 'average')

            else:
                excellent_data = Trip.objects.filter(user_id=user_id,
                                                     created_date__date__range=[from_date, to_date]).annotate(
                    day=TruncDay('created_date')).values('day').annotate(
                    average=Avg('excellent_driving')).values('day', 'average')

            import matplotlib
            matplotlib.use('Agg')
            import matplotlib.pyplot as plt
            date_list = []
            average_list = []
            for data in excellent_data:
                if ('year' in str(excellent_data[0])):
                    date_list.append(str(data['year']).replace('00:00:00', '').replace('-01', ''))
                    average_list.append(round(data['average'], 2))
                if ('week' in str(excellent_data[0])):
                    date_list.append(str(data['week']).replace('00:00:00', '').replace('-01', ''))
                    average_list.append(round(data['average'], 2))
                if ('month' in str(excellent_data[0])):
                    date_list.append(str(data['month']).replace('00:00:00', '').replace('-01', ''))
                    average_list.append(round(data['average'], 2))
                if ('day' in str(excellent_data[0])):
                    date_list.append(str(data['day']).replace('00:00:00', '').replace('-01', ''))
                    average_list.append(round(data['average'], 2))

            plt.style.use('ggplot')
            x = date_list
            y = average_list
            x_pos = [i for i, _ in enumerate(x)]
            plt.xticks(rotation=90)

            for i in range(len(date_list)):  # your number of bars
                plt.text(x=x_pos[i] - .3,  # takes your x values as horizontal positioning argument
                         y=average_list[i],  # takes your y values as vertical positioning argument
                         s=average_list[i],  # the labels you want to add to the data
                         size=10,
                         rotation=90)

            attr = "excellent_driving"
            ## deleting image
            path = 'media/' + attr + 'smart_report' + '.png'
            if os.path.isfile(path):
                os.remove(path)
            plt.bar(x_pos, y, color='#6b93f2')
            plt.xlabel(attr.replace('_', ' ').upper(), color='black', size=16)
            plt.ylabel("Average")
            plt.title('')
            plt.xticks(x_pos, x, size=8)
            image_ticks = str(time.time()).replace('.', '')
            plt.savefig('media/' + attr + 'smart_report' + '.png', dpi=(200), bbox_inches="tight")
            plt.close()

            params = {
                'today': today,
                'users': user,
                'record': stats,
                'from': from_date,
                'to': to_date,
                'type': report_type,
                'email_type': email_type,
                'graph_image': BASE_DIR + '/media/' + attr + 'smart_report' + '.png'
            }
            print('params', params)

            return Render.render('smartreport.html', params)

            return Response(
                {
                    'Code': HTTP_200_OK,
                    "status": "success",
                    'data': params,

                })
        else:
            return Response(
                {
                    'data': [],
                    'Code': HTTP_200_OK,
                    "status": "success"
                },
                status=HTTP_200_OK
            )
    except ObjectDoesNotExist:
        return "No Record Found"

@csrf_exempt
@api_view(["DELETE"])
def delete_user(request, pk):
    #check user exist or not
    user_ = User.objects.filter(id = pk).values()

    if user_:
        #delete user
        User.objects.filter(id = pk).delete()
        return Response(
            {
                'data': [],
                'Code': HTTP_200_OK,
                "status": "success",
                "message": "User deleted Successfully"
            },
            status=HTTP_200_OK
        )
    return Response(
        {
            'data': [],
            'Code': HTTP_200_OK,
            "status": "Failed",
            "message":"User not found"
        },
        status=HTTP_200_OK
    )
