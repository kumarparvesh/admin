from django.db import models
from companies.models import Companies
# Create your models here.

from django.contrib.auth.models import AbstractUser


# Extend User Model
class User(AbstractUser):
    mobile = models.CharField(max_length=16, blank=False, unique=True)
    score = models.FloatField(max_length=3, default=0)
    styled = models.CharField(max_length=16, default='')
    distance = models.FloatField(max_length=3, default=0)
    e_number = models.CharField(max_length=10, default='')
    e_email = models.CharField(max_length=255, default='')
    wifi_id = models.CharField(max_length=255, default='')
    wifi_password = models.CharField(max_length=255, default='')
    user_email = models.CharField(max_length=255, default='')
    user_email_password = models.CharField(max_length=255, default='')
    avatar = models.ImageField(upload_to='images', blank=True, null=True)
    email = models.EmailField(max_length=100, blank=False, unique=True)
    reset_token = models.CharField(max_length=10, default='')
    absolute_skill_score = models.FloatField(max_length=7, default=0)
    absolute_agression_score = models.FloatField(max_length=7,  default=0)
    company = models.ForeignKey(
        Companies, on_delete=models.CASCADE, null=True)
    USERNAME_FIELD = 'mobile'
