# Generated by Django 2.1.3 on 2019-02-14 15:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trip', '0009_auto_20190214_1123'),
    ]

    operations = [
        migrations.AddField(
            model_name='trip',
            name='acceleration',
            field=models.FloatField(default=0, max_length=3),
        ),
        migrations.AddField(
            model_name='trip',
            name='highway',
            field=models.FloatField(default=0, max_length=3),
        ),
        migrations.AddField(
            model_name='trip',
            name='lane_change',
            field=models.FloatField(default=0, max_length=3),
        ),
        migrations.AddField(
            model_name='trip',
            name='non_highway_inside_city',
            field=models.FloatField(default=0, max_length=3),
        ),
        migrations.AddField(
            model_name='trip',
            name='non_highway_outside_city',
            field=models.FloatField(default=0, max_length=3),
        ),
        migrations.AddField(
            model_name='trip',
            name='rural',
            field=models.FloatField(default=0, max_length=3),
        ),
        migrations.AddField(
            model_name='trip',
            name='turns',
            field=models.FloatField(default=0, max_length=3),
        ),
    ]
