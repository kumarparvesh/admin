# Generated by Django 2.1.3 on 2019-11-29 11:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trip', '0023_auto_20191129_1109'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='trip',
            name='sudden_lane_change',
        ),
        migrations.RemoveField(
            model_name='trip',
            name='sudden_turns',
        ),
    ]
