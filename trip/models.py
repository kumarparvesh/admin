from django.db import models
from django.utils import timezone
from users.models import User


# Create Trips Models
class Trip(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True)
    trip_start_time = models.DateTimeField(blank=True, null=True)
    trip_end_time = models.DateTimeField(blank=True, null=True)
    distance = models.DecimalField(max_digits=7, decimal_places=2, default=0)
    speed_max = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    speed_avg = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    speed_min = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    excellent_driving = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    fair_driving = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    risky_driving = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    road_type = models.CharField(max_length=40, default='District Road')
    road_slope = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    acceleration_max = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    acceleration_avg = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    phone_distraction = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    hard_braking = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    over_speeding = models.DecimalField(max_digits=7, decimal_places=2, default=0)
    hard_acceleration = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    highway = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    acceleration = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    lane_change = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    turns = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    duration = models.CharField(max_length=10, blank=True, default=0)
    time_in_traffic = models.CharField(max_length=10, blank=True, default=0)
    speed_limit = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    right_turns = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    left_turns = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    frequent_braking = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    speed_too_high_curve = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    finished = models.BooleanField(default=False)
    status = models.BooleanField(default=False)
    flag = models.BooleanField(default=False)
    is_gyro_available = models.BooleanField(default=True)
    created_date = models.DateTimeField(default=timezone.now)
    usertype = models.CharField(max_length=255, default='Driver')
    device_id = models.CharField(max_length=255, default = None)
    mobile_model = models.CharField(max_length=255, default = None)
    mobile_version = models.CharField(max_length=255, default = None)
    app_version_name = models.CharField(max_length=255, default = None)
    has_user_changed = models.BooleanField(default=False)
    is_confirmed = models.BooleanField(default=False)
    event_lane_change = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    event_acc = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    event_turn = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    event_braking = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    event_other = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    events_acc_hard = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    events_freq_breaking = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    events_excesive_breaking = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    event_sudden_lane_change = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    event_sudden_turn = models.DecimalField(max_digits=5, decimal_places=2, default=0)

# Create Deleted Trips Models
class DeletedTrip(models.Model):
    trip_id = models.IntegerField(blank=False, null=False, default=None)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    trip_start_time = models.DateTimeField(blank=True, null=True)
    trip_end_time = models.DateTimeField(blank=True, null=True)
    trip_created_date = models.DateTimeField(default=timezone.now)
    usertype = models.CharField(max_length=255, default='Driver')
    created_date = models.DateTimeField(default=timezone.now)


class DssWeight(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, default='Driver')
    weight_acc = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    weight_turn = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    weight_braking = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    weight_lane_change = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    weight_other = models.DecimalField(max_digits=5, decimal_places=2, default=0)

    class Meta:
        db_table = 'dss_weight'