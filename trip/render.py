from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template
import xhtml2pdf.pisa as pisa
from django.core.mail import EmailMessage
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_200_OK
)


class Render:

    @staticmethod
    def render(path: str, params: dict):
        global filename, user_email
        for user in params['users']:
            filename = user['mobile']
            user_email = user['email']
        template = get_template(path)
        html = template.render(params)
        response = BytesIO()
        result = open('pdf/' + str(filename) + '.pdf', 'wb')  # Changed from file to filename
        save_pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), result)
        pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), response)
        if not pdf.err:
            if params['email_type'] == 'Yes':
                msg = 'Report'
                email = EmailMessage('Password Reset Token', msg, to=[user_email])
                email.attach_file('pdf/' + str(filename) + '.pdf')
                email.send()
                return Response(
                    {
                        'Code': 200,
                        "status": "success",
                        "Msg": "Email Send to your Registered Email"
                    },
                    status=HTTP_200_OK
                )
            else:
                return HttpResponse(response.getvalue(), content_type='application/pdf')
        else:
            return HttpResponse("Error Rendering PDF", status=400)

    @staticmethod
    def render_to(path: str, params: dict):
        template = get_template(path)
        html = template.render(params)
        response = BytesIO()
        pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), response)
        if not pdf.err:
            return HttpResponse(response.getvalue(), content_type='application/pdf')
        else:
            return HttpResponse("Error Rendering PDF", status=400)
