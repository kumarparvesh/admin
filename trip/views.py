from django.db.models.functions import TruncMonth
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from django.conf import settings
from django.db.models import Q
import numpy as np
import datetime
import pandas as pd
import geopy.distance
import random
from dateutil.relativedelta import *
from easy_pdf.rendering import render_to_pdf

# import polyline
# import json
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import ProtectedError
from rest_framework import viewsets
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from .serializers import TripSerializer
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_200_OK
)
import twilio
from twilio.rest import Client
from django.db.models import F, Func
from .models import Trip, DeletedTrip, DssWeight
from sensordata.models import Sensordata
from rest_framework.response import Response
from django.core.exceptions import (
    ValidationError,
)
from django.apps import apps
from django.db.models import Avg, Sum, Max, Count, Min
from celery.task.schedules import crontab
from celery.decorators import periodic_task
from celery.utils.log import get_task_logger
# from sklearn.externals import joblib
import joblib
from datetime import date, timedelta, time, datetime
from django.db.models import Q
from .render import Render
from django.utils import timezone
import sys
from django.db.models.functions import TruncMonth , TruncYear , TruncDay, TruncWeek
import time
import calendar
from fpdf import FPDF
from decimal import *
import os
from dateutil.relativedelta import relativedelta
import monthdelta

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

Social = apps.get_model('social', 'Social')
Users = apps.get_model('users', 'User')
logger = get_task_logger(__name__)
#model = joblib.load(settings.ML_MODEL + 'analysis.joblib')

#model = joblib.load(settings.ML_MODEL + 'train-data.joblib')
model = joblib.load(settings.ML_MODEL + 'analysis.joblib')
driver_passenger = joblib.load(settings.ML_MODEL + 'driver.joblib')


class TripViewSet(viewsets.ModelViewSet):
    queryset = Trip.objects.all()
    serializer_class = TripSerializer


class Round(Func):
    function = 'ROUND'
    template = '%(function)s(%(expressions)s, 2)'


# Latest Trips Driving Stats.

@csrf_exempt
@api_view(["GET"])
def latest_trip_driving_stats(request):
    try:
        dss_data = {}
        # data_event = DssWeight.objects.filter(name = event).values()
        dss_weight = DssWeight.objects.values_list('name', flat=True)
        #print(dss_weight)
        for name in dss_weight:
            name_data = DssWeight.objects.filter(name=name).values().first()
            dss_data[name] = name_data
        stats = Trip.objects.filter(user_id=request.user).filter(status=1).order_by('-id').values()
        friends = Social.objects.filter(user_id=request.user, status='circle').count()
        overall = Trip.objects.filter(user_id=request.user).filter(status=1).aggregate(
            #excellent=Round(Avg('excellent_driving')),
            #risky=Round(Avg('risky_driving')),
            #fair=Round(Avg('fair_driving')),
            total_distance=Round(Sum('distance')),
            highest_score=Round(Max('excellent_driving'))
        )

        #get excellent risky and fair
        overall_drivers = Trip.objects.filter(user_id=request.user, usertype='Driver', is_confirmed = 1).filter(status=1).aggregate(
            excellent=Round(Avg('excellent_driving')),
            risky=Round(Avg('risky_driving')),
            fair=Round(Avg('fair_driving')),

        )



        excellent_driving_score = 0
        risky_driving_score = 0
        fair_driving_score = 0

        total_excellent_driving = Trip.objects.filter(user_id=request.user, excellent_driving__gte=80).filter(
            status=1).count()
        total_fair_driving = Trip.objects.filter(user_id=request.user, excellent_driving__gte=50,
                                                 excellent_driving__lt=80).filter(status=1).count()
        total_risky_driving = Trip.objects.filter(user_id=request.user, excellent_driving__lt=50).filter(status=1).count()
        total_count = Trip.objects.filter(user_id=request.user).filter(status=1).count()

        if total_count > 0:
            excellent_driving_score = (total_excellent_driving / total_count)*100
            fair_driving_score = (total_fair_driving / total_count)*100
            risky_driving_score = (total_risky_driving / total_count)*100

        #return Response({"data":[round(excellent_driving_score,2), round(fair_driving_score,2), round(risky_driving_score, 2)]})
        # stats['excelent_driving'] =
        # stats['fair_driving'] =
        # stats['risky_driving'] =

        overall['excellent'] = round(excellent_driving_score, 2)
        overall['risky'] = round(risky_driving_score, 2)
        overall['fair'] = round(fair_driving_score, 2)


        stats_data = stats[0]
        stats_data['trip_score'] = stats_data['excellent_driving']
        print(dss_data)
        dss_new = dss_data['Early Morning']
        #check out the trip timing also
        if stats_data['time_in_traffic']=='Morning':
            dss_new = dss_data['Morning']
        elif stats_data['time_in_traffic'] == 'Afternoon':
            dss_new = dss_data['Afternoon']
        elif stats_data['time_in_traffic'] == 'Evening':
            dss_new = dss_data['Evening']
        else:
            dss_new = dss_data['Night']

        #check which point is coming in excellent_driving, fair_driving and risky driving


        excellent_total= 0;
        fair_total = 0
        risky_total = 0

        if 100>=stats_data['hard_acceleration'] >= 80:
            excellent_total = dss_new['weight_acc']
        elif 80>stats_data['hard_acceleration']>=50:
            fair_total = dss_new['weight_acc']
        else:
            risky_total = dss_new['weight_acc']

        if 100 >= stats_data['hard_braking'] >= 80:
            excellent_total +=dss_new['weight_braking']
        elif 80 > stats_data['hard_braking'] >= 50:
            fair_total +=dss_new['weight_braking']
        else:
            risky_total += dss_new['weight_braking']

        if 100 >= stats_data['turns'] >= 80:
            excellent_total += dss_new['weight_turn']
        elif 80 > stats_data['turns'] >= 50:
            fair_total += dss_new['weight_turn']
        else:
            risky_total += dss_new['weight_turn']

        if 100 >= stats_data['lane_change'] >= 80:
            excellent_total += dss_new['weight_lane_change']
        elif 80 > stats_data['lane_change'] >= 50:
            fair_total += dss_new['weight_lane_change']
        else:
            risky_total += dss_new['weight_lane_change']

        if 100 >= stats_data['phone_distraction'] >= 80:
            excellent_total += dss_new['weight_other']
        elif 80 > stats_data['phone_distraction'] >= 50:
            fair_total += dss_new['weight_other']
        else:
            risky_total += dss_new['weight_other']

        stats_data['excellent_driving'] = excellent_total*100
        stats_data['fair_driving'] = fair_total*100
        stats_data['risky_driving'] = risky_total*100

        # TODO: Convert the lat long in string
        if overall['excellent'] is not None and overall['risky'] is not None and overall['fair'] is not None:
            if (overall['excellent'] >= overall['risky']) and (overall['excellent'] >= overall['fair']):
                overall['style'] = 'Excellent'
            elif (overall['risky'] >= overall['excellent']) and (overall['risky'] >= overall['fair']):
                overall['style'] = 'Risky'
            else:
                overall['style'] = 'Fair'

        user_rank = Users.objects.order_by('-score').values('score', 'mobile')
        rank = 0
        for i, record in enumerate(user_rank):
            if str(request.user) == record['mobile']:
                rank = i + 1
        circle = {
            'friends': friends,
            'Rank': rank,
            'score': overall['highest_score']
        }
        if stats:
            return Response(
                {
                    'data': {'latest': stats_data, 'overall': overall, 'circle': circle, 'DssWeight': dss_data},
                    'Code': HTTP_200_OK,
                    "status": "success"
                },
                status=HTTP_200_OK
            )
        else:
            return Response(
                {
                    'data': None,
                    'Code': HTTP_200_OK,
                    "status": "success"
                },
                status=HTTP_200_OK
            )
    except ObjectDoesNotExist:
        return "No Record Found"


# Find the Trips of Given Date Provided the Users and filters the Trips as per the Date

@csrf_exempt
@api_view(["POST"])
def trips_of_a_particular_date(request):
    from_date = request.data.get("from_date", "")
    to_date = request.data.get("to_date", "")

    if not from_date:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter Start Date and Time"
            },
            status=HTTP_400_BAD_REQUEST
        )
    if not to_date:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter To Date and Time"
            },
            status=HTTP_400_BAD_REQUEST
        )

    try:
        stats = Trip.objects.filter(user_id=request.user).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).values()
        if stats:
            return Response(
                {
                    'data': stats,
                    'Code': HTTP_200_OK,
                    "status": "success"
                },
                status=HTTP_200_OK
            )
        else:
            return Response(
                {
                    'data': [],
                    'Code': HTTP_200_OK,
                    "status": "success"
                },
                status=HTTP_200_OK
            )
    except ObjectDoesNotExist:
        return "No Record Found"


# Daily Trips Records Stats of One day


@csrf_exempt
@api_view(["POST"])
def daily_trip_record(request):
    from_date = request.data.get("from_date", "")
    to_date = request.data.get("to_date", "")
    if not from_date:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Valid Date"
            },
            status=HTTP_400_BAD_REQUEST
        )
    if not to_date:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Valid Date"
            },
            status=HTTP_400_BAD_REQUEST
        )
    try:
        stats = Trip.objects.filter(user_id=request.user).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).extra(select={'daily': 'date( created_date )'}).values(
            'daily') \
            .annotate(average=Avg('excellent_driving'))
        if stats:
            return Response(
                {
                    'data': stats,
                    'Code': HTTP_200_OK,
                    "status": "success"
                },
                status=HTTP_200_OK
            )
        else:
            return Response(
                {
                    'data': [],
                    'Code': HTTP_200_OK,
                    "status": "success"
                },
                status=HTTP_200_OK
            )
    except ObjectDoesNotExist:
        return "No Record Found"


@csrf_exempt
@api_view(["GET"])
def monthly_chart(request):
    try:
        is_daily = False
        stats = Trip.objects.filter(user_id=request.user).filter(status=1).annotate(
            month=TruncMonth('created_date')).values('month').annotate(
            average=Avg('excellent_driving')).values('month', 'average')
        if len(stats) == 1:
            weeks = Trip.objects.filter(user_id=request.user).extra(select={'month': 'date( created_date )'}).values(
                'month') \
                .annotate(average=Avg('excellent_driving'))
            is_daily = True
            return Response(
                {
                    'data': weeks,
                    'is_daily': is_daily,
                    'Code': HTTP_200_OK,
                    "status": "success"
                },
                status=HTTP_200_OK
            )

        if stats:
            return Response(
                {
                    'data': stats,
                    'is_daily': is_daily,
                    'Code': HTTP_200_OK,
                    "status": "success"
                },
                status=HTTP_200_OK
            )
        else:
            return Response(
                {
                    'data': [],
                    'Code': HTTP_200_OK,
                    "status": "success"
                },
                status=HTTP_200_OK
            )
    except ObjectDoesNotExist:
        return "No Record Found"


@csrf_exempt
@api_view(['POST'])
def trip_details(request):
    trip_id = request.data.get('trip_id', '')
    data_needed = 6
    step_skip_speed = 60
    step_skip_turn = 60
    step_skip_acc = 60
    if not trip_id:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Trip id is missing"
            },
            status=HTTP_400_BAD_REQUEST
        )
    current_trip_details = Trip.objects.filter(id=trip_id).values().first()

    speed_data_count = Sensordata.objects.filter(~Q(speed=0), trip_id= trip_id).count()
    turn_event_count = Sensordata.objects.filter(~Q(gyro_y=0), trip_id= trip_id).count()
    acceleration_event_count = Sensordata.objects.filter(trip_id = trip_id).count()

    #
    print(speed_data_count)
    #change the number of skipping of the data
    step_skip_speed = int(speed_data_count/7)
    step_skip_turn= int(turn_event_count/7)
    acceleration_event_count =  int(acceleration_event_count/7)

    print(step_skip_speed)
    check_gyro_available = Trip.objects.filter(id=trip_id).values_list('is_gyro_available', flat=True)
    print(check_gyro_available[0])


    speed_graph = Sensordata.objects.filter(~Q(speed=0),trip_id=trip_id).values('timeInMillis', 'speed')[::step_skip_speed]

    turn_events = Sensordata.objects.filter(~Q(gyro_y=0), trip_id=trip_id).values('timeInMillis', 'gyro_y')[::step_skip_turn]
    if not check_gyro_available[0]:
        turn_event_count = Sensordata.objects.filter(trip_id=trip_id).count()
        step_skip_turn = int(turn_event_count / 7)
        print("step_skip")
        print(step_skip_turn)
        turn_events = Sensordata.objects.filter(trip_id=trip_id).values('timeInMillis', 'bearing')[::step_skip_turn]

    acceleration_events = Sensordata.objects.filter(trip_id=trip_id).values('timeInMillis', 'acc_magnitude')[::acceleration_event_count]

    if trip_details:
        return Response(
            {
                'data': {
                    'trip_details': current_trip_details,
                    'speed_graph': speed_graph,
                    'acceleration_events': acceleration_events,
                    'turn_events': turn_events,
                },
                'Code': HTTP_200_OK,
                "status": "success"
            },
            status=HTTP_200_OK
        )
####
@csrf_exempt
@api_view(['POST'])
def trip_more_details(request):
    category = []
    trip_id = request.data.get('trip_id', '')
    print(request.user)
    if not trip_id:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Trip id is missing"
            },
            status=HTTP_400_BAD_REQUEST
        )

    current_trip_details = Trip.objects.filter(id=trip_id).values(
        'trip_start_time',
        'trip_end_time',
        'distance',
        'duration',
        'highway',
        'excellent_driving',
        'risky_driving'
    )
    turn = Trip.objects.filter(user_id = request.user).filter(id = trip_id).values_list('turns',flat=True)

    categories = Trip.objects.filter(user_id = request.user).filter(id=trip_id).annotate(
        Distraction =F('phone_distraction'),
        Acceleration=F('hard_acceleration'),
        Braking=F('hard_braking'),
        Turns=F('turns'),
        RightTurn=F('right_turns'),
        LeftTurn=F('left_turns'),
        FrequentBraking = F('frequent_braking'),
        Lanechange=F('lane_change'),
        SpeedToHighCurve = F('speed_too_high_curve'),
        OverSpeeding=F('over_speeding'),

    ).values(
        'Distraction',
        'Acceleration',
        'Braking',
        'Turns',
        'RightTurn',
        'LeftTurn',
        'FrequentBraking',
        'Lanechange',
        'SpeedToHighCurve',
        'OverSpeeding',


    )
    print(categories)
    if categories:
        for json_dict in categories:
            for key, value in json_dict.items():
                color = '#006400'
                if key == 'Distraction':
                    key = 'Distracted Driving'
                    color = '#FF4500'
                if key == 'Acceleration':
                    key = 'Hard Acceleration'
                    color = '#FF0000'
                if key == 'Braking':
                    key = 'Hard Braking'
                    color = '#000000'
                if key == 'Turns':
                    key = 'Sudden Turns'
                    color = '#00CED1'
                if key == 'RightTurn':
                    key = 'Sudden Right Turn'
                    color = '#9c9c3d'
                if key == 'LeftTurn':
                    key = 'Sudden Left Turns'
                    color = '#71db5c'
                if key == 'FrequentBraking':
                    key = 'Frequent Braking'
                    color = '#FF0000'
                if key == 'Lanechange':
                    key = 'Sudden Lane Change'
                    color = '#FFFF00'
                if key == 'SpeedToHighCurve':
                    key = 'Speed To High Curve'
                    color = '#9c9c3d'
                    #value = (json_dict['RightTurn'] + json_dict['LeftTurn']) / 2
                if key == 'OverSpeeding':
                     if value == 0:
                         value = 100
                     else:
                         value = 0
                     color = '#00008B'


                cat = {
                    'category': key,
                    'score': value,
                    'color': color
                }
                category.append(cat)


    slice_data = 1000
    step_count = 100
    total_sensor_data = Sensordata.objects.filter(trip_id= trip_id).count()
    step_count = int(total_sensor_data/slice_data)

    graph = Sensordata.objects.filter(trip_id=trip_id).values('latitude', 'longitude')[::step_count]
    #print(len(graph))
    # for items in graph:
    #     label = calculate_driving_score(items["acc_z"], items["acc_y"], items["gyro_y"])
    #     if label == 'normal':
    #         items['color'] = '#000000'
    #     elif label == 'Sudden Turns':
    #         items['color'] = '#00CED1'
    #     elif label == 'Excessive Break':
    #         items['color'] = '#000000'
    #     elif label == 'Aggressive Left Turn':
    #         items['color'] = '#00CED1'
    #     elif label == 'Aggressive Right Turn':
    #         items['color'] = '#00CED1'
    #     elif label == 'Aggressive Acceleration':
    #         items['color'] = '#FF0000'
    #     elif label == 'Frequent Braking':
    #         items['color'] = '#000000'
    #     elif label == 'Lane Change':
    #         items['color'] = '#FFFF00'
    #     elif label == 'Sudden Lane Change':
    #         items['color'] = '#FFFF00'

    #print(current_trip_details)
    if trip_details:
        return Response(
            {
                'data': {
                    'trip_details': current_trip_details[0],
                    'category': category,
                    'graph': graph,
                },
                'Code': HTTP_200_OK,
                "status": "success"
            },
            status=HTTP_200_OK
        )


def rescale(values, new_min=0, new_max=5):
    output = []
    old_min, old_max = min(values), max(values)
    for v in values:
        try:
            new_v = (new_max - new_min) / (old_max - old_min) * (v - old_min) + new_min
            new_v = 5 - new_v
            output.append(new_v)
        except ZeroDivisionError:
            new_v = 5
            output.append(new_v)
    return output


@csrf_exempt
@api_view(["POST"])
def driving_style_analysis(request):
    from_date = request.data.get("from_date", "")
    to_date = request.data.get("to_date", "")

    total_count = 0
    day_count = 0
    night_count = 0
    day_ratio = 0
    night_ratio = 0


    if not from_date:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter Start Date and Time"
            },
            status=HTTP_400_BAD_REQUEST
        )
    if not to_date:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter To Date and Time"
            },
            status=HTTP_400_BAD_REQUEST
        )
    overspeeding_data = Trip.objects.filter(user_id=request.user).filter(status=1).filter(
        created_date__date__range=(from_date, to_date)).values_list('over_speeding', flat=True)
    try:

        stats = Trip.objects.filter(user_id=request.user).filter(status=1).filter(usertype='Driver').filter(
            created_date__date__range=(from_date, to_date)).aggregate(
            Sum('distance'),
            Avg('speed_avg'),
            Sum('time_in_traffic'),
            Avg('speed_limit'),
            Avg('phone_distraction'),
            Avg('hard_acceleration'),
            Avg('hard_braking'),
            Avg('turns'),
            Avg('right_turns'),
            Avg('left_turns'),
            Avg('frequent_braking'),
            Avg('lane_change'),
            Avg('speed_too_high_curve'),
            Avg('over_speeding')


        )

        #get the overspeeding values list to count the data


        print(overspeeding_data)
        overspeeding_list = []

        #check out the overspeeding loop
        for overspeeding in overspeeding_data:
            if overspeeding>0:
                overspeeding_list.append(0)
            else:
                overspeeding_list.append(100)

        overspeeding_avg = sum(overspeeding_list)/len(overspeeding_list)

        #return Response({"data":stats})
        time_in_traffic = Trip.objects.filter(user_id=request.user).filter(usertype='Driver').filter(
            created_date__date__range=(from_date, to_date)).values_list('time_in_traffic', flat=True)



        for day_night in time_in_traffic:
            print(day_night)
            if day_night == 'Early Morning':
                day_count += 1
            elif day_night == 'Morning':
                day_count += 1
            elif day_night == 'Afternoon':
                day_count += 1
            elif day_night == 'Evening':
                day_count += 1
            else:
                night_count += 1
            total_count += 1

        if total_count > 0:
            day_ratio = (day_count / total_count) * 100
            night_ratio = (night_count / total_count) * 100

        if stats:
            meter_speed = stats['speed_avg__avg']
            meter_speed = meter_speed * 20
            speed = round(meter_speed / 5)
            speed = format(round((stats['speed_avg__avg']*3.6), 1), '.1f')
            # score = rescale([
            #     stats['speed_limit__avg'],
            #     stats['phone_distraction__avg'],
            #     stats['hard_acceleration__avg'],
            #     stats['hard_braking__avg'],
            #     stats['turns__avg'],
            #     stats['right_turns__avg'],
            #     stats['left_turns__avg'],
            #     stats['frequent_braking__avg'],
            #     stats['lane_change__avg'],
            #     stats['speed_too_high_curve__avg'],
            #     stats['rainy_curve__avg']
            # ])
            return Response(
                {
                    'data': {
                        'distance': stats['distance__sum'],
                        'average_speed': str(speed),
                        'traffic': stats['time_in_traffic__sum'],
                        'day':day_ratio,
                        'night':night_ratio,
                        'speed_limit': stats['speed_limit__avg'],
                        'phone_distraction': stats['phone_distraction__avg'],
                        'hard_acceleration': stats['hard_acceleration__avg'],
                        'hard_braking': stats['hard_braking__avg'],
                        'turns': stats['turns__avg'],
                        'right_turns': stats['right_turns__avg'],
                        'left_turns': stats['left_turns__avg'],
                        'frequent_braking': stats['frequent_braking__avg'],
                        'lane_change': stats['lane_change__avg'],
                        'speed_too_high_curve': stats['speed_too_high_curve__avg'],
                        'over_speeding':overspeeding_avg
                    },
                    'Code': HTTP_200_OK,
                    "status": "success"
                },
                status=HTTP_200_OK
            )
        else:
            return Response(
                {
                    'data': None,
                    'Code': HTTP_200_OK,
                    "status": "success"
                },
                status=HTTP_200_OK
            )
    except ObjectDoesNotExist:
        return "No Record Found"


@csrf_exempt
@api_view(["GET"])
def pattern_analysis(request):
    d = date.today()
    first_day = get_first_day(d)
    last_day = get_last_day(d)
    print(first_day)
    print(last_day)
    try:
        speed_limit = Trip.objects.filter(user_id=request.user).filter(status=1).filter(
            created_date__date__range=(first_day, last_day)).values('speed_limit')
        distracted_driving = Trip.objects.filter(user_id=request.user).filter(status=1).filter(
            created_date__date__range=(first_day, last_day)).values('phone_distraction')
        hard_acceleration = Trip.objects.filter(user_id=request.user).filter(status=1).filter(
            created_date__date__range=(first_day, last_day)).values('hard_acceleration')
        hard_braking = Trip.objects.filter(user_id=request.user).filter(status=1).filter(
            created_date__date__range=(first_day, last_day)).values('hard_braking')
        sudden_turns = Trip.objects.filter(user_id=request.user).filter(status=1).filter(
            created_date__date__range=(first_day, last_day)).values('turns')
        right_turns = Trip.objects.filter(user_id=request.user).filter(status=1).filter(
            created_date__date__range=(first_day, last_day)).values('right_turns')
        left_turns = Trip.objects.filter(user_id=request.user).filter(status=1).filter(
            created_date__date__range=(first_day, last_day)).values('left_turns')
        frequent_braking = Trip.objects.filter(user_id=request.user).filter(status=1).filter(
            created_date__date__range=(first_day, last_day)).values('frequent_braking')
        sudden_lane_change = Trip.objects.filter(user_id=request.user).filter(status=1).filter(
            created_date__date__range=(first_day, last_day)).values('lane_change')
        speed_too_high_curve = Trip.objects.filter(user_id=request.user).filter(status=1).filter(
            created_date__date__range=(first_day, last_day)).values('speed_too_high_curve')
        over_speeding = Trip.objects.filter(user_id=request.user).filter(status=1).filter(
            created_date__date__range=(first_day, last_day)).values('over_speeding')
        #rainy_curve = Trip.objects.filter(user_id=request.user).filter(status=1).filter(
            #created_date__date__range=(first_day, last_day)).values('rainy_curve')
        return Response(
            {
                'data': {
                    'speed_limit': speed_limit,
                    'distracted_driving': distracted_driving,
                    'hard_acceleration': hard_acceleration,
                    'hard_braking': hard_braking,
                    'sudden_turns': sudden_turns,
                    'right_turns': right_turns,
                    'left_turns': left_turns,
                    'frequent_braking': frequent_braking,
                    'sudden_lane_change': sudden_lane_change,
                    'speed_too_high_curve': speed_too_high_curve,
                    'over_speeding':over_speeding
                    #'rainy_curve': rainy_curve
                },
                'Code': HTTP_200_OK,
                "status": "success"
            },
            status=HTTP_200_OK
        )
    except ObjectDoesNotExist:
        return "No Record Found"


def get_first_day(dt, d_years=0, d_months=0):
    # d_years, d_months are "deltas" to apply to dt
    y, m = dt.year + d_years, dt.month + d_months
    a, m = divmod(m - 1, 12)
    return date(y + a, m + 1, 1)


def get_last_day(dt):
    return get_first_day(dt, 0, 1)
    #return get_first_day(dt, 0, 1) + timedelta(-1)


@csrf_exempt
@api_view(["GET"])
@permission_classes((AllowAny,))
def manually_calculate(request):
    try:
        un_process_trips = Trip.objects.filter(id=32207).values()
        for trip in un_process_trips:
            print(trip["id"])
            # Trips Parameters
            # print('trip',trip)
            user_id = trip["user_id"]
            start_time = trip["trip_start_time"]
            end_time = trip["trip_end_time"]
            duration = end_time - start_time
            # Score Parameters
            time_in_traffic = 0
            # Events Parameters
            hard_acceleration = 100
            hard_acceleration_label = 0
            speed_limit = 100
            speed_limit_label = 0
            hard_braking = 100
            hard_braking_label = 0
            phone_distraction = 100
            phone_distraction_label = 0
            lane_change = 100
            lane_change_label = 0
            frequent_braking = 100
            frequent_braking_label = 0
            left_turn = 100
            left_turn_label = 0
            right_turn = 100
            right_turn_label = 0
            sudden_turns = 100
            sudden_turns_label = 0
            rainy_curve = 100
            rainy_curve_label = 0
            speed_too_high_curve = 100
            speed_too_high_curve_label = 0
            overspeeding = 0
            second = convert_to_second(duration)
            print('second')
            print(second)
            trip_deleted = 'NO'
            if second:
                if second < 30:
                    try:
                        tripData = Trip.objects.get(id=trip["id"])

                        # create trip log before delete

                        # create_trip_log(tripData)

                        # Delete Trip Data
                        # tripData.delete()

                        # Delete Sensor Data
                        # Sensordata.objects.filter(trip_id=trip["id"]).delete()
                        trip_deleted = 'YES'
                        print("Small Trips Are Deleted")
                    except ProtectedError:
                        print("Not able to Delete")

                # if sensor Data less than 1500 then remove trip
                try:
                    total_sensor_data = Sensordata.objects.filter(trip_id=trip["id"], type='Moving').count()
                    print('total_sensor_data - {}'.format(total_sensor_data))
                    # if total_sensor_data < 1000:
                    #    tripData = Trip.objects.get(id=trip["id"])
                    # create trip log before delete
                    # create_trip_log(tripData)

                    # Delete Trip Data
                    # tripData.delete()

                    #    trip_deleted = 'YES'
                    #    print("Small Trips Are Deleted")
                except:
                    print("Oops!  Sensor data not deleted.  Try again...")
                    pass

                # Delete trip if trip time greater than 2 minitues and sensor data less than 2000
                # trip_deleted = delete_2minitue_trip(second, trip["id"])
                # print('delete_2minitue_trip - {}'.format(trip_deleted))
                # Delete trip if trip time greater than 5 minitues and sensor data less than 3000
                # trip_deleted = delete_5minitue_trip(second, trip["id"])
                # print('delete_5minitue_trip - {}'.format(trip_deleted))
                if (trip_deleted == 'NO'):
                    # update trip flag
                    Trip.objects.filter(id=trip['id']).update(flag=True)

                    # Sensordata update user id, trip id - trip['id'], Trip

                    trip_end_date = start_time + timedelta(minutes=5)
                    trip_start_date = start_time - timedelta(minutes=10)

                    Sensordata.objects.filter(trip_id=0).filter(user_id=user_id).filter(
                        created_at__range=(trip_start_date, trip_end_date)).update(trip_id=trip['id'])

                    # Proccess sensordata
                    try:
                        user_stats = Sensordata.objects.filter(trip_id=trip["id"]).aggregate(
                            Avg('speed'),
                            Max('speed'),
                            Max('acc_magnitude'),
                            Avg('acc_magnitude'),
                            Avg('altitude')
                        )
                        speed_avg = user_stats['speed__avg']
                        speed_max = user_stats['speed__max']
                        acc_max = user_stats['acc_magnitude__max']
                        acc_avg = user_stats['acc_magnitude__avg']
                        slope = user_stats['altitude__avg']
                        sensor_records = Sensordata.objects.filter(trip_id=trip["id"]).values()
                        print("Trip Id :", trip["id"])
                        if sensor_records:
                            n = 10
                            sensor_value_median = []
                            phone_distraction = count_distracted_behaviour(sensor_records)
                            overspeeding = count_overspeeding(sensor_records)
                            final = [sensor_records[i * n:(i + 1) * n] for i in
                                     range((len(sensor_records) + n - 1) // n)]
                            for final_median in final:
                                median_acc_z = []
                                median_acc_y = []
                                median_acc_x = []
                                median_gyro_y = []
                                median_gyro_x = []
                                for acc in final_median:
                                    median_acc_z.append(float(acc["acc_z"]))
                                    median_acc_y.append(float(acc["acc_y"]))
                                    median_acc_x.append(float(acc["acc_x"]))
                                    median_gyro_y.append(float(acc["gyro_y"]))
                                    median_gyro_x.append(float(acc["gyro_x"]))
                                score_z = np.mean(median_acc_z)
                                score_y = np.mean(median_acc_y)
                                score_gy = np.mean(median_gyro_y)
                                score_gx = np.mean(median_gyro_x)
                                score_x = np.mean(median_acc_x)
                                sensor_value_median.append([score_x,score_y, score_z, score_gy, score_gx])
                                length = len(sensor_value_median)
                                # phone_distraction = distracted_driving(trip_id=trip['id'])

                            for test_value in sensor_value_median:
                                label = calculate_driving_score_new(test_value[0], test_value[1], test_value[2],
                                                                test_value[3], test_value[4])
                                if label == 'Sudden Turns':
                                    sudden_turns_label += 1
                                if label == 'Excessive Break':
                                    hard_braking_label += 1
                                if label == 'Aggressive Left Turn':
                                    left_turn_label += 1
                                if label == 'Aggressive Right Turn':
                                    right_turn_label += 1
                                if label == 'Aggressive Acceleration':
                                    hard_acceleration_label += 1
                                if label == 'Frequent Braking':
                                    frequent_braking_label += 1
                                if label == 'Lane Change':
                                    lane_change_label += 1
                                if label == 'Sudden Lane Change':
                                    lane_change_label += 1
                                if label == 'Distracted Driving':
                                    phone_distraction_label += 1

                            sudden_turns_label = (sudden_turns_label * 100) / length
                            hard_braking_label = (hard_braking_label * 100) / length
                            left_turn_label = (left_turn_label * 100) / length
                            right_turn_label = (right_turn_label * 100) / length
                            hard_acceleration_label = (hard_acceleration_label * 100) / length
                            frequent_braking_label = (frequent_braking_label * 100) / length
                            lane_change_label = (lane_change_label * 100) / length
                            phone_distraction_label = (phone_distraction_label * 100) / length

                            sudden_turns = sudden_turns - sudden_turns_label
                            hard_braking = hard_braking - hard_braking_label
                            # if (len(str(hard_braking)) > 5):
                            #     hard_braking = 0

                            left_turn = left_turn - left_turn_label
                            right_turn = right_turn - right_turn_label
                            hard_acceleration = hard_acceleration - hard_acceleration_label
                            frequent_braking = frequent_braking - frequent_braking_label
                            lane_change = lane_change - lane_change_label
                            # phone_distraction = phone_distraction - phone_distraction_label
                            speed_limit = speed_limit - speed_limit_label
                            rainy_curve = rainy_curve - rainy_curve_label
                            speed_too_high_curve = speed_too_high_curve - speed_too_high_curve_label
                            excellent_driving = (
                                                        sudden_turns + hard_braking + left_turn + right_turn + hard_acceleration + frequent_braking + lane_change + phone_distraction) / 8
                            risky_driving = 100 - excellent_driving
                            print('sudden_turns_label {}'.format(sudden_turns_label))
                            print('hard_braking_label {}'.format(hard_braking_label))
                            print('frequent_braking_label {}'.format(frequent_braking_label))
                            print('left_turn_label {}'.format(left_turn_label))

                            print('right_turn_label {}'.format(right_turn_label))
                            print('hard_acceleration_label {}'.format(hard_acceleration_label))
                            print('frequent_braking_label {}'.format(frequent_braking_label))
                            print('lane_change_label {}'.format(lane_change_label))

                            print('phone_distraction_label {}'.format(phone_distraction_label))
                            print('sudden_turns {}'.format(sudden_turns))
                            print('hard_braking {}'.format(hard_braking))
                            print('left_turn {}'.format(left_turn))

                            print('right_turn {}'.format(right_turn))
                            print('hard_acceleration {}'.format(hard_acceleration))
                            print('frequent_braking {}'.format(frequent_braking))
                            print('lane_change {}'.format(lane_change))

                            print('phone_distraction {}'.format(phone_distraction))
                            print('speed_limit {}'.format(speed_limit))
                            print('rainy_curve {}'.format(rainy_curve))
                            print('speed_too_high_curve {}'.format(speed_too_high_curve))
                            print("Excellent", excellent_driving)
                            print("Risky", risky_driving)
                            print("slope {}".format(slope))
                            fair = 0
                            # distance = (second * speed_avg) * 0.001
                            df = pd.DataFrame(sensor_records)
                            for col in df.columns:
                                print(col)
                            print(df.values.shape)

                            df = df.sort_values(by=['timeInMillis'])
                            df = df.loc[:, ['latitude', 'longitude']]
                            # df.columns= ['Latitude', 'Longitude']
                            distance = 0
                            lat = np.array(df.iloc[:, 0])
                            longa = np.array(df.iloc[:, 1])
                            l1 = []
                            l2 = []
                            for i in range(len(lat) - 1):
                                if lat[i] not in l1 and longa[i] not in l2:
                                    if lat[i] != '0' and longa[i] != '0':
                                        l1.append(lat[i])
                                        l2.append(longa[i])
                            # print(type(l1[3]))
                            # print('------')
                            # print(l2)
                            for i in range(0, len(l1) - 1):
                                cords1 = (l1[i], l2[i])
                                cords2 = (l1[i + 1], l2[i + 1])
                                distance = distance + geopy.distance.vincenty(cords1, cords2).km
                            print('(1): ' + str(distance) + 'km')
                            # if slope data length greater the 5
                            slope = round(float(slope), 2)
                            if (len(str(slope)) > 5):
                                slope = 0
                            user_type = analysis_unmoving(trip_id=trip['id'])
                            # phone_distracted = distracted_driving(trip_id=trip['id'])
                            time_in_traffic = total_time_in_traffic(trip_id=trip['id'])
                            # try:
                            #     Trip.objects.filter(id=trip['id']).update(
                            #         distance=distance,
                            #         duration=str(duration),
                            #         time_in_traffic=str(time_in_traffic),
                            #         excellent_driving=excellent_driving,
                            #         fair_driving=fair,
                            #         risky_driving=risky_driving,
                            #         speed_avg=speed_avg,
                            #         speed_max=speed_max,
                            #         acceleration_max=acc_max,
                            #         acceleration_avg=acc_avg,
                            #         road_slope=slope,
                            #         turns=sudden_turns,
                            #         hard_acceleration=hard_acceleration,
                            #         hard_braking=hard_braking,
                            #         lane_change=lane_change,
                            #         phone_distraction=phone_distraction,
                            #         # phone_distraction = phone_distracted,
                            #         left_turns=left_turn,
                            #         right_turns=right_turn,
                            #         speed_limit=speed_limit,
                            #         frequent_braking=frequent_braking,
                            #         rainy_curve=rainy_curve,
                            #         speed_too_high_curve=speed_too_high_curve,
                            #         status=True,
                            #         usertype=user_type
                            #     )
                            #     print('-----------Trip generated------------')
                            # # except ValidationError as e:
                            # except:
                            #     print("Oops!", sys.exc_info()[0], "occured.")

                            #    return sys.exc_info()[0]
                    except Sensordata.DoesNotExist:
                        return 'Does Not exist'
            else:
                tripData = Trip.objects.get(id=trip["id"])
                # Trip.objects.filter(id=trip['id']).update(
                #                    status=True
                #                    )

                # create trip log before delete
                # create_trip_log(tripData)

                # Delete Trip Data
                # tripData.delete()

        return Response(
            {
                'data': un_process_trips,
                'code': HTTP_200_OK
            },
            status=HTTP_200_OK
        )
    except Trip.DoesNotExist:
        un_process_trips = None
        print(un_process_trips)


@periodic_task(run_every=(crontab(minute='*/5')), name="Trip_analysis", ignore_result=True)
def analysis():
    try:
        print('Starting to print')
        un_process_trips = Trip.objects.filter(finished=1, status=0, flag=0).filter(trip_end_time__isnull=False).values()
        for trip in un_process_trips:
            print(trip["id"])
            # Trips Parameters
            # print('trip',trip)
            version_code = trip['app_version_name']
            user_id = trip["user_id"]
            start_time = trip["trip_start_time"]
            end_time = trip["trip_end_time"]
            duration = end_time - start_time
            # Score Parameters
            time_in_traffic = 0
            # Events Parameters
            acceleration_label = 0
            braking_label = 0
            turn_label = 0
            hard_acceleration = 100
            hard_acceleration_label = 0
            speed_limit = 100
            speed_limit_label = 0
            hard_braking = 100
            hard_braking_label = 0
            phone_distraction = 100
            phone_distraction_label = 0
            lane_change = 100
            lane_change_label = 0
            frequent_braking = 100
            frequent_braking_label = 0
            left_turn = 100
            left_turn_label = 0
            right_turn = 100
            right_turn_label = 0
            sudden_turns = 100
            sudden_turns_label = 0
            rainy_curve = 100
            rainy_curve_label = 0
            speed_too_high_curve = 100
            speed_too_high_curve_label = 0
            accident = 100
            accident_label = 0
            overspeeding = 0
            list_acc = []
            list_braking = []
            list_lane_change = []
            list_turn = []
            list_other = []
            simple_list_turn = []
            simple_list_acc = []
            simple_list_brake = []
            simple_list_lane_change =[]
            list_freq_brake = []
            list_left_turn = []
            list_right_turn = []
            sudden_lane_change_label = 0
            list_speed = []
            check_gyro_available = 1


            event_acc = 0
            event_braking = 0
            event_lane_change = 0
            event_turn = 0
            event_other = 0
            event_simple_acc = 0
            event_simple_brake = 0
            event_simple_lane_change = 0
            event_freq_brake = 0
            event_simple_turn = 0
            event_left_turn = 0
            event_right_turn = 0

            percentage_score_acc = 0
            percentage_score_turn = 0
            percentage_score_braking = 0
            percentage_score_lange_change = 0
            percentage_score_other = 0
            pecentage_score_left_turn=0
            percentage_score_right_turn=0
            speed_label=0

            second = convert_to_second(duration)
            print('second')
            print(second)
            trip_deleted = 'NO'
            if second:
                if second < 30:
                    try:
                        tripData = Trip.objects.get(id=trip["id"])

                        # create trip log before delete

                        # create_trip_log(tripData)

                        # Delete Trip Data
                        # tripData.delete()

                        # Delete Sensor Data
                        # Sensordata.objects.filter(trip_id=trip["id"]).delete()
                        trip_deleted = 'YES'
                        print("Small Trips Are Deleted")
                    except ProtectedError:
                        print("Not able to Delete")

                # if sensor Data less than 1500 then remove trip
                try:
                    total_sensor_data = Sensordata.objects.filter(trip_id=trip["id"], type='Moving').count()
                    print('total_sensor_data - {}'.format(total_sensor_data))
                    # if total_sensor_data < 1000:
                    #    tripData = Trip.objects.get(id=trip["id"])
                    # create trip log before delete
                    # create_trip_log(tripData)

                    # Delete Trip Data
                    # tripData.delete()

                    #    trip_deleted = 'YES'
                    #    print("Small Trips Are Deleted")
                except:
                    print("Oops!  Sensor data not deleted.  Try again...")
                    pass


                if (trip_deleted == 'NO'):
                    # update trip flag
                    Trip.objects.filter(id=trip['id']).update(flag=True)

                    trip_end_date = start_time + timedelta(minutes=5)
                    trip_start_date = start_time - timedelta(minutes=10)

                    Sensordata.objects.filter(trip_id=0).filter(user_id=user_id).filter(
                        created_at__range=(trip_start_date, trip_end_date)).update(trip_id=trip['id'])

                    try:
                        user_stats = Sensordata.objects.filter(trip_id=trip["id"]).aggregate(
                            Avg('speed'),
                            Max('speed'),
                            Max('acc_magnitude'),
                            Avg('acc_magnitude'),
                            Avg('altitude')
                        )
                        speed_avg = user_stats['speed__avg']
                        speed_max = user_stats['speed__max']
                        acc_max = user_stats['acc_magnitude__max']
                        acc_avg = user_stats['acc_magnitude__avg']
                        slope = user_stats['altitude__avg']

                        sensor_records = Sensordata.objects.filter(trip_id=trip["id"]).order_by('-id').values()
                        length = Sensordata.objects.filter(trip_id=trip["id"]).count()
                        print("Trip Id :", trip["id"])
                        if sensor_records:
                            n = 10
                            sensor_value_median = []

                            for sensor_record in sensor_records:
                                label = calculate_driving_score(sensor_record['acc_x'], sensor_record['acc_y'], sensor_record['acc_z'], sensor_record['gyro_x'], sensor_record['gyro_y'], sensor_record['gyro_z'])
                                if label == 'Sudden Turns':
                                    list_turn.append(sensor_record['created_at'])
                                    sudden_turns_label += 1
                                if label == 'Excessive Break':
                                    list_braking.append(sensor_record['created_at'])
                                    hard_braking_label += 1
                                if label == 'Aggressive Left Turn':
                                    list_left_turn.append(sensor_record['created_at'])
                                    left_turn_label += 1
                                if label == 'Aggressive Right Turn':
                                    list_right_turn.append(sensor_record['created_at'])
                                    right_turn_label += 1
                                if label == 'Aggressive Acceleration':
                                    list_acc.append(sensor_record['created_at'])
                                    hard_acceleration_label += 1
                                if label == 'Frequent Braking':
                                    list_freq_brake.append(sensor_record['created_at'])
                                    frequent_braking_label += 1
                                if label == 'Lane Change':
                                    simple_list_lane_change.append(sensor_record['created_at'])
                                    lane_change_label += 1
                                if label == 'Sudden Lane Change':
                                    list_lane_change.append(sensor_record['created_at'])
                                    sudden_lane_change_label += 1
                                if label == 'Distracted Driving':
                                    list_other.append(sensor_record['created_at'])
                                    phone_distraction_label += 1
                                if label == 'Accident':
                                    accident_label += 1
                                if label =='Acceleration':
                                    simple_list_acc.append(sensor_record['created_at'])
                                    acceleration_label+=1
                                if label=='Turns':
                                    simple_list_turn.append(sensor_record['created_at'])
                                    turn_label+=1
                                if label == 'Break':
                                    simple_list_brake.append(sensor_record['created_at'])
                                    braking_label+=1
                                if label == 'Accident':
                                    #send_message(user_id, sensor_record['latitude'], sensor_record['longitude'])
                                    accident_label+=1
                                if label== 'Speed to high curve':
                                    list_speed.append(sensor_record['created_at'])
                                    speed_label+=1

                            event_lane_change = get_event_sudden(list_lane_change)
                            event_acc = get_event(list_acc)
                            event_turn = get_event_sudden(list_turn)
                            event_braking = get_event(list_braking)
                            event_other = get_event(list_other)
                            event_simple_acc = get_event(simple_list_acc)
                            event_simple_brake = get_event(simple_list_brake)
                            event_simple_lane_change = get_event(simple_list_lane_change)
                            event_freq_brake = get_event_sudden(list_freq_brake)
                            event_simple_turn = get_event(simple_list_turn)
                            event_left_turn = get_event(list_left_turn)
                            event_right_turn = get_event(list_right_turn)
                            event_speed = get_event(list_speed)


                            phone_distraction_data = count_distracted_behaviour(sensor_records) #analysis
                            phone_distraction = phone_distraction_data[0]

                            overspeeding_value = count_overspeeding(sensor_records)
                            if overspeeding_value == 0:
                                overspeeding = 100

                            #check out the if gyro is available or not
                            check_gyro_available = Trip.objects.filter(id=trip['id'] ).values_list('is_gyro_available', flat=True)
                            print("check gyro part of the here")
                            print(check_gyro_available[0])
                            if not check_gyro_available[0]:
                                records = get_record_turn(sensor_records)
                                sudden_turns_label = records['sudden_turn']
                                turn_label = records['turn']
                                left_turn_label = records['left_turn']
                                right_turn_label = records['right_turn']
                                lane_change_label = records['lane_change']
                                sudden_lane_change_label = records['sudden_lane']
                                speed_label = records['speed_to_high']
                                phone_distraction_data = count_distracted_behaviour_without_gyro(sensor_records)
                                phone_distraction=phone_distraction_data[0]

                            percentage_score_acc = (1-(hard_acceleration_label/(acceleration_label+hard_acceleration_label+1)))*100
                            percentage_score_turn = (1-(sudden_turns_label/(turn_label+sudden_turns_label+left_turn_label+right_turn_label+1)))*100
                            percentage_score_braking = (1-(hard_braking_label/(braking_label+hard_braking_label+frequent_braking_label+1)))*100
                            percentage_score_lane_change = (1-(sudden_lane_change_label/(lane_change_label+sudden_lane_change_label+1)))*100
                            percentage_score_right_turn = (1-(right_turn_label/(turn_label+sudden_turns_label+left_turn_label+right_turn_label+1)))*100
                            percentage_score_left_turn = (1-(left_turn_label/(turn_label+sudden_turns_label+left_turn_label+right_turn_label+1)))*100
                            percentage_speed = (1-(speed_label/speed_label+phone_distraction_label))


                            trip_traffic_data = str(total_time_in_traffic_new(trip_id=trip['id']))

                            scores = DssWeight.objects.filter(name=trip_traffic_data).values()
                            print("SCORES {}".format(scores))

                            weight_score_acc = scores[0]['weight_acc']
                            weight_score_turn = scores[0]['weight_turn']
                            weight_score_braking = scores[0]['weight_braking']
                            weight_score_lane_change = scores[0]['weight_lane_change']
                            weight_score_other = scores[0]['weight_other']

                            total_dss = (float(percentage_score_acc) * float(weight_score_acc)) + (float(percentage_score_braking) * float(weight_score_braking)) + (float(percentage_score_turn) * float(weight_score_turn)) + (float(percentage_score_lane_change) * float(weight_score_lane_change)) + (float(phone_distraction) * float(weight_score_other))
                            print("TOTAL DSS {}".format(total_dss))

                            excellent_driving  = total_dss
                            risky_driving = 100 - excellent_driving

                            fair = 0
                            df = pd.DataFrame(sensor_records)
                            for col in df.columns:
                                print(col)
                            print(df.values.shape)

                            df = df.sort_values(by=['timeInMillis'])
                            df = df.loc[:, ['latitude', 'longitude', 'accuracy']]
                            distance = 0
                            lat = np.array(df.iloc[:, 0])
                            longa = np.array(df.iloc[:, 1])
                            accuracy = np.array(df.iloc[:, 2])
                            l1 = []
                            l2 = []
                            for i in range(len(lat) - 1):
                                if lat[i] not in l1 and longa[i] not in l2:
                                    if lat[i] != '0' and longa[i] != '0' and float(accuracy[i])<50:
                                        l1.append(lat[i])
                                        l2.append(longa[i])

                            for i in range(0, len(l1) - 1):
                                cords1 = (l1[i], l2[i])
                                cords2 = (l1[i + 1], l2[i + 1])
                                distance = distance + geopy.distance.vincenty(cords1, cords2).km
                            print('(1): ' + str(distance) + 'km')
                            # if slope data length greater the 5
                            print("This is the slope value of the 32784....................")
                            print(slope)

                            slope = round(float(slope), 1)
                            if len(str(slope)) > 5:
                                slope = 0
                            slope_value = slope
                            print("This is the slope value.....")
                            print(slope_value)

                            user_type = analysis_unmoving(trip_id=trip['id'])
                            time_in_traffic = total_time_in_traffic_new(trip_id= trip['id'])

                            braking_event = event_simple_brake + event_braking+1
                            acc_event = event_simple_acc+event_acc+1
                            frequent_braking = get_frequent_braking(braking_event, acc_event)

                            try:
                                Trip.objects.filter(id=trip['id']).update(
                                    distance=distance,
                                    duration=str(duration),
                                    time_in_traffic=str(time_in_traffic),
                                    excellent_driving=total_dss,
                                    fair_driving=fair,
                                    risky_driving=risky_driving,
                                    speed_avg=speed_avg,
                                    speed_max=speed_max,
                                    acceleration_max=acc_max,
                                    acceleration_avg=acc_avg,
                                    road_slope=slope_value,
                                    turns=percentage_score_turn,
                                    hard_acceleration=percentage_score_acc,
                                    hard_braking=percentage_score_braking,
                                    lane_change=percentage_score_lane_change,
                                    phone_distraction=phone_distraction,
                                    road_type='Highway' if speed_avg>=40 else 'Non-Highway',
                                    left_turns=percentage_score_left_turn,
                                    right_turns=percentage_score_right_turn,
                                    speed_limit=speed_limit,
                                    frequent_braking=frequent_braking,
                                    speed_too_high_curve=percentage_speed,
                                    status=True,
                                    usertype=user_type,
                                    event_lane_change=event_simple_lane_change + event_lane_change + 1,
                                    event_acc=event_simple_acc + event_acc + 1,
                                    event_turn=event_simple_turn + event_left_turn + event_right_turn + 1,
                                    event_braking=event_simple_brake + event_braking + 1,
                                    event_other = phone_distraction_data[1],
                                    events_acc_hard=event_acc,
                                    events_freq_breaking=event_freq_brake,
                                    events_excesive_breaking=event_braking,
                                    event_sudden_lane_change=event_lane_change,
                                    event_sudden_turn=event_turn,
                                    over_speeding = overspeeding
                                )


                                print('-----------Trip generated------------')
                            # except ValidationError as e:
                            except:

                                print("Oops!", sys.exc_info()[0], "occured.")

                                return sys.exc_info()[0]
                    except Sensordata.DoesNotExist:
                        return 'Does Not exist'
            else:
                tripData = Trip.objects.get(id=trip["id"])
                # Trip.objects.filter(id=trip['id']).update(
                #                    status=True
                #                    )

                # create trip log before delete
                # create_trip_log(tripData)

                # Delete Trip Data
                # tripData.delete()

        return Response(
            {
                'data': un_process_trips,
                'code': HTTP_200_OK
            },
            status=HTTP_200_OK
        )
    except Trip.DoesNotExist:
        un_process_trips = None
        print(un_process_trips)


def delete_2minitue_trip(second, trip_id):
    """
    Delete trip if trip time greater than 2 minitues and sensor data less than 2000
    """

    trip_deleted = 'NO'

    try:
        total_sensor_data = Sensordata.objects.filter(trip_id=trip_id, type='Moving').count()
        print('total_sensor_data - {}'.format(total_sensor_data))

        if second > 120 and total_sensor_data < 2000:
            tripData = Trip.objects.get(id=trip_id)
            # create trip log before delete
            create_trip_log(tripData)

            # Delete Trip Data
            tripData.delete()

            trip_deleted = 'YES'
            print("Small Trips Are Deleted")
            return trip_deleted
        return trip_deleted
    except:
        print("Oops!  Sensor data not deleted.  Try again...")
        return trip_deleted


def delete_5minitue_trip(second, trip_id):
    """
    Delete trip if trip time greater than 2 minitues and sensor data less than 2000
    """

    trip_deleted = 'NO'

    try:
        total_sensor_data = Sensordata.objects.filter(trip_id=trip_id, type='Moving').count()
        print('total_sensor_data - {}'.format(total_sensor_data))

        if second > 300 and total_sensor_data < 3000:
            tripData = Trip.objects.get(id=trip_id)
            # create trip log before delete
            create_trip_log(tripData)

            # Delete Trip Data
            tripData.delete()

            trip_deleted = 'YES'
            print("Small Trips Are Deleted")
            return trip_deleted
        return trip_deleted
    except:
        print("Oops!  Sensor data not deleted.  Try again...")
        return trip_deleted


"""
Delete small trip 
"""


def create_trip_log(tripData):
    DeletedTrip.objects.create(
        trip_id=tripData.id,
        trip_start_time=tripData.trip_start_time,
        trip_end_time=tripData.trip_end_time,
        trip_created_date=tripData.created_date,
        usertype=tripData.usertype,
        user_id=tripData.user_id
    )


def calculate_driving_score(acc_x, acc_y, acc_z, gyro_x, gyro_y, gyro_z):
    try:
        test = [[acc_x, acc_y, acc_z, gyro_x, gyro_y, gyro_z]]

        label = model.predict(test)

        # print('Predicted', label)
        return label
    except FileNotFoundError:
        return 'File Not found'


def convert_to_second(duration):
    duration = str(duration)
    try:
        x = sum(x * int(float(t)) for x, t in zip([3600, 60, 1], duration.split(":")))
        return x
    except ValueError:
        # print('Non-numeric data found in the file.')
        return None


@csrf_exempt
@api_view(["GET"])
@permission_classes((AllowAny,))
def calculate_distance(request):
    un_process_trips = Trip.objects.filter(distance=0.0).values()
    for trip in un_process_trips:
        second = convert_to_second(trip["duration"])
        if second:
            distance = (second * float(trip['speed_avg'])) * 0.001
            Trip.objects.filter(id=trip['id']).update(
                distance=distance,
            )
    return Response(
        {
            'done': 'Successful',
        },
        status=HTTP_400_BAD_REQUEST
    )


@csrf_exempt
@api_view(["GET"])
@permission_classes((AllowAny,))
def calculate_ranks(request):
    users = Users.objects.filter(~Q(id=1)).values('id')
    for user in users:
        try:
            score = Trip.objects.filter(user_id=user['id']).filter(status=1).aggregate(
                Avg('excellent_driving'),
            )
            score_avg = score['excellent_driving__avg']
            if score_avg is not None:
                if score_avg > 70:
                    style = 'Excellent'
                elif 70 > score_avg > 50:
                    style = 'Fair'
                else:
                    style = 'Risky'

                Users.objects.filter(id=user['id']).update(
                    score=score['excellent_driving__avg'],
                    styled=style
                )
        except ValidationError as e:
            return Response(
                {
                    'error': e,
                    'code': HTTP_400_BAD_REQUEST
                },
                status=HTTP_400_BAD_REQUEST
            )
    return Response(
        {
            'Message': 'Score Calculated',
            'code': HTTP_200_OK
        },
        status=HTTP_200_OK
    )


@csrf_exempt
@api_view(['POST'])
@permission_classes((AllowAny,))
def trip_detail(request):
    trip_id = request.data.get('trip_id', '')
    if not trip_id:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Trip id is missing"
            },
            status=HTTP_400_BAD_REQUEST
        )

    #check out the trip_id if it is district roadd
    current_trip_details = Trip.objects.filter(id=trip_id).values().first()
    if current_trip_details['road_type']=='District Road':
        current_trip_details['road_type'] = 'Highway'

    #change the speed_avg and speed max from m/s to km/hr
    current_trip_details['speed_avg'] = round(3.6*float(current_trip_details['speed_avg']),2)
    current_trip_details['speed_max'] = round(3.6*float(current_trip_details['speed_max']),2)

    speed_graph = Sensordata.objects.filter(trip_id=trip_id).values_list('speed', flat=True).distinct()
    turn_events = Sensordata.objects.filter(trip_id=trip_id).values_list('gyro_y', flat=True)
    acceleration_events = Sensordata.objects.filter(trip_id=trip_id).values_list('acc_magnitude', flat=True).distinct()

    speed_graph = list(speed_graph)
    turn_events = list(turn_events)
    acceleration_events = list(acceleration_events)

    if (len(speed_graph) > 20):
        speed_graph = random.sample(speed_graph, 20)
    if (len(turn_events) > 20):
        turn_events = random.sample(turn_events, 20)
    if (len(acceleration_events) > 20):
        acceleration_events = random.sample(acceleration_events, 20)

    del current_trip_details['highway']
    if trip_details:
        return Response(
            {
                'data': {
                    'trip_details': current_trip_details,
                    'speed_graph': speed_graph,
                    'acceleration_events': acceleration_events,
                    'turn_events': turn_events,
                },
                'Code': HTTP_200_OK,
                "status": "success"
            },
            status=HTTP_200_OK
        )


@csrf_exempt
@api_view(["POST"])
def get_all_trips(request):
    user_id = request.data.get("userId", "")
    if not user_id:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "User Id Required"
            },
            status=HTTP_400_BAD_REQUEST
        )
    try:
        trips = Trip.objects.filter(user_id=user_id).filter(status=1).values()
        stats = Trip.objects.filter(user_id=user_id).filter(status=1).aggregate(
            Sum('distance'),
            Avg('speed_avg'),
            Avg('excellent_driving'),
            Avg('risky_driving'),
            Sum('duration'),
            Avg('fair_driving'),
            Avg('speed_avg'),
        )
        speed_limit = Trip.objects.filter(user_id=user_id).filter(status=1).values_list('speed_limit', flat=True)
        distracted_driving = Trip.objects.filter(user_id=user_id).filter(status=1).values_list('phone_distraction',
                                                                                               flat=True)
        hard_acceleration = Trip.objects.filter(user_id=user_id).filter(status=1).values_list('hard_acceleration',
                                                                                              flat=True)
        hard_braking = Trip.objects.filter(user_id=user_id).filter(status=1).values_list('hard_braking', flat=True)
        sudden_turns = Trip.objects.filter(user_id=user_id).filter(status=1).values_list('turns', flat=True)
        right_turns = Trip.objects.filter(user_id=user_id).filter(status=1).values_list('right_turns', flat=True)
        left_turns = Trip.objects.filter(user_id=user_id).filter(status=1).values_list('left_turns', flat=True)
        frequent_braking = Trip.objects.filter(user_id=user_id).filter(status=1).values_list('frequent_braking',
                                                                                             flat=True)
        sudden_lane_change = Trip.objects.filter(user_id=user_id).filter(status=1).values_list('lane_change', flat=True)
        speed_too_high_curve = Trip.objects.filter(user_id=user_id).filter(status=1).values_list('speed_too_high_curve',
                                                                                                 flat=True)
        #rainy_curve = Trip.objects.filter(user_id=user_id).filter(status=1).values_list('rainy_curve', flat=True)
        if stats:
            meter_speed = stats['speed_avg__avg']
            if meter_speed:
                meter_speed = meter_speed * 20
                speed = round(meter_speed / 5)
            else:
                speed = 0

            return Response(
                {
                    'data': {
                        'distance': stats['distance__sum'],
                        'average_speed': str(speed),
                        'excellent_driving': stats['excellent_driving__avg'],
                        'risky_driving': stats['risky_driving__avg'],
                        'duration': stats['duration__sum'],
                        'fair_driving': stats['fair_driving__avg'],
                        'speed_avg': stats['speed_avg__avg'],
                        'trips': trips,
                        'patterns': {
                            'speed_limit': speed_limit,
                            'distracted_driving': distracted_driving,
                            'hard_acceleration': hard_acceleration,
                            'hard_braking': hard_braking,
                            'sudden_turns': sudden_turns,
                            'right_turns': right_turns,
                            'left_turns': left_turns,
                            'frequent_braking': frequent_braking,
                            'sudden_lane_change': sudden_lane_change,
                            'speed_too_high_curve': speed_too_high_curve,
                            #'rainy_curve': rainy_curve
                        }
                    },
                    'Code': HTTP_200_OK,
                    "status": "success"
                },
                status=HTTP_200_OK
            )
        else:
            return Response(
                {
                    'data': [],
                    'Code': HTTP_200_OK,
                    "status": "success",
                    "message": " No Trips Details Found"
                },
                status=HTTP_200_OK
            )
    except ObjectDoesNotExist:
        return "No Record Found"


@periodic_task(run_every=(crontab(minute='*/59')), name="calculate_rank", ignore_result=True)
def calculate_rank():
    users = Users.objects.filter(~Q(id=1)).values('id')
    for user in users:
        try:
            score = Trip.objects.filter(user_id=user['id']).filter(status=1).aggregate(
                Avg('excellent_driving'),
            )
            score_avg = score['excellent_driving__avg']
            if score_avg:
                if score_avg > 75:
                    style = 'Excellent'
                elif 75 > score_avg > 50:
                    style = 'Fair'
                else:
                    style = 'Risky'

                Users.objects.filter(id=user['id']).update(
                    score=score['excellent_driving__avg'],
                    styled=style
                )
        except ValidationError as e:
            return e


@csrf_exempt
@api_view(['POST'])
def trip_more_details_web(request):
    category = []
    trip_id = request.data.get('trip_id', '')
    if not trip_id:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Trip id is missing"
            },
            status=HTTP_400_BAD_REQUEST
        )

    current_trip_details = Trip.objects.filter(id=trip_id).values(
        'trip_start_time',
        'trip_end_time',
        'distance',
        'duration',
        'highway',
        # 'non_highway_inside_city',
        # 'non_highway_outside_city',
        #'rural',
        'excellent_driving',
        'risky_driving'
    )
    categories = Trip.objects.filter(id=trip_id).annotate(
        Acceleration=F('acceleration'),
        Braking=F('hard_braking'),
        Lanechange=F('lane_change'),
        Turns=F('turns'),
        OverSpeeding=F('over_speeding'),
        Distraction=F('phone_distraction')
    ).values(
        'Acceleration',
        'Braking',
        'Lanechange',
        'Turns',
        'OverSpeeding',
        'Distraction'
    )
    if categories:
        for json_dict in categories:
            for key, value in json_dict.items():
                color = '#006400'
                if key == 'Acceleration':
                    color = '#FF0000'
                if key == 'Braking':
                    color = '#000000'
                if key == 'Lanechange':
                    color = '#FFFF00'
                if key == 'Turns':
                    color = '#00CED1'
                if key == 'OverSpeeding':
                    color = '#00008B'
                if key == 'Distraction':
                    color = '#FF4500'

                cat = {
                    'category': key,
                    'score': value,
                    'color': color
                }
                category.append(cat)
    graph = Sensordata.objects.filter(trip_id=trip_id).values('latitude', 'longitude')
    # result = json.dumps(([x['latitude'], x['latitude']] for x in graph), indent=2)
    # encoded_polyline = polyline.encode([(38.5, -120.2), (40.7, -120.9), (43.2, -126.4)])
    if trip_details:
        return Response(
            {
                'data': {
                    'trip_details': current_trip_details[0],
                    'category': category,
                    'graph': graph,
                },
                'Code': HTTP_200_OK,
                "status": "success"
            },
            status=HTTP_200_OK
        )


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def smart_report_old(request):
    from_date = request.data.get("from_date", "")
    to_date = request.data.get("to_date", "")
    user_id = request.data.get("user_id", "")
    report_type = request.data.get("type", "")
    email_type = request.data.get("email", "")
    if not report_type:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Type Required"
            },
            status=HTTP_400_BAD_REQUEST
        )
    if type == 'Smart':
        if not from_date:
            return Response(
                {
                    "Code": HTTP_400_BAD_REQUEST,
                    "status": "error",
                    "message": "Please Enter Start Date and Time"
                },
                status=HTTP_400_BAD_REQUEST
            )
        if not to_date:
            return Response(
                {
                    "Code": HTTP_400_BAD_REQUEST,
                    "status": "error",
                    "message": "Please Enter To Date and Time"
                },
                status=HTTP_400_BAD_REQUEST
            )
        else:
            d = date.today()
            from_date = get_first_day(d)
            to_date = get_last_day(d)
    if not id:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "User ID Required"
            },
            status=HTTP_400_BAD_REQUEST
        )
    try:
        user = Users.objects.filter(id=user_id).values()
        stats = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).aggregate(
            speed=Round(Avg('speed_avg')),
            total_distance=Round(Sum('distance')),
            timein_traffic=Round(Max('time_in_traffic')),
            excelent_driving=Round(Avg('excellent_driving')),
            fair_driving=Round(Avg('fair_driving')),
            risky_driving=Round(Avg('risky_driving'))
        )
        if stats:
            today = timezone.now()
            params = {
                'today': today,
                'users': user,
                'record': stats,
                'from': from_date,
                'to': to_date,
                'type': report_type,
                'email_type': email_type
            }
            return Render.render('smartreport.html', params)
        else:
            return Response(
                {
                    'data': [],
                    'Code': HTTP_200_OK,
                    "status": "success"
                },
                status=HTTP_200_OK
            )
    except ObjectDoesNotExist:
        return "No Record Found"


@csrf_exempt
@api_view(["GET"])
@permission_classes((AllowAny,))
def awareness_report(self):
    today = timezone.now()
    params = {
        'today': today,
    }
    return Render.render_to('awareness-report.html', params)


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def admin_report_old(request):
    from_date = request.data.get("from_date", "")
    to_date = request.data.get("to_date", "")
    user_id = request.data.get("user_id", "")
    if not from_date:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter Start Date and Time"
            },
            status=HTTP_400_BAD_REQUEST
        )
    if not to_date:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter To Date and Time"
            },
            status=HTTP_400_BAD_REQUEST
        )
    if not id:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "User ID Required"
            },
            status=HTTP_400_BAD_REQUEST
        )
    try:
        user = Users.objects.filter(id=user_id).values()
        stats = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).aggregate(
            speed=Round(Avg('speed_avg')),
            total_distance=Round(Sum('distance')),
            timein_traffic=Round(Max('time_in_traffic')),
            excelent_driving=Round(Avg('excellent_driving')),
            fair_driving=Round(Avg('fair_driving')),
            risky_driving=Round(Avg('risky_driving'))
        )
        speed_limit = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).values('speed_limit')
        distracted_driving = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).values('phone_distraction')
        hard_acceleration = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).values('hard_acceleration')
        hard_braking = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).values('hard_braking')
        sudden_turns = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).values('turns')
        right_turns = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).values('right_turns')
        left_turns = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).values('left_turns')
        frequent_braking = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).values('frequent_braking')
        sudden_lane_change = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).values('lane_change')
        speed_too_high_curve = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).values('speed_too_high_curve')
        rainy_curve = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).values('rainy_curve')
        if stats:
            today = timezone.now()
            params = {
                'today': today,
                'users': user,
                'record': stats,
                'from': from_date,
                'to': to_date,
                "email_type": "No"
            }
            return Render.render("driver-risk-report.html", params)
        else:
            return Response(
                {
                    'data': [],
                    'Code': HTTP_200_OK,
                    "status": "success"
                },
                status=HTTP_200_OK
            )
    except ObjectDoesNotExist:
        return "No Record Found"


# @csrf_exempt
# @api_view(["POST"])
# @permission_classes((AllowAny,))
# def admin_report(request):
#     from_date = request.data.get("from_date", "")
#     to_date = request.data.get("to_date", "")
#     user_id = request.data.get("user_id", "")  # 35
#
#     print("dates:=> ", from_date, to_date)
#     if not from_date:
#         return Response(
#             {
#                 "Code": HTTP_400_BAD_REQUEST,
#                 "status": "error",
#                 "message": "Please Enter Start Date and Time"
#             },
#             status=HTTP_400_BAD_REQUEST
#         )
#     if not to_date:
#         return Response(
#             {
#                 "Code": HTTP_400_BAD_REQUEST,
#                 "status": "error",
#                 "message": "Please Enter To Date and Time"
#             },
#             status=HTTP_400_BAD_REQUEST
#         )
#     if not id:
#         return Response(
#             {
#                 "Code": HTTP_400_BAD_REQUEST,
#                 "status": "error",
#                 "message": "User ID Required"
#             },
#             status=HTTP_400_BAD_REQUEST
#         )
#     try:
#         ## getting sum and average for smart report
#         stats = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
#             created_date__date__range=(from_date, to_date)).aggregate(
#             speed=Round(Avg('speed_avg')),
#             total_distance=Round(Sum('distance')),
#             timein_traffic=Round(Max('time_in_traffic')),
#             excelent_driving=Round(Avg('excellent_driving')),
#             fair_driving=Round(Avg('fair_driving')),
#             risky_driving=Round(Avg('risky_driving'))
#         )
#
#         style = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
#             created_date__date__range=(from_date, to_date)).aggregate(
#             total_frequent_braking=Round(Sum('frequent_braking')),
#             ave_frequent_braking=Round(Avg('frequent_braking')),
#             max_frequent_braking=Round(Max('frequent_braking')),
#             min_frequent_braking=Round(Min('frequent_braking')),
#             total_distraction=Round(Sum('phone_distraction')),
#             ave_distraction=Round(Avg('phone_distraction')),
#             max_distraction=Round(Max('phone_distraction')),
#             min_distraction=Round(Min('phone_distraction')),
#             total_hard_acceleration=Round(Sum('hard_acceleration')),
#             ave_hard_acceleration=Round(Avg('hard_acceleration')),
#             max_hard_acceleration=Round(Max('hard_acceleration')),
#             min_hard_acceleration=Round(Min('hard_acceleration')),
#             total_hard_braking=Round(Sum('hard_braking')),
#             ave_hard_braking=Round(Avg('hard_braking')),
#             max_hard_braking=Round(Max('hard_braking')),
#             min_hard_braking=Round(Min('hard_braking')),
#             total_lane_change=Round(Sum('lane_change')),
#             ave_lane_change=Round(Avg('lane_change')),
#             max_lane_change=Round(Max('lane_change')),
#             min_lane_change=Round(Min('lane_change')),
#             total_speed_too_high_curve=Round(Sum('speed_too_high_curve')),
#             ave_speed_too_high_curve=Round(Avg('speed_too_high_curve')),
#             max_speed_too_high_curve=Round(Max('speed_too_high_curve')),
#             min_speed_too_high_curve=Round(Min('speed_too_high_curve')),
#             total_turns=Round(Sum('turns')),
#             ave_turns=Round(Avg('turns')),
#             max_turns=Round(Max('turns')),
#             min_turns=Round(Min('turns')),
#         )
#
#         hard_braking = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
#             created_date__date__range=(from_date, to_date)).values('hard_braking')
#         sudden_lane_change = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
#             created_date__date__range=(from_date, to_date)).values('lane_change')
#         distracted_driving = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
#             created_date__date__range=(from_date, to_date)).values('phone_distraction')
#         hard_acceleration = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
#             created_date__date__range=(from_date, to_date)).values('hard_acceleration')
#         right_turns = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
#             created_date__date__range=(from_date, to_date)).values('right_turns')
#         left_turns = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
#             created_date__date__range=(from_date, to_date)).values('left_turns')
#         frequent_braking = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
#             created_date__date__range=(from_date, to_date)).values('frequent_braking')
#         speed_too_high_curve = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
#             created_date__date__range=(from_date, to_date)).values('speed_too_high_curve')
#
#         speed_limit = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
#             created_date__date__range=(from_date, to_date)).values('speed_limit')
#         sudden_turns = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
#             created_date__date__range=(from_date, to_date)).values('turns')
#         rainy_curve = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
#             created_date__date__range=(from_date, to_date)).values('rainy_curve')
#
#         user = Users.objects.filter(id=user_id).values()
#         if stats:
#             attribute_list = ['hard_braking', 'lane_change', 'phone_distraction',
#                               'hard_acceleration', 'right_turns',
#                               'left_turns', 'frequent_braking',
#                               'speed_too_high_curve']
#
#             for attr in attribute_list:
#                 chart_data = get_chart_data(request, attr)
#                 if not chart_data:
#                     return Response(
#                         {
#                             "Code": HTTP_400_BAD_REQUEST,
#                             "status": "error",
#                             "message": "Invalid Date Range"
#                         },
#                         status=HTTP_400_BAD_REQUEST
#                     )
#
#                 chart_data = chart_data.exclude(average=Decimal('0.000000'))
#                 create_image(chart_data, attr)
#
#             ## creating pdf file
#             pdf = FPDF()
#             ## First page
#             pdf.add_page()
#             pdf.set_font('Arial', 'B', 14)
#             pdf.cell(0, txt='Report : ' + str(timezone.now().date()))
#             pdf.y = 20
#             pdf.x = 10
#             name = ''
#             if (user[0]['first_name']):
#                 name = ' (' + user[0]['first_name'] + ' ' + user[0]['last_name'] + ')'
#             pdf.cell(0, txt='Name : ' + user[0]['username'] + name)
#             pdf.y = 30
#             pdf.x = 10
#             pdf.cell(0, txt='Mobile Number : ' + user[0]['mobile'])
#
#             pdf.y = 5
#             pdf.x = 150
#             try:
#                 pdf.image(BASE_DIR + '/' + str(user[0]['avatar']), 150, 5, 35, 30)
#             except Exception as e:
#                 pdf.image(BASE_DIR + '/images/no_profile.jpg', 150, 5, 35, 30)
#
#             ## creating table
#             pdf.y = 40
#             pdf.x = 10
#             pdf.set_fill_color(118, 146, 227)
#             pdf.set_font('Arial', 'B', 16)
#             pdf.set_draw_color(90, 118, 196)
#             pdf.cell(180, 30, txt="Report", align='C', fill=True)
#
#             heading_list = ['From', 'To', 'Distance(Km)', 'Speed(Km/hr)', 'Time in Traffic']
#             values_list = [from_date, to_date, stats['total_distance'], stats['speed'], stats['timein_traffic']]
#             pdf.y = 70
#             pdf.x = 10
#             pdf.set_fill_color(157, 179, 242)
#             pdf.set_font('Arial', 'B', 14)
#             for i in range(5):
#                 pdf.cell(36, 10, txt=heading_list[i], align='C', border=1, fill=True)
#             pdf.y = 80
#             pdf.x = 10
#             pdf.set_font('Arial', '', 14)
#             for i in range(5):
#                 pdf.cell(36, 10, txt=str(values_list[i]), align='C', border=1, fill=True)
#
#             pdf.y = 90
#             pdf.x = 10
#             pdf.set_fill_color(118, 146, 227)
#             pdf.set_font('Arial', 'B', 16)
#             pdf.cell(180, 30, txt="Overall Driving Score (100%)", align='C', fill=True)
#             pdf.set_font('Arial', 'B', 14)
#             heading_list = ["Excellent Driving", "Risky Driving"]
#             values_list = [stats['excelent_driving'], stats['risky_driving']]
#             pdf.y = 120
#             pdf.x = 10
#             pdf.set_fill_color(157, 179, 242)
#             pdf.set_font('Arial', 'B', 14)
#             for i in range(2):
#                 pdf.cell(90, 10, txt=heading_list[i], align='C', border=1, fill=True)
#             pdf.y = 130
#             pdf.x = 10
#             pdf.set_font('Arial', '', 14)
#             for i in range(2):
#                 pdf.cell(90, 10, txt=str(values_list[i]), align='C', border=1, fill=True)
#
#             pdf.y = 140
#             pdf.x = 10
#             pdf.set_fill_color(118, 146, 227)
#             pdf.set_font('Arial', 'B', 16)
#             pdf.cell(180, 30, txt="Driving Style", align='C', fill=True)
#             pdf.set_font('Arial', 'B', 12)
#             heading_list = ["Event", "Average", "Maximum", "Min"]
#             values_list = [['Frequent Braking', style['ave_frequent_braking'], style['max_frequent_braking'],
#                             style['min_frequent_braking']],
#                            ['Distracted driving', style['ave_distraction'], style['max_distraction'],
#                             style['min_distraction']],
#                            ['Hard Acceleration', style['ave_hard_acceleration'], style['max_hard_acceleration'],
#                             style['min_hard_acceleration']],
#                            ['Hard Braking', style['ave_hard_braking'], style['max_hard_braking'],
#                             style['min_hard_braking']],
#                            ['Sudden Lane change', style['ave_lane_change'], style['max_lane_change'],
#                             style['min_lane_change']],
#                            ['Speed High Curve', style['ave_speed_too_high_curve'], style['max_speed_too_high_curve'],
#                             style['min_speed_too_high_curve']],
#                            ['Sudden Turn', style['ave_turns'], style['max_turns'], style['min_turns']]]
#
#             # print("values_list",type(values_list))
#             pdf.y = 170
#             pdf.x = 10
#             pdf.set_fill_color(157, 179, 242)
#             pdf.set_font('Arial', 'B', 12)
#             for i in range(4):
#                 pdf.cell(45, 10, txt=heading_list[i], align='C', border=1, fill=True)
#             pdf.y = 180
#             pdf.x = 10
#             for i in range(7):
#                 for j in range(4):
#                     if (j == 0):
#                         pdf.set_font('Arial', 'B', 10)
#                     else:
#                         pdf.set_font('Arial', '', 12)
#                     pdf.cell(45, 10, txt=str(values_list[i][j]), align='C', border=1, fill=True)
#                 pdf.y = pdf.y + 10
#                 pdf.x = 10
#
#             ## Attaching graph images
#             pdf.add_page()
#             x = 10;
#             y = 5
#             for i in range(len(attribute_list)):
#                 pdf.image('media/' + attribute_list[i] + '.png', x, y, 90, 70)
#                 x += 90
#                 if (i % 2 != 0):
#                     x = 10
#                     y += 70
#
#             pdf.output("media/Insurance_Risk_Reports.pdf", "F")
#             print("pdf has been Created!!!!!!!")
#
#             # # Get chard data
#             # chart_data = get_chart_data(request)
#             # print("Chart Data :::::: ", chart_data)
#
#             userData = {'username': user[0]['username'], 'mobile': user[0]['mobile']}
#             userObj = Users.objects.get(id=user_id)
#             print(userObj.username)
#             today = timezone.now()
#             params = {
#                 'today': today,
#                 'users': userData,
#                 'record': stats,
#                 'from': from_date,
#                 'to': to_date,
#                 "email_type": "No",
#                 "chart_data": chart_data
#             }
#             # return Render.render("driver-risk-report.html", params)
#             return Response(
#                 {
#                     'data': params,
#                     'Code': HTTP_200_OK,
#                     "status": "success"
#                 },
#                 status=HTTP_200_OK
#             )
#         else:
#             return Response(
#                 {
#                     'data': [],
#                     'Code': HTTP_200_OK,
#                     "status": "success"
#                 },
#                 status=HTTP_200_OK
#             )
#     except ObjectDoesNotExist:
#         return "No Record Found"

@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def admin_report(request):
    from_date = request.data.get("from_date", "")
    to_date = request.data.get("to_date", "")
    user_id = request.data.get("user_id", "")
    type_1 = request.data.get('type',0)

    subject = 'Smart Report'
    email = 'seasia.test@gmail.com'

    message = 'test messages'

    month_names = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']
    if type_1=="Smart":
        type_1=1

    if not from_date:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter Start Date and Time"
            },
            status=HTTP_400_BAD_REQUEST
        )
    if not to_date:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter To Date and Time"
            },
            status=HTTP_400_BAD_REQUEST
        )
    if not id:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "User ID Required"
            },
            status=HTTP_400_BAD_REQUEST
        )
    try:
        user = Users.objects.filter(id=user_id).values()
        stats = Trip.objects.filter(user_id=user_id).filter(status=1).filter(usertype='Driver').filter(
            created_date__date__range=(from_date, to_date)).aggregate(
            speed=Round(Avg('speed_avg')),
            total_distance=Round(Sum('distance')),
            timein_traffic=Round(Max('time_in_traffic')),
            excelent_driving=Round(Avg('excellent_driving')),
            fair_driving=Round(Avg('fair_driving')),
            risky_driving=Round(Avg('risky_driving'))
        )
        early_morning = 0
        morning = 0
        afternoon = 0
        evening = 0
        night = 0
        total_count = 0


        #stats['speed'] = round(stats['speed'],1)
        #stats['total_distance'] = round(stats['total_distance'], 1)

        #get the timef in traffic
        trip_ids = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).filter(usertype='Driver').values_list('id',flat=True)
        #return Response({"data":trip_ids})
        ass_user = Users.objects.filter(id= user_id).values_list('absolute_skill_score', flat=True)
        aas_user = Users.objects.filter(id= user_id).values_list('absolute_agression_score', flat=True)
        to_emaill = Users.objects.filter(id= user_id).values_list('e_email', flat=True)
        to_email=to_emaill[0]
        rss_user_data = get_relative_score(user_id)
        print(from_date)
        print("To")
        print(to_date)

        for trip_id in trip_ids:
            #increase the count of the every of the variables
            #variable = total_time_in_traffic_new(trip_id)
            variable= Trip.objects.filter(id=trip_id).values_list('time_in_traffic', flat=True)

            print(variable)
            if variable[0]=='Early Morning':
                morning+=1
            elif variable[0] == 'Morning':
                morning+=1
            elif variable[0] == 'Afternoon':
                morning +=1
            elif variable[0] == 'Evening':
                morning+=1
            else:
                night+=1
            total_count+=1
        #get the percentage of each of the variable
        morning_percentage = (morning*100)/total_count
        night_percentage = (night*100)/total_count
        overall_score = stats['excelent_driving']

        stats['timein_traffic'] ={"Day":round(morning_percentage,0), "Night":round(night_percentage,0)}
        #return Response({"data":exellent_driving})
        total_excellent_driving = Trip.objects.filter(user_id=user_id, excellent_driving__gte=80).filter(
            status=1).filter(
            created_date__date__range=(from_date, to_date)).count()
        total_fair_driving = Trip.objects.filter(user_id=user_id, excellent_driving__gte=50,
                                                 excellent_driving__lt=80).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).count()
        total_risky_driving = Trip.objects.filter(user_id=user_id, excellent_driving__lt=50).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).count()
        total_count = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).count()

        #return Response({"data":[total_excellent_driving, total_count]})
        if total_count > 0:
            excellent_driving_score = (total_excellent_driving / total_count)*100
            fair_driving_score = (total_fair_driving / total_count)*100
            risky_driving_score = (total_risky_driving / total_count)*100

        #return Response({"data":[round(excellent_driving_score,2), round(fair_driving_score,2), round(risky_driving_score, 2)]})
        stats['excelent_driving'] = round(excellent_driving_score,1)
        stats['fair_driving'] = round(fair_driving_score, 1)
        stats['risky_driving'] = round(risky_driving_score, 1)

        hard_braking = get_average_values_six_months('hard_braking', user_id, to_date, from_date)
        lane_change = get_average_values_six_months('lane_change', user_id, to_date, from_date)
        phone_distraction = get_average_values_six_months('phone_distraction', user_id, to_date, from_date)
        hard_acceleration = get_average_values_six_months('hard_acceleration', user_id, to_date, from_date)
        right_turns = get_average_values_six_months('right_turns', user_id, to_date, from_date)
        left_turns = get_average_values_six_months('left_turns', user_id, to_date, from_date)
        turns = get_average_values_six_months('turns', user_id, to_date, from_date)
        frequent_braking = get_average_values_six_months('frequent_braking', user_id, to_date, from_date)
        # speed_too_high_curve = get_average_values_six_months('speed_too_high_curve', user_id, to_date, from_date)
        excellent_driving = get_average_values_six_months('excellent_driving', user_id, to_date, from_date)

        visualization_details = {'hard_braking': hard_braking,
                                 'sudden_lane_change': lane_change,
                                 'distracted_driving': phone_distraction,
                                 'hard_acceleration': hard_acceleration,
                                 'sudden_turns': turns,
                                 'frequent_braking': frequent_braking,
                                 'right_turn': right_turns,
                                 'left_turn': left_turns,
                                 'excellent_driving': excellent_driving
                                 }
        # return Response({"data":visualization_details})
        key_imp = {'frequent_braking': ["", 100], 'distracted_driving': ["", 100], 'hard_acceleration': ["", 100],
                   'sudden_turns': ["", 100], 'hard_braking': ["", 100], 'sudden_lane_change': ["", 100]}
        for k, v in visualization_details.items():
            date_list = []
            average_list = []
            for data in v:
                date_list.append(data['X'])
                average_list.append(data['Y'])
                if data['Y'] < 50:
                    x_y = data['X'].split('/')
                    if x_y[0] == '1':
                        key_imp[k][0] = month_names[0]
                    elif x_y[0] == '2':
                        key_imp[k][0] = month_names[1]
                    elif x_y[0] == '3':
                        key_imp[k][0] = month_names[2]
                    elif x_y[0] == '4':
                        key_imp[k][0] = month_names[3]
                    elif x_y[0] == '5':
                        key_imp[k][0] = month_names[4]
                    elif x_y[0] == '6':
                        key_imp[k][0] = month_names[5]
                    elif x_y[0] == '7':
                        key_imp[k][0] = month_names[6]
                    elif x_y[0] == '8':
                        key_imp[k][0] = month_names[7]
                    elif x_y[0] == '9':
                        key_imp[k][0] = month_names[8]
                    elif x_y[0] == '10':
                        key_imp[k][0] = month_names[9]
                    elif x_y[0] == '11':
                        key_imp[k][0] = month_names[10]
                    elif x_y[0] == '12':
                        key_imp[k][0] = month_names[11]

                    key_imp[k][1] = data['Y']
        if stats:
            today = timezone.now()

            style = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).filter(usertype='Driver').aggregate(
            total_frequent_braking=Round(Sum('frequent_braking')),
            ave_frequent_braking=Round(Avg('frequent_braking')),
            max_frequent_braking=Round(Max('frequent_braking')),
            min_frequent_braking=Round(Min('frequent_braking')),
            total_distraction=Round(Sum('phone_distraction')),
            ave_distraction=Round(Avg('phone_distraction')),
            max_distraction=Round(Max('phone_distraction')),
            min_distraction=Round(Min('phone_distraction')),
            total_hard_acceleration=Round(Sum('hard_acceleration')),
            ave_hard_acceleration=Round(Avg('hard_acceleration')),
            max_hard_acceleration=Round(Max('hard_acceleration')),
            min_hard_acceleration=Round(Min('hard_acceleration')),
            total_hard_braking=Round(Sum('hard_braking')),
            ave_hard_braking=Round(Avg('hard_braking')),
            max_hard_braking=Round(Max('hard_braking')),
            min_hard_braking=Round(Min('hard_braking')),
            total_lane_change=Round(Sum('lane_change')),
            ave_lane_change=Round(Avg('lane_change')),
            max_lane_change=Round(Max('lane_change')),
            min_lane_change=Round(Min('lane_change')),
            total_speed_too_high_curve=Round(Sum('speed_too_high_curve')),
            ave_speed_too_high_curve=Round(Avg('speed_too_high_curve')),
            max_speed_too_high_curve=Round(Max('speed_too_high_curve')),
            min_speed_too_high_curve=Round(Min('speed_too_high_curve')),
            total_turns=Round(Sum('turns')),
            ave_turns=Round(Avg('turns')),
            max_turns=Round(Max('turns')),
            min_turns=Round(Min('turns')),
            )

            ## Average and Min Max Table details
            heading_list= ["Event", "Average","Maximum", "Minimum"]
            values_list = [['Frequent Braking',round(style['ave_frequent_braking'],1),   round(style['max_frequent_braking'],1), round(style['min_frequent_braking'],1)],
                        ['Distracted driving',round(style['ave_distraction'],1), round(style['max_distraction'],1), round(style['min_distraction'],1)],
                        ['Hard Acceleration',round(style['ave_hard_acceleration'],1), round(style['max_hard_acceleration'],1),round(style['min_hard_acceleration'],1)],
                        ['Hard Braking',round(style['ave_hard_braking'],1), round(style['max_hard_braking'],1), round(style['min_hard_braking'],1)],
                        ['Sudden Lane change',round(style['ave_lane_change'],1), round(style['max_lane_change'],1), round(style['min_lane_change'],1)],

                        ['Sudden Turn',round(style['ave_turns'],1), round(style['max_turns'],1), round(style['min_turns'],1)]]

            ### Graphs
            attribute_list =['hard_braking','lane_change','phone_distraction',
                            'hard_acceleration','turns',
                            'frequent_braking',
                            'excellent_driving','right_turns','left_turns']
            for attr in attribute_list:
                chart_data = Trip.objects.filter(user_id=user_id).filter(status=1).filter(usertype='Driver').filter(created_date__date__range=(from_date, to_date)).annotate(
                                           day=TruncDay('created_date')).values('day').annotate(
                                            average=Avg(attr)).values('day', 'average')
                if not chart_data:
                    return Response(
                        {
                            "Code": HTTP_400_BAD_REQUEST,
                            "status": "error",
                            "message": "Invalid Date Range"
                        },
                        status=HTTP_400_BAD_REQUEST
                        )

                data_table_event = user_driving_style(from_date, to_date, user_id)
                chart_data=chart_data.exclude(average=Decimal('0.000000'))
                create_image(chart_data,attr)

            graphs_image_list =[]

            attribute_list.append('time_event')
            for i in range(len(attribute_list)):
                graphs_image_list.append(BASE_DIR+'/media/'+attribute_list[i]+'.png')
            #print(user['username'])
            #print(type(user['username']))

            improvement_areas = {'Frequent Braking': {"month": key_imp['frequent_braking'][0],
                                                      "value": key_imp['frequent_braking'][1],
                                                      "msg": "Please concentrate on road and avoid distraction during driving"},
                                 'Distraction': {"value": key_imp['distracted_driving'][1],
                                                 "month": key_imp['distracted_driving'][0],
                                                 "msg": "Avoid using mobile phone while driving"},
                                 'Hard Acceleration': {"value": key_imp['hard_acceleration'][1],
                                                       "month": key_imp['hard_acceleration'][0],
                                                       "msg": "Please increase the accelerator calmly and gradually."},
                                 'Hard Braking': {"value": key_imp['hard_braking'][1],
                                                  "month": key_imp['hard_braking'][0],
                                                  "msg": "Please apply the brakes calmly and gradually to fully immobilize the vehicle."},
                                 'Lane Change': {"value": key_imp['sudden_lane_change'][1],
                                                 "month": key_imp['sudden_lane_change'][0],
                                                 "msg": "Kindly drive in own lane and do not change lane frequently."},
                                 'Turns': {"value": key_imp['sudden_turns'][1], "month": key_imp['sudden_turns'][0],
                                           "msg": "Reduce driving speed and turn on indicator before taking the  turn."}
                                 }

            new_speed = format(round((stats['speed']*3.6), 1), '.1f')
            print(stats['speed'])
            print(stats['speed'])
            stats['speed'] = new_speed
            #stats[0]['speed'] = round(speed['stats'], 1)
            params = {
                'today': today,
                'type':type_1,
                'username':user[0]['username'].capitalize(),
                'overall_score':overall_score,
                'users': user,
                'record': stats,
                 'speed':new_speed,
                'from': datetime.strptime(from_date, "%Y-%m-%d").strftime("%d.%m.%Y"),
                'to': datetime.strptime(to_date, "%Y-%m-%d").strftime("%d.%m.%Y"),
                "email_type": "No",
                'heading_list' : heading_list,
                'values_list' : values_list,
                'ass_user':round(ass_user[0], 1),
                'aas_user':round(aas_user[0], 1),
                'rss_user':rss_user_data[0],
                'ras_user':rss_user_data[1],
                'table_data_header' : data_table_event[0],
                "improvement_areas": improvement_areas,
                'event_data_table' : data_table_event[1],
                'excellent_driving_graph':graphs_image_list[6:7],
                'graphs_image_list1' : graphs_image_list[:2],
                'graphs_image_list2' : graphs_image_list[2:4],
                'graphs_image_list3' : graphs_image_list[4:6],
                'graphs_image_list4' : graphs_image_list[7:9],

            }
            #return Response({"data":params})
            if type_1=='email':
                text_content = 'Check your report'
                # htmly = get_template('driver-risk-report.html')
                #
                # html_content = htmly.render(params)
                # return Response({"data":html_content})
                print("THISI IS START")
                params['excellent_driving_graph'] = [s.replace('/var/www/html/driveiq-backend/', 'http://203.100.79.170:8000/') for s in graphs_image_list[6:7]]
                print(params['excellent_driving_graph'])
                params['graphs_image_list1'] = [s.replace('/var/www/html/driveiq-backend/', 'http://203.100.79.170:8000/') for s in graphs_image_list[:2]]
                params['graphs_image_list2'] = [s.replace('/var/www/html/driveiq-backend/', 'http://203.100.79.170:8000/') for s in graphs_image_list[2:4]]
                params['graphs_image_list3'] = [s.replace('/var/www/html/driveiq-backend/', 'http://203.100.79.170:8000/') for s in graphs_image_list[4:6]]
                params['graphs_image_list4'] = [s.replace('/var/www/html/driveiq-backend/', 'http://203.100.79.170:8000/') for s in graphs_image_list[7:9]]


                post_pdf = render_to_pdf('driver-risk-report.html', params)

                msg = EmailMultiAlternatives(subject, text_content, email, [to_email])
                msg.attach('report.pdf', post_pdf, "application/pdf")
                msg.send()
            #return Response({"data":"snee"})
            return Render.render("driver-risk-report.html", params)
        else:
            return Response(
                {
                    'data': [],
                    'Code': HTTP_200_OK,
                    "status": "success"
                },
                status=HTTP_200_OK
            )
    except ObjectDoesNotExist:
        return "No Record Found"


def create_image(chart_data, attr):
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    date_list = []
    average_list = []
    for data in chart_data:
        if 'month' in str(chart_data[0]):
            date_list.append(str(data['month']).replace('00:00:00', '').replace('-01', ''))
            average_list.append(round(data['average'], 2))
        elif 'week' in str(chart_data[0]):
            date_list.append(str(data['week']).replace('00:00:00', ''))
            average_list.append(round(data['average'], 2))
        elif 'day' in str(chart_data[0]):
            date_list.append(str(data['day']).replace('00:00:00', ''))
            average_list.append(round(data['average'], 2))
        else:
            date_1 = str(data['year'])
            date_1 = date_1.split('/')[0]
            date_list.append(date_1)
            average_list.append(float(round(data['average'], 2)))

    plt.style.use('ggplot')
    x = date_list
    y = average_list
    x_pos = [i for i, _ in enumerate(x)]
    plt.xticks(rotation=90)

    for i in range(len(date_list)):  # your number of bars
        plt.text(x=x_pos[i] - .3,  # takes your x values as horizontal positioning argument
                 y=average_list[i],  # takes your y values as vertical positioning argument
                 s=average_list[i],  # the labels you want to add to the data
                 size=10,
                 rotation=90)

    plt.bar(x_pos, y, color='#6b93f2')
    plt.xlabel(attr.replace('_', ' ').upper(), color='black', size=16)
    plt.ylabel("Average Score")
    if attr=='excellent_driving':
        plt.ylabel("Driving Score")
        plt.xlabel("Day")
    if attr=='lane_change':
        plt.xlabel('Sudden Lange Change')
    if attr=='phone_distraction':
        plt.xlabel('Distracted Driving')
    plt.title('')
    plt.xticks(x_pos, x, size=8)
    plt.savefig('media/' + attr + '.png', dpi=(200), bbox_inches="tight")
    plt.close()

def create_image_time_event(x_data, y_data, attr):
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    date_list = []
    average_list = []

    plt.style.use('ggplot')
    x = x_data
    y = y_data
    x_pos = [i for i, _ in enumerate(x)]
    plt.xticks(rotation=90)

    for i in range(len(date_list)):  # your number of bars
        plt.text(x=x_pos[i] - .3,  # takes your x values as horizontal positioning argument
                 y=average_list[i],  # takes your y values as vertical positioning argument
                 s=average_list[i],  # the labels you want to add to the data
                 size=10,
                 rotation=90)

    plt.bar(x_pos, y, color='#6b93f2')
    plt.xlabel(attr.replace('_', ' ').upper(), color='black', size=16)
    plt.ylabel("Average")
    plt.title('')
    plt.xticks(x_pos, x, size=8)
    plt.savefig('media/' + attr + '.png', dpi=(200), bbox_inches="tight")
    plt.close()

def create_image_time_event_scatter(ha,hb,st, sl, fb, y_data, attr):
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    date_list = []
    average_list = []

    plt.style.use('ggplot')
    #x = x_data
    y = y_data
    #x_pos = [i for i, _ in enumerate(x)]
    plt.xticks(rotation=90)

    # for i in range(len(date_list)):  # your number of bars
    #     plt.text(x=x_pos[i] - .3,  # takes your x values as horizontal positioning argument
    #              y=average_list[i],  # takes your y values as vertical positioning argument
    #              s=average_list[i],  # the labels you want to add to the data
    #              size=10,
    #              rotation=90)

    plt.scatter(ha, y, color='red',label='hard acceleration')
    plt.scatter(hb,y, color='blue', label='hard braking')
    plt.scatter(st, y, color='cyan',label='sudden turn')
    plt.scatter(sl, y, color='green', label='sudden lane change')
    plt.scatter(fb, y, color='magenta', label='Freqent braking')



    plt.xlabel(attr.replace('_', ' ').upper(), color='black', size=16)
    plt.ylabel("Average")
    plt.title('')
    #plt.xticks(x_pos, x, size=8)
    plt.savefig('media/' + attr + '.png', dpi=(200), bbox_inches="tight")
    plt.close()

def get_chart_data(request, attribute):
    """
    Get chart data based on years, months or days
    """
    # try:
    from_date = request.data.get("from_date", "")
    to_date = request.data.get("to_date", "")
    user_id = request.data.get("user_id", "")  # 35

    print(from_date)
    print(to_date)
    date_diff = get_date_Difference(from_date, to_date)
    print(date_diff)
    #stats = Trip.objecsts.filter(user_id = user_id).filter(status = 1).filter(created_date__date__range=(from_date, to_date)).values()
    if not date_diff:
        return 0

    if (date_diff == 'years'):
        stats = Trip.objects.filter(user_id=user_id).filter(status=1).filter(created_date__date__range=(from_date, to_date)).annotate(
            year=TruncYear('created_date')).values('year').annotate(
            average=Avg(attribute)).values('year', 'average')

    elif (date_diff == 'months'):
        stats = Trip.objects.filter(user_id=user_id).filter(status=1).filter(created_date__date__range=(from_date, to_date)).annotate(
            month=TruncMonth('created_date')).values('month').annotate(
            average=Avg(attribute)).values('month', 'average')

    elif (date_diff == 'weeks'):
        stats = Trip.objects.filter(user_id=user_id).filter(status=1).filter(created_date__date__range=(from_date, to_date)).annotate(
            week=TruncWeek('created_date')).values('week').annotate(
            average=Avg(attribute)).values('week', 'average')

    else:
        stats = Trip.objects.filter(user_id=user_id).filter(status=1).filter(usertype='Driver').filter(created_date__date__range=(from_date, to_date)).annotate(
            day=TruncDay('created_date')).values('day').annotate(
            average=Avg(attribute)).values('day', 'average')

    return stats

    # except ObjectDoesNotExist:
    #     return None


def get_date_Difference(from_date, to_date):
    """
    Calculate two date difference in years, months and days
    """
    # date_1 = datetime(from_date)
    date_1 = datetime.strptime(str(from_date), '%Y-%m-%d')

    # date_2 = datetime(to_date)
    date_2 = datetime.strptime(str(to_date), '%Y-%m-%d')

    # This will find the difference between the two dates
    # difference = relativedelta.relativedelta(date_1, date_2)
    difference = date_2 - date_1
    if ( '-' in str(difference)):
        return 0
    days = str(difference).split()[0]
    days = int(days)
    print(days)

    years = days // 365
    months = days // 180
    # years = difference.years
    # months = difference.months
    # days = difference.days
    weeks = 0
    if 180 > days >= 30:
        weeks = days // 7

    # print ("Difference is {} year, {} months,{} weeks, {} days.".format(years, months, weeks, days))
    print("years")
    print(years)
    print(months)
    print(weeks)

    if years > 0:
        return 'years'
    elif months > 0:
        return 'months'
    elif weeks > 0:
        return 'weeks'
    else:
        return 'days'

@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def driver_detection(request):
    sensor_data = request.data.get("sensor_data", "")
    if not sensor_data:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Send the Sensor Data of 30 sec"
            },
            status=HTTP_400_BAD_REQUEST
        )
    driver = 0
    passenger = 0
    not_defined = 0
    length = len(sensor_data)
    # print("Length", length)
    print(sensor_data)
    user_id = 0
    for record in sensor_data:
        user_id = record['user_id']
        # predict = driver_passenger.predict([[record['X'], record['Y'], record['Gyro Y']]])
        predict = driver_passenger.predict([[record['acc_x'], record['acc_y'], record['acc_z'], record['gyro_x'], record['gyro_y'], record['gyro_z'], record['magneto_x'], record['magneto_y'], record['magneto_z']]])
        #predict = driver_passenger.predict([[record['acc_x'], record['acc_y'], record['acc_z']]])
        # if predict == 'Driver':
        #     driver += 1
        # elif predict == 'Passenger':
        #     passenger += 1
        # else:
        #     not_defined += 1
        if predict[0] < 0.5:
            driver += 1
        else:
            passenger += 1
    # print("Driver", driver)
    # print("Passenger", passenger)
    # print("Not Defined", not_defined)
    if driver > passenger:
        data = "Driver"
    else:
        data = "Passenger"


    end_date = datetime.now() - timedelta(minutes=15)
    start_date = datetime.now() - timedelta(hours=48, minutes=30)

    Sensordata.objects.filter(trip_id=0).filter(user_id=user_id).delete()

    # print('sensorRecords', red, Sensordata.objects.filter(trip_id=0).filter(user_id=2).filter(created_at__range=(end_date, start_date)).query)

    for record in sensor_data:
        # print('predict',record['device_id'])

        Sensordata.objects.create(
            device_id=record['device_id'],
            acc_x=record['acc_x'],
            acc_y=record['acc_y'],
            acc_z=record['acc_z'],
            acc_magnitude=record['acc_magnitude'],
            accuracy=record['accuracy'],
            altitude=record['altitude'],
            gyro_x=record['gyro_x'],
            gyro_y=record['gyro_y'],
            gyro_z=record['gyro_z'],
            latitude=record['latitude'],
            longitude=record['longitude'],
            magneto_x=record['magneto_x'],
            magneto_y=record['magneto_y'],
            magneto_z=record['magneto_z'],
            speed=record['speed'],
            timeInMillis=record['timeInMillis'],
            type=record['type'],
            user_id=record['user_id']
        )

    return Response(
        {
            "Code": HTTP_200_OK,
            "status": "success",
            "message": "Detection Done",
            "data": data
        },
        status=HTTP_200_OK
    )


@csrf_exempt
@api_view(["POST"])
def update_trip_user(request):
    """
    API endpoint update trip user type
    """
    trip_id = request.data.get("trip_id", "")
    user_type = request.data.get("user_type", "")

    if not trip_id:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter Trip Id"
            },
            status=HTTP_400_BAD_REQUEST
        )
    if not user_type:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Please Enter User Type"
            },
            status=HTTP_400_BAD_REQUEST
        )

    try:
        tripObj = Trip.objects.get(id=trip_id)
        # check for the has_user_changed
        has_user_changed = Trip.objects.filter(id=trip_id).values_list('usertype', flat=True)
        print(has_user_changed[0])
        if user_type != has_user_changed[0]:
            tripObj.has_user_changed = 1
            tripObj.is_confirmed = 1
        else:
            print(trip_id)
            tripObj.is_confirmed = 1

        tripObj.usertype = user_type
        tripObj.save()


        return Response(
            {
                'Code': HTTP_200_OK,
                "status": "success"
            },
            status=HTTP_200_OK
        )
    except Trip.DoesNotExist:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Trip Does Not Exist. Please try again!"
            },
            status=HTTP_400_BAD_REQUEST
        )


@csrf_exempt
@api_view(["POST"])
def create_trip_user(request):
    # trip_start_time = datetime.datetime.strptime(request.data.get("trip_start_time", ""), '%Y-%m-%d %H:%M:%S')
    trip_time = request.data.get("trip_start_time", "")
    print('trip_time', trip_time)
    print("changes here")
    # trip_start_time = trip_time / 1000.0
    # trip_start_end =  (trip_time - 300000 ) / 1000.0
    user_id = request.data.get("user_id", "")
    usertype = request.data.get("usertype", "")
    app_version_name = request.data.get("app_version_name", "")
    device_id = request.data.get("device_id", "")
    mobile_model = request.data.get("mobile_model", "")
    mobile_version = request.data.get("mobile_version", "")
    is_gyro_available = request.data.get("isGyroAvailable", 1)

    tripList = Trip.objects.filter(user_id=user_id).filter(finished=0).order_by('id').count()

    if tripList > 0:
        tripList = Trip.objects.filter(user_id=user_id).filter(finished=0).order_by('-id').values('id',
                                                                                                  'trip_start_time')
        TripID = tripList[0]['id']
        # sensorCount = Sensordata.objects.filter(user_id=user_id).filter(trip_id=TripID).order_by('id').values('timeInMillis').count()
        sensorData = Sensordata.objects.filter(user_id=user_id).filter(trip_id=TripID).filter(type='Moving').order_by('id').values(
            'timeInMillis')

        # print('ddddddddddddd sensorCount',sensorData)

        if len(sensorData) == 0:

            print('No Sensor Data')

            trip_stated_time = time.mktime(
                datetime.strptime(tripList[0]['trip_start_time'].strftime('%Y-%m-%d %H:%M:%S.%f'),
                                  '%Y-%m-%d %H:%M:%S.%f').timetuple())
            print('trip_stated_time')
            print(trip_stated_time)
            # convert milliseconds date and trip date to a specific format
            fmt = '%Y-%m-%d %H:%M:%S'
            d1 = datetime.fromtimestamp(int(trip_time) / 1000).strftime('%Y-%m-%d %H:%M:%S')
            d2 = datetime.fromtimestamp(trip_stated_time).strftime('%Y-%m-%d %H:%M:%S')
            print('d1')
            print(d1)
            print('d2')
            print(d2)

            # convert date to time and calculate minutes
            date_format = "%Y-%m-%d %H:%M:%S"
            time1 = datetime.strptime(d1, date_format)
            time2 = datetime.strptime(d2, date_format)
            print('time1')
            print(time1)
            print('time2')
            print(time2)

            diff = time1 - time2
            diff_minutes = int(diff.seconds / 60)
            print('diff_minutes')
            print(diff_minutes)

            if time2 >= time1:
                TripID = tripList[0]['id']
            elif diff_minutes <= 5:
                TripID = tripList[0]['id']
            else:
                print('end time function over here')
                print(datetime.fromtimestamp(float(trip_stated_time) / 1000.0).strftime('%Y-%m-%d %H:%M:%S.%f'))
                Trip.objects.filter(user_id=user_id).filter(id=tripList[0]['id']).filter(finished=0).update(
                    finished=1,
                    trip_end_time=d2,
                )


                TripCreated = Trip.objects.create(
                    trip_start_time=d1,
                    user_id=user_id,
                    usertype=usertype,
                    app_version_name=app_version_name,
                    device_id=device_id,
                    mobile_model=mobile_model,
                    mobile_version=mobile_version,
                    is_gyro_available= is_gyro_available
                )

                TripID = TripCreated.id


        else:
            sensorList = Sensordata.objects.filter(user_id=user_id).filter(trip_id=TripID).filter(type='Moving').order_by('-id').values(
                'timeInMillis')
            sensorTime = int(sensorList[0]['timeInMillis']) / 1000  # 12:10

            print('Sensor Data found')

            # convert milliseconds date and trip date to a specific format
            fmt = '%Y-%m-%d %H:%M:%S'
            d1 = datetime.fromtimestamp(int(trip_time) / 1000).strftime('%Y-%m-%d %H:%M:%S')
            d2 = datetime.fromtimestamp(sensorTime).strftime('%Y-%m-%d %H:%M:%S')
            print('d1 is time sent by mobile')
            print(d1)
            print('d2 is last sensor record ')
            print(d2)

            # convert date to time and calculate minutes
            date_format = "%Y-%m-%d %H:%M:%S"
            time1 = datetime.strptime(d1, date_format)
            time2 = datetime.strptime(d2, date_format)
            print('time1 is time sent by mobile')
            print(time1)
            print('time2 is last sensor record')
            print(time2)

            diff = time1 - time2
            diff_minutes = int(diff.seconds / 60)
            print('diff_minutes')
            print(diff_minutes)

            # print('ddddddddddddd',trip_time , int(sensorTime), int(trip_time) - int(sensorTime))

            # if sensorTime  > time.mktime(datetime.strptime( tripList[0]['created_date'].strftime('%Y-%m-%d %H:%M:%S.%f'), '%Y-%m-%d %H:%M:%S.%f').timetuple()) :

            if time2 >= time1:
                TripID = tripList[0]['id']
            elif diff_minutes <= 5:
                TripID = tripList[0]['id']
            else:
                TripID = tripList[0]['id']
                Trip.objects.filter(user_id=user_id).filter(id=tripList[0]['id']).filter(finished=0).update(
                    finished=1,
                    trip_end_time=d2
                )

                Sensordata.objects.filter(trip_id=0).filter(user_id=user_id).update(trip_id=TripID)

                TripCreated = Trip.objects.create(
                    trip_start_time=d1,
                    user_id=user_id,
                    usertype=usertype,
                    app_version_name=app_version_name,
                    device_id=device_id,
                    mobile_model=mobile_model,
                    mobile_version=mobile_version,
                    is_gyro_available= is_gyro_available
                )

                TripID = TripCreated.id


    else:
        # sensorList = Sensordata.objects.filter(user_id=user_id).filter(~Q(trip_id=0)).filter(timeInMillis__range=( (trip_time - 300000)  , trip_time )).order_by('id').count()
        # print('sensorList',sensorList)
        # if sensorList == 0:
        # print('sssssssssssssssss',trip_time)
        TripCreated = Trip.objects.create(
            trip_start_time=datetime.fromtimestamp(float(trip_time) / 1000.0).strftime('%Y-%m-%d %H:%M:%S.%f'),
            user_id=user_id,
            usertype=usertype,
            app_version_name=app_version_name,
            device_id=device_id,
            mobile_model=mobile_model,
            mobile_version=mobile_version,
            is_gyro_available = is_gyro_available
        )

        TripID = TripCreated.id
        # else:
        #    tripList = Sensordata.objects.filter(user_id=user_id).filter(~Q(trip_id=0)).filter(timeInMillis__range=( (trip_time - 300000)  , trip_time )).order_by('-timeInMillis').distinct().values('trip_id')
        #    print(tripList)
        # for trips in tripList:
        #      print(trips['trip_id'])
        #    TripID = tripList[0]['trip_id']

    return Response(
        {
            'data': TripID,
            'Code': HTTP_200_OK,
            "status": "success"
        },
        status=HTTP_200_OK
    )


@csrf_exempt
@api_view(["POST"])
def close_trip_user(request):
    trip_id = request.data.get("trip_id", "")
    trip_time = request.data.get("trip_close_time", "")
    user_id = request.data.get("user_id", "")
    time_in_trafic = request.data.get("time_in_trafic", "")
    code = HTTP_200_OK
    message = 'Trip Terminated'

    sensorList = Sensordata.objects.filter(user_id=user_id).filter(type='Moving').filter(trip_id=trip_id).order_by('-id').values(
        'timeInMillis')
    print(sensorList)
    if len(sensorList) == 0:
        print('No Sensor Data')
        code = HTTP_400_BAD_REQUEST
        message = 'Trip Not Terminated'

    else:

        sensorTime = int(sensorList[0]['timeInMillis']) / 1000  # 12:10

        d1 = datetime.fromtimestamp(int(trip_time) / 1000).strftime('%Y-%m-%d %H:%M:%S')
        d2 = datetime.fromtimestamp(sensorTime).strftime('%Y-%m-%d %H:%M:%S')
        print('d1 is time sent by mobile')
        print(d1)
        print('d2 is last sensor record ')
        print(d2)

        # convert date to time and calculate minutes
        date_format = "%Y-%m-%d %H:%M:%S"
        time1 = datetime.strptime(d1, date_format)
        time2 = datetime.strptime(d2, date_format)
        print('time1 is time sent by mobile')
        print(time1)
        print('time2 is last sensor record')
        print(time2)

        diff = time1 - time2
        print('diff')
        print(diff)
        diff_minutes = int(diff.seconds / 60)
        print('diff_minutes')
        print(diff_minutes)
        print(diff_minutes)

        if diff_minutes <= 5:
            # TripID = tripList[0]['id']
            code = HTTP_400_BAD_REQUEST
            message = 'Trip Not Terminated'
        elif diff_minutes == 0:
            # TripID = tripList[0]['id']
            code = HTTP_400_BAD_REQUEST
            message = 'Trip Not Terminated'
        else:
            #get the trip start time
            trip_start_time = Trip.objects.filter(id=trip_id).values_list('trip_start_time', flat=True)

            trip_end_date = trip_start_time[0] + timedelta(minutes=5)
            trip_start_date = trip_start_time[0] - timedelta(minutes=10)
            print(trip_end_date)
            print(trip_start_date)

            Sensordata.objects.filter(trip_id=0).filter(user_id=user_id).update(trip_id=trip_id)

            Trip.objects.filter(user_id=user_id).filter(id=trip_id).filter(finished=0).update(
                finished=1,
                trip_end_time=d2,
                time_in_trafic=time_in_trafic
            )

    return Response(
        {
            'Code': code,
            "status": "success",
            "message": message
        },
        status=HTTP_200_OK
    )


@csrf_exempt
@api_view(["POST"])
def close_trip(request):
    trip_id = request.data.get("trip_id", "")
    trip_time = request.data.get("trip_close_time", "")
    user_id = request.data.get("user_id", "")
    # time_in_trafic = request.data.get("time_in_trafic", "")
    code = HTTP_200_OK
    message = 'Trip Terminated'

    #sensorList = Sensordata.objects.filter(user_id=user_id).filter(type='Moving').filter(trip_id=trip_id).order_by('-id').values(
    #    'timeInMillis')
    Sensordata.objects.filter(trip_id=0).filter(user_id=user_id).update(trip_id=trip_id)
    sensorList = Sensordata.objects.filter(user_id=user_id).filter(type='Moving').filter(trip_id=trip_id).order_by('-id').values('timeInMillis')
    print(sensorList)
    if len(sensorList) == 0:

        print('No Sensor Data')
        code = HTTP_400_BAD_REQUEST
        message = 'Trip Not Terminated'

    else:

        sensorTime = int(sensorList[0]['timeInMillis']) / 1000  # 12:10

        d1 = datetime.fromtimestamp(int(trip_time) / 1000).strftime('%Y-%m-%d %H:%M:%S')
        d2 = datetime.fromtimestamp(sensorTime).strftime('%Y-%m-%d %H:%M:%S')
        print('d1 is time sent by mobile')
        print(d1)
        print('d2 is last sensor record ')
        print(d2)

        # convert date to time and calculate minutes
        date_format = "%Y-%m-%d %H:%M:%S"
        time1 = datetime.strptime(d1, date_format)
        time2 = datetime.strptime(d2, date_format)
        print('time1 is time sent by mobile')
        print(time1)
        print('time2 is last sensor record')
        print(time2)

        diff = time1 - time2
        print('diff')
        print(diff)
        diff_minutes = int(diff.seconds / 60)
        print('diff_minutes')
        print(diff_minutes)
        print(diff_minutes)

        #get the trip start time
        trip_start_time = Trip.objects.filter(id=trip_id).values_list('trip_start_time', flat=True)

        trip_end_date = trip_start_time[0] + timedelta(minutes=5)
        trip_start_date = trip_start_time[0] - timedelta(minutes=10)
        print(trip_end_date)
        print(trip_start_date)

        Sensordata.objects.filter(trip_id=0).filter(user_id=user_id).update(trip_id=trip_id)

        Trip.objects.filter(user_id=user_id).filter(id=trip_id).filter(finished=0).update(
            finished=1,
            trip_end_time=d2
            #time_in_trafic=time_in_trafic
        )

    return Response(
        {
            'Code': code,
            "status": "success",
            "message": message
        },
        status=HTTP_200_OK
    )


@csrf_exempt
@api_view(["POST"])
def close_trip_user_test(request):
    trip_id = request.data.get("trip_id", "")
    trip_time = request.data.get("trip_close_time", "")
    user_id = request.data.get("user_id", "")

    Trip.objects.filter(user_id=user_id).filter(id=trip_id).filter(finished=0).update(
        finished=1,
        trip_end_time=datetime.fromtimestamp(trip_time / 1000.0).strftime('%Y-%m-%d %H:%M:%S.%f'),
    )

    return Response(
        {
            'Code': HTTP_200_OK,
            "status": "success",
            "message": "trip terminate"
        },
        status=HTTP_200_OK
    )


# @csrf_exempt
# @api_view(["POST"])
def analysis_unmoving(trip_id):
    # get the sensor data
    total_sensor_data = Sensordata.objects.filter(type='Moving', trip_id=trip_id)
    driver = 0
    passenger = 0
    not_defined = 0
    for sensor_data in total_sensor_data:

        predict = driver_passenger.predict([[sensor_data.acc_x, sensor_data.acc_y, sensor_data.acc_z, sensor_data.gyro_x, sensor_data.gyro_y, sensor_data.gyro_z, sensor_data.magneto_x, sensor_data.magneto_y, sensor_data.magneto_y]])
        #predict = driver_passenger.predict([[sensor_data.acc_x, sensor_data.acc_y, sensor_data.acc_z]])

        if predict[0] < 0.5:
            driver += 1
        else:
            passenger += 1

    if driver > passenger:
        data = "Driver"
    else:
        data = "Passenger"

    return data
    # trip = Trip.objects.get(id = request.data.get('trip_id'))
    # trip.usertype = data
    # trip.save()


# @csrf_exempt
# @api_view(["POST"])
def distracted_driving(trip_id):
    total_sensor_data = Sensordata.objects.filter(trip_id=trip_id, type='Moving').count()
    sensor_data = Sensordata.objects.filter(trip_id=trip_id, type='Moving', is_user_device=1).values_list('timeInMillis',
                                                                                         flat=True).distinct().count()
    #make the percentage
    if total_sensor_data > 0:
        percentage = (sensor_data*100)/total_sensor_data
    else:
        percentage = 0
    percentage = 100-percentage
    return percentage
    # return Response({"Total Seconds": "Your total seconds is {} seconds".format(percentage)})

@csrf_exempt
@api_view(["POST"])
def set_user_confirmed(request, pk):
     #set the user_confirmed to 1 and check the has_user_chagned of the user
     has_user_changed = Trip.objects.filter(id = pk).values_list('usertype', flat=True)
     print(request.data.get('has_user_changed'))
     #check for the has_user_changed
     if request.data.get('has_user_changed')!=has_user_changed:
         update_trip = Trip.objects.filter(id= pk).update(
            has_user_changed = 1,
            is_confirmed= 1
         )
         return Response(
                {
                    'data': "Trip updated successfully",
                    'Code': HTTP_200_OK,
                    "status": "success"
                },
                status=HTTP_200_OK
            )
     else:
         update_trip = Trip.objects.filter(id=pk).update(
             is_confirmed=1
         )
         return Response(
             {
                 'data': "Trip updated successfully",
                 'Code': HTTP_200_OK,
                 "status": "success"
             },
             status=HTTP_200_OK
         )




def get_average_values_daywise(field, user_id):
    value_list=[]
    current_date = datetime.today()
    previous_date = datetime.today() - timedelta(days=1000)
    trips = Trip.objects.filter(user_id=user_id, created_date__date__range= [previous_date,current_date]).annotate(
            day=TruncDay('created_date')).values('day').annotate(
            average=Avg(field)).values('day', 'average')


    trip_count = 0
    for trip in trips:
        if round(trip['average'], 2) > 0 :
            value_dict={ 'X': str(trip['day'].day) + '/' + str(trip['day'].month), 'Y' : round(trip['average'], 2) }
            value_list.append(value_dict)
    if len(value_list) > 12 :
        return value_list[-12:]
    else :
        return value_list

def get_average_values_monthwise(field, user_id):
    value_list = []

    current_date = datetime.today()

    previous_date = current_date.replace(year=current_date.year - 1)

    trips = Trip.objects.filter(user_id=user_id, created_date__date__range=[previous_date, current_date]).annotate(

        month=TruncMonth('created_date')).values('month').annotate(

        average=Avg(field)).values('month', 'average')

    for trip in trips:
        value_dict = {'X': str(trip['month'].month) + '/' + str(trip['month'].year), 'Y': trip['average']}

        value_list.append(value_dict)

    return value_list


def get_average_values_weekwise(field, user_id):
    value_list = []

    current_date = datetime.today()

    previous_date = datetime.today() - timedelta(days=12 * 7)

    date1 = current_date

    week_slot = []

    for i in range(12):

        week_dict = {}

        dates = []

        for dy in range(7):
            date1 = date1 - timedelta(days=1)

            dates.append((str(date1.day) + '/' + str(date1.month)))

        week_dict['dates'] = dates

        week_slot.append(week_dict)

    trips = Trip.objects.filter(user_id=user_id, created_date__date__range=[previous_date, current_date]).annotate(

        week=TruncWeek('created_date')).values('week').annotate(

        average=Avg(field)).values('week', 'average')



    for trip in trips:

        value_dict = {}

        for weeks in week_slot:

            if ((str(trip['week'].day) + '/' + str(trip['week'].month)) in weeks['dates']):
                value_dict = {'X': str(weeks['dates'][6]) + '-' + str(weeks['dates'][0]),
                              'Y': round(trip['average'], 2)}

                break
        if bool(value_dict):
            value_list.append(value_dict)

    return value_list


def get_average_values_yearwise(field, user_id):
    value_list = []

    current_date = datetime.today()

    previous_date = current_date.replace(year=current_date.year - 12)

    trips = Trip.objects.filter(user_id=user_id, created_date__date__range=[previous_date, current_date]).annotate(

        year=TruncYear('created_date')).values('year').annotate(

        average=Avg(field)).values('year', 'average')

    for trip in trips:
        value_dict = {'X': trip['year'].year, 'Y': trip['average']}

        value_list.append(value_dict)

    return value_list


@api_view(["POST"])
@permission_classes((AllowAny,))
def trip_visualization(request):
    visualization_details = {}

    try:

        data = request.data

        filter_type = data['filter_type']

        user_id = data['user_id']

        current_date = datetime.today()

        previous_date = datetime.today()

        hard_braking = []

        lane_change = []

        phone_distraction = []

        hard_acceleration = []

        right_turns = []

        left_turns = []

        frequent_braking = []

        speed_too_high_curve = []

        phone_distraction = []

        sudden_turns = []

        over_speeding = []

        if filter_type == 'days' or filter_type == '1':

            hard_braking = get_average_values_daywise('hard_braking', user_id)

            lane_change = get_average_values_daywise('lane_change', user_id)

            phone_distraction = get_average_values_daywise('phone_distraction', user_id)

            hard_acceleration = get_average_values_daywise('hard_acceleration', user_id)

            right_turns = get_average_values_daywise('right_turns', user_id)

            left_turns = get_average_values_daywise('left_turns', user_id)

            frequent_braking = get_average_values_daywise('frequent_braking', user_id)

            speed_too_high_curve = get_average_values_daywise('speed_too_high_curve', user_id)

            sudden_turns = get_average_values_daywise('turns', user_id)

            over_speeding = get_average_values_daywise('over_speeding', user_id)


            accident_detection = get_average_dummy_values('1')

        elif filter_type == 'weeks' or filter_type == '2':

            hard_braking = get_average_values_weekwise('hard_braking', user_id)

            lane_change = get_average_values_weekwise('lane_change', user_id)

            phone_distraction = get_average_values_weekwise('phone_distraction', user_id)

            hard_acceleration = get_average_values_weekwise('hard_acceleration', user_id)

            right_turns = get_average_values_weekwise('right_turns', user_id)

            left_turns = get_average_values_weekwise('left_turns', user_id)

            frequent_braking = get_average_values_weekwise('frequent_braking', user_id)

            speed_too_high_curve = get_average_values_weekwise('speed_too_high_curve', user_id)

            sudden_turns = get_average_values_daywise('turns', user_id)

            over_speeding = get_average_values_weekwise('over_speeding', user_id)
            accident_detection = get_average_dummy_values('2')



        elif filter_type == 'months' or filter_type == '3':

            hard_braking = get_average_values_monthwise('hard_braking', user_id)

            lane_change = get_average_values_monthwise('lane_change', user_id)

            phone_distraction = get_average_values_monthwise('phone_distraction', user_id)

            hard_acceleration = get_average_values_monthwise('hard_acceleration', user_id)

            right_turns = get_average_values_monthwise('right_turns', user_id)

            left_turns = get_average_values_monthwise('left_turns', user_id)

            frequent_braking = get_average_values_monthwise('frequent_braking', user_id)

            speed_too_high_curve = get_average_values_monthwise('speed_too_high_curve', user_id)

            sudden_turns = get_average_values_daywise('turns', user_id)

            over_speeding = get_average_values_monthwise('over_speeding', user_id)

            accident_detection = get_average_dummy_values('3')



        else:

            hard_braking = get_average_values_yearwise('hard_braking', user_id)

            lane_change = get_average_values_yearwise('lane_change', user_id)

            phone_distraction = get_average_values_yearwise('phone_distraction', user_id)

            hard_acceleration = get_average_values_yearwise('hard_acceleration', user_id)

            right_turns = get_average_values_yearwise('right_turns', user_id)

            left_turns = get_average_values_yearwise('left_turns', user_id)

            frequent_braking = get_average_values_yearwise('frequent_braking', user_id)

            speed_too_high_curve = get_average_values_yearwise('speed_too_high_curve', user_id)

            sudden_turns = get_average_values_daywise('turns', user_id)

            over_speeding = get_average_values_yearwise('over_speeding', user_id)
            accident_detection = get_average_dummy_values('3')

        visualization_details = {'hard_braking': hard_braking,

                                 'lane_change': lane_change,

                                 'phone_distraction': phone_distraction,

                                 'hard_acceleration': hard_acceleration,

                                 'right_turns': right_turns,

                                 'left_turns': left_turns,

                                 'frequent_braking': frequent_braking,

                                 'speed_too_high_curve': speed_too_high_curve,
                                 "accident_detection": accident_detection,
                                 'sudden_turns': sudden_turns,
                                 'over_speeding': over_speeding

                                 }

    except Exception as e:

        print("Error>>>>>>>>>>>>>>>>>", e)

    print("API Called.......")

    return Response(

        {

            'Code': HTTP_200_OK,

            "status": "success",

            "message": "trip terminate",

            "data": visualization_details,

        }

    )


def get_average_dummy_values(filter):
    current_date = datetime.today()

    value_list = []

    if filter == '1':

        value_dict = {}

        for i in range(12):
            value_list.append({'X': str((datetime.today() - timedelta(days=i)).day) + '/' + str(
                (datetime.today() - timedelta(days=i)).month), 'Y': 100})



    elif filter == '2':

        dy = 0

        for i in range(12):
            value_list.append({'X': str((datetime.today() - timedelta(days=dy + 6)).day) + '/' + str(
                (datetime.today() - timedelta(days=dy + 7)).month) + '-' + str(
                (datetime.today() - timedelta(days=dy)).day) + '/' + str((datetime.today() - timedelta(days=dy)).month),
                               'Y': 100})

            dy = dy + 7

    elif filter == '3':

        dy = 0

        for i in range(12):
            value_list.append({'X': str((datetime.today() - timedelta(days=dy)).month) + '/' + str(
                (datetime.today() - timedelta(days=dy)).year), 'Y': 100})

            dy = dy + 30

    else:

        dy = 0

        for i in range(12):
            value_list.append({'X': str((datetime.today() - timedelta(days=dy)).year), 'Y': 100})

            dy = dy + 365

    return value_list



# @csrf_exempt
# @api_view(["POST"])
def count_distracted_behaviour(sensorRecords):
   #get all the values of the trip_id

   #trips = Sensordata.objects.filter(trip_id=trip_id).order_by('-id').values('timeInMillis', 'gyro_x')

   total_distraction = 0
   timeseconds = 0
   i = 0
   score = 100
   for trip_data in sensorRecords:
       #take the loop data
       current_seconds = int(trip_data['timeInMillis'])/1000

       if (float(trip_data['gyro_x']) >= 2.5 or float(trip_data['gyro_x']) <= -2.5) and i==0:
           total_distraction+=1
           timeseconds = int(trip_data['timeInMillis'])/1000
           i = i+1

           #check out the difference between two successive trips
       #print(abs(current_seconds-timeseconds))
       if abs(current_seconds-timeseconds) > 30:
           if float(trip_data['gyro_x']) >= 2.5 or float(trip_data['gyro_x']) <= -2.5:
               total_distraction+=1
               timeseconds = int(trip_data['timeInMillis']) / 1000


   #update the score of the of the user0
   if 1<=total_distraction<2:
       score = 70
   elif 2<=total_distraction<3:
       score = 50
   elif 3<=total_distraction <4:
       score = 30
   elif 4 <= total_distraction:
       score = 0
   return [score, total_distraction]


def count_distracted_behaviour_without_gyro(sensorRecords):
   #get all the values of the trip_id

   #trips = Sensordata.objects.filter(trip_id=trip_id).order_by('-id').values('timeInMillis', 'gyro_x')
   print("in the phone count distracted driving")
   total_distraction = 0
   timeseconds_1 = 0
   ii = 0
   score = 100
   for trip_data in sensorRecords:
       #take the loop data
       current_seconds_1 = int(trip_data['timeInMillis'])/1000

       if int(trip_data['is_user_device'])==1 and ii==0:
           total_distraction+=1
           timeseconds_1 = int(trip_data['timeInMillis'])/1000
           ii = ii+1


       #print(abs(current_seconds-timeseconds))
       if abs(current_seconds_1-timeseconds_1) > 30:
           if int(tripata['is_user_device'])==1:
               total_distraction+=1
               timeseconds_1 = int(trip_data['timeInMillis']) / 1000
               print("int there second")

   print("Total distraction is here")
   print(total_distraction)
   #update the score of the of the user0
   if 1<=total_distraction<2:
       score = 70
   elif 2<=total_distraction<3:
       score = 50
   elif 3<=total_distraction <4:
       score = 30
   elif 4 <= total_distraction:
       score = 0

   return [score, total_distraction]
"""
time in traffic 
"""
@api_view(["GET"])
@permission_classes((AllowAny,))
def total_time_in_traffic(request, trip_id):
    #make it the group_by function
    # total_time = Sensordata.objects.filter(trip_id = trip_id, type = 'Moving').filter(speed__lt = 4).annotate(Count('timeInMillis', distinct=True))
    # print("TOTAL TIME")
    # print(total_time[0].timeInMillis__count)
    # return str(timedelta(seconds=total_time[0].timeInMillis__count))
    #get the trip start time and check out the Day and night
    start_time = Trip.objects.filter(id=trip_id).values_list('trip_start_time', flat=True)

    early_morning_in = "05:00:00"
    early_morning_out = "08:00:00"

    morning_in = "08:00:00"
    morning_out = "12:00:00"

    afternoon_in = "12:00:00"
    afternoon_out = "17:00:00"

    evening_in = "17:00:00"
    evening_out = "21:00:00"

    trip_datetime = datetime.strptime(datetime.strftime(start_time[0], "%Y-%m-%d"), "%Y-%m-%d")
    earlym_check = datetime.strptime(early_morning_in, "%H:%M:%S")
    earlym_check2 = datetime.strptime(early_morning_out, "%H:%M:%S")

    morning_check = datetime.strptime(morning_in, "%H:%M:%S")
    morning_check2 = datetime.strptime(morning_out, "%H:%M:%S")

    afternoon_check = datetime.strptime(afternoon_in, "%H:%M:%S")
    afternoon_check2 = datetime.strptime(afternoon_out, "%H:%M:%S")

    evening_check = datetime.strptime(evening_in, "%H:%M:%S")
    evening_check2 = datetime.strptime(evening_out, "%H:%M:%S")

    # I am supposing that the date must be the same as now
    start_time2 = start_time[0].replace(hour=start_time[0].time().hour, minute=start_time[0].time().minute,
                                        second=start_time[0].time().second, microsecond=0)

    trip_datetime_em = trip_datetime.replace(hour=earlym_check.time().hour, minute=earlym_check.time().minute,
                                             second=earlym_check.time().second, microsecond=0)
    trip_datetime_em2 = trip_datetime.replace(hour=earlym_check2.time().hour, minute=earlym_check2.time().minute,
                                              second=earlym_check2.time().second, microsecond=0)

    trip_datetime_m = trip_datetime.replace(hour=morning_check.time().hour, minute=morning_check.time().minute,
                                            second=morning_check.time().second, microsecond=0)
    trip_datetime_m2 = trip_datetime.replace(hour=morning_check2.time().hour, minute=morning_check2.time().minute,
                                             second=morning_check2.time().second, microsecond=0)

    trip_datetime_a = trip_datetime.replace(hour=afternoon_check.time().hour, minute=afternoon_check.time().minute,
                                            second=afternoon_check.time().second, microsecond=0)
    trip_datetime_a2 = trip_datetime.replace(hour=afternoon_check2.time().hour, minute=afternoon_check2.time().minute,
                                             second=afternoon_check2.time().second, microsecond=0)

    trip_datetime_e = trip_datetime.replace(hour=evening_check.time().hour, minute=evening_check.time().minute,
                                            second=evening_check.time().second, microsecond=0)
    trip_datetime_e2 = trip_datetime.replace(hour=evening_check2.time().hour, minute=evening_check2.time().minute,
                                             second=evening_check2.time().second, microsecond=0)

    if (start_time2 >= trip_datetime_em and start_time2 < trip_datetime_em2):
        return Response({"data":'Early Morning'})
    elif (start_time2 >= trip_datetime_m and start_time2 < trip_datetime_m2):
        return Response({"data", "Morning"})
    elif (start_time2 >= trip_datetime_a and start_time2 < trip_datetime_a2):
        return Response ({"data":"Afternoon"})

    elif (start_time2 >= trip_datetime_e and start_time2 < trip_datetime_e2):
        return Response ({"data":"Evening"})
    else:
        return Response({"data":'Night'})

"""
time in traffic 
"""
# @api_view(["GET"])
# @permission_classes((AllowAny,))
def total_time_in_traffic_new(trip_id):
    #make it the group_by function
    # total_time = Sensordata.objects.filter(trip_id = trip_id, type = 'Moving').filter(speed__lt = 4).annotate(Count('timeInMillis', distinct=True))
    # print("TOTAL TIME")
    # print(total_time[0].timeInMillis__count)
    # return str(timedelta(seconds=total_time[0].timeInMillis__count))
    #get the trip start time and check out the Day and night

    start_time = Trip.objects.filter(id = trip_id).values_list('trip_start_time', flat=True)

    early_morning_in = "05:00:00"
    early_morning_out = "08:00:00"

    morning_in = "08:00:00"
    morning_out = "12:00:00"

    afternoon_in =  "12:00:00"
    afternoon_out = "17:00:00"

    evening_in =  "17:00:00"
    evening_out = "21:00:00"


    trip_datetime = datetime.strptime(datetime.strftime(start_time[0],"%Y-%m-%d"), "%Y-%m-%d")
    earlym_check = datetime.strptime(early_morning_in, "%H:%M:%S")
    earlym_check2 = datetime.strptime(early_morning_out, "%H:%M:%S")

    morning_check = datetime.strptime(morning_in, "%H:%M:%S")
    morning_check2 = datetime.strptime(morning_out, "%H:%M:%S")

    afternoon_check = datetime.strptime(afternoon_in, "%H:%M:%S")
    afternoon_check2 = datetime.strptime(afternoon_out, "%H:%M:%S")

    evening_check = datetime.strptime(evening_in, "%H:%M:%S")
    evening_check2 = datetime.strptime(evening_out, "%H:%M:%S")


    # I am supposing that the date must be the same as now
    start_time2 = start_time[0].replace(hour=start_time[0].time().hour, minute=start_time[0].time().minute,
                              second=start_time[0].time().second, microsecond=0)

    trip_datetime_em = trip_datetime.replace(hour=earlym_check.time().hour, minute=earlym_check.time().minute,
                              second=earlym_check.time().second, microsecond=0)
    trip_datetime_em2 = trip_datetime.replace(hour=earlym_check2.time().hour, minute=earlym_check2.time().minute,
                              second=earlym_check2.time().second, microsecond=0)

    trip_datetime_m = trip_datetime.replace(hour=morning_check.time().hour, minute=morning_check.time().minute,
                              second=morning_check.time().second, microsecond=0)
    trip_datetime_m2 = trip_datetime.replace(hour=morning_check2.time().hour, minute=morning_check2.time().minute,
                              second=morning_check2.time().second, microsecond=0)

    trip_datetime_a = trip_datetime.replace(hour=afternoon_check.time().hour, minute=afternoon_check.time().minute,
                              second=afternoon_check.time().second, microsecond=0)
    trip_datetime_a2 = trip_datetime.replace(hour=afternoon_check2.time().hour, minute=afternoon_check2.time().minute,
                              second=afternoon_check2.time().second, microsecond=0)

    trip_datetime_e = trip_datetime.replace(hour=evening_check.time().hour, minute=evening_check.time().minute,
                              second=evening_check.time().second, microsecond=0)
    trip_datetime_e2 = trip_datetime.replace(hour=evening_check2.time().hour, minute=evening_check2.time().minute,
                              second=evening_check2.time().second, microsecond=0)


    if (start_time2 >= trip_datetime_em and start_time2 < trip_datetime_em2):
        return 'Early Morning'
    elif (start_time2 >= trip_datetime_m and start_time2 < trip_datetime_m2):
        return "Morning"
    elif (start_time2 >= trip_datetime_a and start_time2 < trip_datetime_a2):
        return "Afternoon"
    elif (start_time2 >= trip_datetime_e and start_time2 < trip_datetime_e2):
        return "Evening"
    else:
        return 'Night'


@csrf_exempt
@api_view(["POST"])
def check_distracted_data(request, trip_id):
    print(trip_id)
    sensor_records = Sensordata.objects.filter(trip_id=trip_id).values()
    count_data1 = Sensordata.objects.filter(trip_id=trip_id).values().count()
    print(count_data1)
    count_data = count_distracted_behaviour(sensor_records)
    return Response({"total distraction": count_data})

def last_day_of_month(any_day):
    next_month = any_day.replace(day=28) + timedelta(days=4)  # this will never fail
    return next_month - timedelta(days=next_month.day)

@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def consolidate_report(request):
    user_id = request.data.get("user_id", "")
    from_date_get = request.data.get('from_date',None)
    to_date_get = request.data.get('to_date', None)

    type_1 = request.data.get('type','Consolidated')
    month = request.data.get('month', 4)
    subject = 'Smart Report'
    email = 'seasia.test@gmail.com'

    message = 'test messages'

    last_date = last_day_of_month(date(2020, int(month), 1))
    #data_date = datetime.strptime(last_date, '%Y-%m-%d %H:%M:00')
    date_new = last_date - monthdelta.MonthDelta(6)


    if not user_id:
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "User ID Required"
            },
            status=HTTP_400_BAD_REQUEST
        )
    try:
        user = Users.objects.filter(id=user_id).values()
        to_emaill = Users.objects.filter(id=user_id).values_list('e_email', flat=True)
        to_email = to_emaill[0]
        print(user)

        user_capt_name = user[0]['username'].capitalize()
        to_date = timezone.now()
        from_date = datetime.today() - timedelta(days=(30 * 6))
        print("First one")
        print(type(to_date))
        print(from_date)


        to_date = datetime.combine(last_date, datetime.min.time())
        from_date = datetime.combine(date_new, datetime.min.time())+timedelta(days=1)

        print("second one")
        print(to_date)
        print(type(from_date))
        print(from_date)


        #return Response({"date": to_date})
        if from_date_get and type_1 !="Smart" and type_1!="Monthly":
            from_date = from_date_get+timedelta(days=1)
            to_date = to_date_get

        if type_1=="Smart":
            from_date=from_date_get
            to_date= to_date_get

        if type_1=="Monthly":
            to_date = timezone.now()
            from_date = datetime.today() - timedelta(days=(30 * 1))

        stats = Trip.objects.filter(user_id=user_id).filter(status=1).filter(usertype='Driver').filter(
            created_date__date__range=(from_date, to_date)).aggregate(
            speed=Round(Avg('speed_avg')),
            total_distance=Round(Sum('distance')),
            timein_traffic=Round(Max('time_in_traffic')),
            excelent_driving=Round(Avg('excellent_driving')),
            fair_driving=Round(Avg('fair_driving')),
            risky_driving=Round(Avg('risky_driving'))
        )
        driving_score = Trip.objects.filter(user_id=user_id,
                                            created_date__date__range=[from_date, to_date]).filter(usertype='Driver').annotate(
            month=TruncMonth('created_date')).values('month').annotate(
            average=Avg('excellent_driving')).values('month', 'average')

        overall_driving_average = 0
        count=0
        for i in driving_score:
            count+=1
            overall_driving_average+=i['average']



        stats['overall_driving'] = round(overall_driving_average/count, 1)

        if stats:
            style = Trip.objects.filter(user_id=user_id).filter(status=1).filter(usertype='Driver').filter(
                created_date__date__range=(from_date, to_date)).aggregate(
                total_frequent_braking=Round(Sum('frequent_braking')),
                ave_frequent_braking=Round(Avg('frequent_braking')),
                max_frequent_braking=Round(Max('frequent_braking')),
                min_frequent_braking=Round(Min('frequent_braking')),
                total_distraction=Round(Sum('phone_distraction')),
                ave_distraction=Round(Avg('phone_distraction')),
                max_distraction=Round(Max('phone_distraction')),
                min_distraction=Round(Min('phone_distraction')),
                total_hard_acceleration=Round(Sum('hard_acceleration')),
                ave_hard_acceleration=Round(Avg('hard_acceleration')),
                max_hard_acceleration=Round(Max('hard_acceleration')),
                min_hard_acceleration=Round(Min('hard_acceleration')),
                total_hard_braking=Round(Sum('hard_braking')),
                ave_hard_braking=Round(Avg('hard_braking')),
                max_hard_braking=Round(Max('hard_braking')),
                min_hard_braking=Round(Min('hard_braking')),
                total_lane_change=Round(Sum('lane_change')),
                ave_lane_change=Round(Avg('lane_change')),
                max_lane_change=Round(Max('lane_change')),
                min_lane_change=Round(Min('lane_change')),
                total_speed_too_high_curve=Round(Sum('speed_too_high_curve')),
                ave_speed_too_high_curve=Round(Avg('speed_too_high_curve')),
                max_speed_too_high_curve=Round(Max('speed_too_high_curve')),
                min_speed_too_high_curve=Round(Min('speed_too_high_curve')),
                total_turns=Round(Sum('turns')),
                ave_turns=Round(Avg('turns')),
                max_turns=Round(Max('turns')),
                min_turns=Round(Min('turns')),
                total_excellent_driving=Round(Sum('excellent_driving')),
                ave_excellent_driving=Round(Avg('excellent_driving')),
                max_excellent_driving=Round(Max('excellent_driving')),
                min_excellent_driving=Round(Min('excellent_driving')),
            )

            total_excellent_driving = Trip.objects.filter(user_id=user_id, excellent_driving__gte=80).filter(
                status=1,usertype='Driver').filter(
                created_date__date__range=(from_date, to_date)).count()
            total_fair_driving = Trip.objects.filter(user_id=user_id, excellent_driving__gte=50,
                                                     excellent_driving__lt=80).filter(status=1,usertype='Driver').filter(
                created_date__date__range=(from_date, to_date)).count()
            total_risky_driving = Trip.objects.filter(user_id=user_id, excellent_driving__lt=50).filter(
                status=1,usertype='Driver').filter(
                created_date__date__range=(from_date, to_date)).count()
            total_count = Trip.objects.filter(user_id=user_id).filter(status=1,usertype='Driver').filter(
                created_date__date__range=(from_date, to_date)).count()

            stats['excelent_driving'] = round((total_excellent_driving/total_count)*100,1)
            stats['fair_driving'] = round((total_fair_driving/total_count)*100,1)
            stats['risky_driving'] = round((total_risky_driving/total_count)*100,1)



            heading_list = ["Event", "Average", "Maximum", "Min"]
            values_list = [['Frequent Braking', round(style['ave_frequent_braking'],1), round(style['max_frequent_braking'],1),
                            round(style['min_frequent_braking'],1)],
                           ['Distracted driving', round(style['ave_distraction'],1),round(style['max_distraction'],1),
                            round(style['min_distraction'],1)],
                           ['Hard Acceleration', round(style['ave_hard_acceleration'],1), round(style['max_hard_acceleration'],1),
                            round(style['min_hard_acceleration'],1)],
                           ['Hard Braking', round(style['ave_hard_braking'],1), round(style['max_hard_braking'],1),
                            round(style['min_hard_braking'],1)],
                           ['Sudden Lane change', round(style['ave_lane_change'],1), round(style['max_lane_change'],1),
                            round(style['min_lane_change'],1)],
                           ['Sudden Turn', round(style['ave_turns'],1), round(style['max_turns'],1), round(style['min_turns'],1)]]


            #return Response({"data": type})
            #check out the scores of all areas and


            details = Trip.objects.filter(user_id=user_id).filter(usertype='Driver').filter(status=1).filter(
                created_date__date__range=(from_date, to_date)).values_list('duration')


            d1=datetime.strptime("00:00:00", "%H:%M:%S")
            dt1 = timedelta(minutes=d1.minute, seconds=d1.second, microseconds=d1.microsecond)
            for time_loop in details:
                if not '.' in time_loop[0]:
                    d2 = datetime.strptime(time_loop[0]+'.00', "%H:%M:%S.%f")
                else:
                    d2 = datetime.strptime(time_loop[0], "%H:%M:%S.%f")
                dt2  = timedelta(minutes=d2.minute, seconds=d2.second, microseconds=d2.microsecond)
                dt1 = dt1+dt2

            total_duration = str(dt1).split(':')
            total_hours = total_duration[0] + " Hours and " + total_duration[1] + " Minutes"

            month_names_list = []
            sum_list = []
            current_date = today = date.today()
            before_sixmonths_date = date.today() - relativedelta(months=+5)
            distance_stats = Trip.objects.filter(user_id=user_id, created_date__date__range=[from_date,
                                                                                             to_date]).filter(usertype='Driver').annotate(
                month=TruncMonth('created_date')).values('month').annotate(
                sum=Sum('distance')).values('month', 'sum')

            month_names = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']
            for stat in distance_stats:
                month_names_list.append(month_names[stat['month'].month - 1])
                sum_list.append(round(stat['sum'], 1))

            ##graphs
            hard_braking = []
            lane_change = []
            phone_distraction = []
            hard_acceleration = []
            right_turns = []
            left_turns = []
            frequent_braking = []
            speed_too_high_curve = []
            phone_distraction = []
            accident_detection = []
            excellent_driving = []
            turns = []
            hard_braking = get_average_values_six_months('hard_braking', user_id, to_date, from_date)
            lane_change = get_average_values_six_months('lane_change', user_id, to_date, from_date)
            phone_distraction = get_average_values_six_months('phone_distraction', user_id, to_date, from_date)
            hard_acceleration = get_average_values_six_months('hard_acceleration', user_id, to_date, from_date)
            right_turns = get_average_values_six_months('right_turns', user_id, to_date, from_date)
            left_turns = get_average_values_six_months('left_turns', user_id, to_date, from_date)
            turns = get_average_values_six_months('turns', user_id, to_date, from_date)
            frequent_braking = get_average_values_six_months('frequent_braking', user_id, to_date, from_date)
            #speed_too_high_curve = get_average_values_six_months('speed_too_high_curve', user_id, to_date, from_date)
            excellent_driving = get_average_values_six_months('excellent_driving', user_id, to_date, from_date)




            visualization_details = {'hard_braking': hard_braking,
                                     'sudden_lane_change': lane_change,
                                      'distracted_driving': phone_distraction,
                                     'hard_acceleration': hard_acceleration,
                                     'sudden_turns': turns,
                                     'frequent_braking': frequent_braking,
                                     'right_turn':right_turns,
                                     'left_turn':left_turns,
                                     'excellent_driving': excellent_driving
                                     }
            #return Response({"data":visualization_details})
            key_imp = {'frequent_braking':["",100],'distracted_driving':["",100],'hard_acceleration':["",100],'sudden_turns':["",100],'hard_braking':["",100],'sudden_lane_change':["",100]}
            for k, v in visualization_details.items():
                date_list = []
                average_list = []
                for data in v:
                    date_list.append(data['X'])
                    average_list.append(data['Y'])
                    if data['Y']<50:
                        x_y =  data['X'].split('/')
                        if x_y[0]=='1':
                            key_imp[k][0]=month_names[0]
                        elif x_y[0]=='2':
                            key_imp[k][0] = month_names[1]
                        elif x_y[0] == '3':
                            key_imp[k][0] = month_names[2]
                        elif x_y[0] == '4':
                            key_imp[k][0] = month_names[3]
                        elif x_y[0] == '5':
                            key_imp[k][0] = month_names[4]
                        elif x_y[0] == '6':
                            key_imp[k][0] = month_names[5]
                        elif x_y[0] == '7':
                            key_imp[k][0] = month_names[6]
                        elif x_y[0] == '8':
                            key_imp[k][0] = month_names[7]
                        elif x_y[0] == '9':
                            key_imp[k][0] = month_names[8]
                        elif x_y[0] == '10':
                            key_imp[k][0] = month_names[9]
                        elif x_y[0] == '11':
                            key_imp[k][0] = month_names[10]
                        elif x_y[0] == '12':
                            key_imp[k][0] = month_names[11]


                        key_imp[k][1]=data['Y']
                import matplotlib
                matplotlib.use('Agg')
                import matplotlib.pyplot as plt
                plt.style.use('ggplot')

                x = date_list
                y = average_list
                x_pos = [i for i, _ in enumerate(x)]
                plt.xticks(rotation=90)

                for i in range(len(date_list)):  # your number of bars
                    plt.text(x=x_pos[i] - .3,  # takes your x values as horizontal positioning argument
                             y=average_list[i],  # takes your y values as vertical positioning argument
                             s=average_list[i],  # the labels you want to add to the data
                             size=10,
                             rotation=90)

                ## deleting image
                path = 'media/' + str(k) + '_consolidated.png'
                if os.path.isfile(path):
                    os.remove(path)

                plt.bar(x_pos, y, color='#6b93f2')
                plt.xlabel(k.replace('_', ' ').upper(), color='black', size=16)
                plt.ylabel("Average Score")
                if k=='excellent_driving':
                    plt.xlabel('Month')
                    plt.ylabel('Driving Score')
                plt.title('')
                plt.xticks(x_pos, x, size=8)
                plt.savefig('media/' + str(k) + '_consolidated.png', dpi=(200), bbox_inches="tight")
                plt.close()

            graphs_image_list = []
            graph_names = list(visualization_details.keys())
            for i in range(len(graph_names)):
                graphs_image_list.append(BASE_DIR + '/media/' + graph_names[i] + '_consolidated.png')
            data_table_event = user_driving_style(from_date, to_date, user_id)
            today = timezone.now()

            improvement_areas = {'Frequent Braking': {"month":key_imp['frequent_braking'][0],
                                                    "value": key_imp['frequent_braking'][1],
                                                      "msg": "Please concentrate on road and avoid distraction during driving"},
                                 'Distraction': {"value": key_imp['distracted_driving'][1],"month":key_imp['distracted_driving'][0],
                                                 "msg": "Avoid using mobile phone while driving"},
                                 'Hard Acceleration': {"value": key_imp['hard_acceleration'][1],"month":key_imp['hard_acceleration'][0],
                                                       "msg": "Please increase the accelerator calmly and gradually."},
                                 'Hard Braking': {"value": key_imp['hard_braking'][1],"month":key_imp['hard_braking'][0],
                                                  "msg": "Please apply the brakes calmly and gradually to fully immobilize the vehicle."},
                                 'Lane Change': {"value": key_imp['sudden_lane_change'][1],"month":key_imp['sudden_lane_change'][0],
                                                 "msg": "Kindly drive in own lane and do not change lane frequently."},
                                 'Turns': {"value": key_imp['sudden_turns'][1],"month":key_imp['sudden_turns'][0],
                                           "msg": "Reduce driving speed and turn on indicator before taking the  turn."}
                                 }



            #time in traffic

            # get the time in traffic
            trip_ids = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
                created_date__date__range=(from_date, to_date)).filter(usertype='Driver').values_list('id', flat=True)
            morning = 0
            night =0
            total_count = 0
            for trip_id in trip_ids:
                # increase the count of the every of the variables
                variable = total_time_in_traffic_new(trip_id)

                if variable == 'Early Morning':
                    morning += 1
                elif variable == 'Morning':
                    morning += 1
                elif variable == 'Afternoon':
                    morning += 1
                elif variable == 'Evening':
                    morning += 1
                else:
                    night += 1
                total_count += 1
            # get the percentage of each of the variable
            morning_percentage = (morning * 100) / total_count
            night_percentage = (night * 100) / total_count

            stats['timein_traffic'] = {"Day": round(morning_percentage,0), "Night": round(night_percentage,0)}




            #change date format
            if from_date_get:
                from_date_modified = datetime.strptime(from_date_get, "%Y-%m-%d").strftime("%d:%m:%Y")
                to_date_modified = datetime.strptime(to_date_get, "%Y-%m-%d").strftime("%d:%m:%Y")
                #return Response({"first":from_date_modified})
            else:
                from_date_modified = datetime.strftime(from_date, "%d:%m:%Y")
                to_date_modified = datetime.strftime(to_date, "%d:%m:%Y")
                #return Response({"second":from_date_modified})

            stats['total_distance'] = round(stats['total_distance'],1)

            #total_distance['distance'] = round(total_distance['distance'],1)

            new_speed = format(round(stats['speed']*3.6, 1), '.1f')
            stats['speed'] = new_speed
            params = {
                'today': today,
                'username':user_capt_name,
                'users': user,
                'record': stats,
                'from': from_date_modified,
                'to': to_date_modified,
                'type':type_1,
                "email_type": "No",
                "heading_list": heading_list,
                "values_list": values_list,
                'table_data_header': data_table_event[0],
                'event_data_table': data_table_event[1],
                "improvement_areas": improvement_areas,
                'total_distance': stats['total_distance'],
                'total_hours': total_hours,
                'month_names_list': month_names_list,
                'sum_list': sum_list,
                'excellent_driving_graph':graphs_image_list[8:9],
                'graphs_image_list1': graphs_image_list[:2],
                'graphs_image_list2': graphs_image_list[2:4],
                'graphs_image_list3': graphs_image_list[4:6],
                'graphs_image_list4': graphs_image_list[6:8],



            }
            print("done!!!!!!!!!!!!!!!!!!!!!!!!!")
            #return Response({"data":params})
            if type_1 == 'email':
                text_content = 'Check your report'
                # htmly = get_template('driver-risk-report.html')
                #
                # html_content = htmly.render(params)
                # return Response({"data":html_content})
                print("THISI IS START")
                # params['excellent_driving_graph'] = [
                #     s.replace('/var/www/html/driveiq-backend/', 'http://203.100.79.170:8000/') for s in
                #     graphs_image_list[6:7]]
                # print(params['excellent_driving_graph'])
                # params['graphs_image_list1'] = [
                #     s.replace('/var/www/html/driveiq-backend/', 'http://203.100.79.170:8000/') for s in
                #     graphs_image_list[:2]]
                # params['graphs_image_list2'] = [
                #     s.replace('/var/www/html/driveiq-backend/', 'http://203.100.79.170:8000/') for s in
                #     graphs_image_list[2:4]]
                # params['graphs_image_list3'] = [
                #     s.replace('/var/www/html/driveiq-backend/', 'http://203.100.79.170:8000/') for s in
                #     graphs_image_list[4:6]]
                # params['graphs_image_list4'] = [
                #     s.replace('/var/www/html/driveiq-backend/', 'http://203.100.79.170:8000/') for s in
                #     graphs_image_list[7:9]]

                post_pdf = render_to_pdf('consolidated_report.html', params)

                msg = EmailMultiAlternatives(subject, text_content, email, [to_email])
                msg.attach('report.pdf', post_pdf, "application/pdf")
                msg.send()
                return Response(
                    {
                        'data': [],
                        'Code': HTTP_200_OK,
                        "status": "success",
                        "message":"Email Sent"
                    },
                    status=HTTP_200_OK
                )
            return Render.render("consolidated_report.html", params)

        else:
            return Response(
                {
                    'data': [],
                    'Code': HTTP_200_OK,
                    "status": "success"
                },
                status=HTTP_200_OK
            )
    except ObjectDoesNotExist:
        return "No Record Found"

@api_view(["POST"])
@permission_classes((AllowAny,))
def consolidate_report_old(request):
    print("API CALLED!!!!")
    consolidate_report_details = {}
    try:
        data = request.data
        user_id = data['user_id']
        to_date = timezone.now()
        from_date = datetime.today() - timedelta(days=(30 * 6))
        style = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
            created_date__date__range=(from_date, to_date)).aggregate(
            total_frequent_braking=Round(Sum('frequent_braking')),
            ave_frequent_braking=Round(Avg('frequent_braking')),
            max_frequent_braking=Round(Max('frequent_braking')),
            min_frequent_braking=Round(Min('frequent_braking')),
            total_distraction=Round(Sum('phone_distraction')),
            ave_distraction=Round(Avg('phone_distraction')),
            max_distraction=Round(Max('phone_distraction')),
            min_distraction=Round(Min('phone_distraction')),
            total_hard_acceleration=Round(Sum('hard_acceleration')),
            ave_hard_acceleration=Round(Avg('hard_acceleration')),
            max_hard_acceleration=Round(Max('hard_acceleration')),
            min_hard_acceleration=Round(Min('hard_acceleration')),
            total_hard_braking=Round(Sum('hard_braking')),
            ave_hard_braking=Round(Avg('hard_braking')),
            max_hard_braking=Round(Max('hard_braking')),
            min_hard_braking=Round(Min('hard_braking')),
            total_lane_change=Round(Sum('lane_change')),
            ave_lane_change=Round(Avg('lane_change')),
            max_lane_change=Round(Max('lane_change')),
            min_lane_change=Round(Min('lane_change')),
            total_speed_too_high_curve=Round(Sum('speed_too_high_curve')),
            ave_speed_too_high_curve=Round(Avg('speed_too_high_curve')),
            max_speed_too_high_curve=Round(Max('speed_too_high_curve')),
            min_speed_too_high_curve=Round(Min('speed_too_high_curve')),
            total_turns=Round(Sum('turns')),
            ave_turns=Round(Avg('turns')),
            max_turns=Round(Max('turns')),
            min_turns=Round(Min('turns')),
            total_excellent_driving=Round(Sum('excellent_driving')),
            ave_excellent_driving=Round(Avg('excellent_driving')),
            max_excellent_driving=Round(Max('excellent_driving')),
            min_excellent_driving=Round(Min('excellent_driving')),
        )
        user = Users.objects.filter(id=user_id).values()

        if (not style['ave_excellent_driving'] or not style['ave_hard_braking'] or not style['ave_turns']):
            pdf = FPDF()
            ## First page
            pdf.add_page()
            pdf.set_font('Arial', 'B', 14)
            pdf.cell(0, txt='User (' + user[0]['username'] + ') does not have any details')
            pdf.output("media/Consolidated_Reports.pdf", "F")
            time.sleep(1)
            print("pdf has been Created(without details!!!!!!!)")
            return Response(
                {
                    'Code': HTTP_200_OK,
                    "status": "success",
                    "message": "Details not Found",
                    "date": ''
                })
        else:

            pdf = FPDF()
            ## First page
            pdf.add_page()
            pdf.set_font('Arial', 'B', 14)
            pdf.cell(0, txt='Report : ' + str(timezone.now().date()))
            pdf.y = 20
            pdf.x = 10
            name = ''
            if (user[0]['first_name']):
                name = ' (' + user[0]['first_name'] + ' ' + user[0]['last_name'] + ')'
            pdf.cell(0, txt='Name : ' + user[0]['username'] + name)
            pdf.y = 30
            pdf.x = 10
            pdf.cell(0, txt='Mobile Number : ' + user[0]['mobile'])
            pdf.y = 5
            pdf.x = 150
            try:
                pdf.image(BASE_DIR + '/' + str(user[0]['avatar']), 150, 5, 35, 30)
            except Exception as e:
                pdf.image(BASE_DIR + '/images/none.jpg', 150, 5, 35, 30)

            pdf.y = 50
            pdf.x = 10
            pdf.set_fill_color(118, 146, 227)
            pdf.set_font('Arial', 'B', 16)
            pdf.cell(180, 20, txt="Driving Style", align='C', fill=True)
            pdf.set_font('Arial', 'B', 12)
            heading_list = ["Event", "Average", "Maximum", "Min"]
            values_list = [['Excellent Driving', style['ave_excellent_driving'], style['max_excellent_driving'],
                            style['min_excellent_driving']],
                           ['Frequent Braking', style['ave_frequent_braking'], style['max_frequent_braking'],
                            style['min_frequent_braking']],
                           ['Distracted driving', style['ave_distraction'], style['max_distraction'],
                            style['min_distraction']],
                           ['Hard Acceleration', style['ave_hard_acceleration'], style['max_hard_acceleration'],
                            style['min_hard_acceleration']],
                           ['Hard Braking', style['ave_hard_braking'], style['max_hard_braking'],
                            style['min_hard_braking']],
                           ['Sudden Lane change', style['ave_lane_change'], style['max_lane_change'],
                            style['min_lane_change']],
                           ['Speed High Curve', style['ave_speed_too_high_curve'], style['max_speed_too_high_curve'],
                            style['min_speed_too_high_curve']],
                           ['Sudden Turn', style['ave_turns'], style['max_turns'], style['min_turns']]]
            pdf.y = 70
            pdf.x = 10
            pdf.set_fill_color(157, 179, 242)
            pdf.set_font('Arial', 'B', 12)
            for i in range(4):
                pdf.cell(45, 10, txt=heading_list[i], align='C', border=1, fill=True)
            pdf.y = 80
            pdf.x = 10
            for i in range(8):
                for j in range(4):
                    if (j == 0):
                        pdf.set_font('Arial', 'B', 10)
                    else:
                        pdf.set_font('Arial', '', 12)
                    pdf.cell(45, 10, txt=str(values_list[i][j]), align='C', border=1, fill=True)
                pdf.y = pdf.y + 10
                pdf.x = 10

            improvement_areas = {'ave_frequent_braking': style['ave_frequent_braking'],
                                 'ave_distraction': style['ave_distraction'],
                                 'ave_hard_acceleration': style['ave_hard_acceleration'],
                                 'ave_hard_acceleration': style['ave_hard_acceleration'],
                                 'ave_hard_braking': style['ave_hard_braking'],
                                 'ave_lane_change': style['ave_lane_change'],
                                 'ave_speed_too_high_curve': style['ave_speed_too_high_curve'],
                                 'ave_turns': style['ave_turns']
                                 }
            pdf.y = 160
            pdf.x = 10
            pdf.set_fill_color(118, 146, 227)
            pdf.set_font('Arial', 'B', 16)
            pdf.cell(180, 10, txt="Key Areas to be Improved", align='C', fill=True)
            pdf.y = 170
            pdf.x = 10
            pdf.set_fill_color(157, 179, 242)
            pdf.set_font('Arial', '', 14)
            found = 0
            for k, v in improvement_areas.items():
                val = int(style[k])
                if (val > 50):
                    found = 1
                    pdf.cell(90, 10, txt=str(k).replace('ave', '').replace('_', ' ').strip().capitalize(), align='C',
                             border=1, fill=True)
                    pdf.cell(90, 10, txt=str(style[k]), align='C', border=1, fill=True)
                    pdf.y += 10
                    pdf.x = 10
            if found == 0:
                pdf.cell(180, 10, txt="Nothing to display yet", align='C', border=1, fill=True)

            ##getting total distance and duration
            total_distance = Trip.objects.filter(user_id=user_id).filter(status=1).aggregate(
                distance=Round(Sum('distance')))
            details = Trip.objects.filter(user_id=user_id).filter(status=1)
            total_duration = '00:00:00'
            for detail in details:
                if (add_duration(total_duration, detail.duration)):
                    total_duration = add_duration(total_duration, detail.duration)
            total_duration = total_duration.split(':')
            total_hours = total_duration[0] + " Hours and " + total_duration[1] + " Minutes"

            pdf.y = 230
            pdf.x = 10
            pdf.set_fill_color(118, 146, 227)
            pdf.set_font('Arial', 'B', 16)
            pdf.cell(180, 10, txt="Total Distance and Duration", align='C', fill=True)
            pdf.y = 240
            pdf.x = 10
            pdf.set_fill_color(157, 179, 242)
            pdf.set_font('Arial', '', 12)
            pdf.cell(90, 10, txt=str(total_distance['distance']) + ' Km', align='C', border=1, fill=True)
            pdf.cell(90, 10, txt=total_hours, align='C', border=1, fill=True)

            ## getting per distance and duration

            current_date = today = date.today()
            before_sixmonths_date = date.today() - relativedelta(months=+5)

            distance_stats = Trip.objects.filter(user_id=user_id, created_date__date__range=[before_sixmonths_date,
                                                                                             current_date]).annotate(
                month=TruncMonth('created_date')).values('month').annotate(
                sum=Sum('distance')).values('month', 'sum')
            month_names = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']

            pdf.y = 250
            pdf.x = 10
            pdf.set_fill_color(118, 146, 227)
            pdf.set_font('Arial', 'B', 12)
            for stat in distance_stats:
                pdf.cell(30, 10, txt=month_names[stat['month'].month - 1], align='C', border=1, fill=True)
            pdf.y = 260
            pdf.x = 10
            pdf.set_fill_color(157, 179, 242)
            pdf.set_font('Arial', '', 10)
            for stat in distance_stats:
                pdf.cell(30, 10, txt=str(stat['sum']), align='C', border=1, fill=True)

            ##graphs
            hard_braking = []
            lane_change = []
            phone_distraction = []
            hard_acceleration = []
            right_turns = []
            left_turns = []
            frequent_braking = []
            speed_too_high_curve = []
            phone_distraction = []
            accident_detection = []
            excellent_driving = []
            hard_braking = get_average_values_six_months('hard_braking', user_id)
            lane_change = get_average_values_six_months('lane_change', user_id)
            phone_distraction = get_average_values_six_months('phone_distraction', user_id)
            hard_acceleration = get_average_values_six_months('hard_acceleration', user_id)
            right_turns = get_average_values_six_months('right_turns', user_id)
            left_turns = get_average_values_six_months('left_turns', user_id)
            frequent_braking = get_average_values_six_months('frequent_braking', user_id)
            speed_too_high_curve = get_average_values_six_months('speed_too_high_curve', user_id)
            excellent_driving = get_average_values_six_months('excellent_driving', user_id)

            visualization_details = {'hard_braking': hard_braking,
                                     'sudden_lane_change': lane_change,
                                      'distracted_driving': phone_distraction,
                                     'hard_acceleration': hard_acceleration,
                                     'sudden_turns': right_turns,
                                     'frequent_braking': frequent_braking,
                                     'speed_too_high_curve': speed_too_high_curve,
                                     'excellent_driving': excellent_driving
                                     }
            for k, v in visualization_details.items():
                date_list = []
                average_list = []
                for data in v:
                    date_list.append(data['X'])
                    average_list.append(data['Y'])

                import matplotlib
                matplotlib.use('Agg')
                import matplotlib.pyplot as plt
                plt.style.use('ggplot')

                x = date_list
                y = average_list
                x_pos = [i for i, _ in enumerate(x)]
                plt.xticks(rotation=90)

                for i in range(len(date_list)):  # your number of bars
                    plt.text(x=x_pos[i] - .3,  # takes your x values as horizontal positioning argument
                             y=average_list[i],  # takes your y values as vertical positioning argument
                             s=average_list[i],  # the labels you want to add to the data
                             size=10,
                             rotation=90)

                ## deleting image
                path = 'media/' + str(k) + '_consolidated.png'
                if os.path.isfile(path):
                    os.remove(path)

                plt.bar(x_pos, y, color='#6b93f2')
                plt.xlabel(k.replace('_', ' ').upper(), color='black', size=16)
                plt.ylabel("Average")
                plt.title('')
                plt.xticks(x_pos, x, size=8)
                plt.savefig('media/' + str(k) + '_consolidated.png', dpi=(200), bbox_inches="tight")
                plt.close()

            graph_names = list(visualization_details.keys())
            pdf.add_page()
            x = 10;
            y = 5
            for i in range(len(graph_names)):
                if (i == (len(graph_names) - 1)):
                    pdf.add_page()
                    x = 35;
                    y = 5
                    pdf.image('media/' + graph_names[i] + '_consolidated.png', x, y, 120, 100)
                    break

                pdf.image('media/' + graph_names[i] + '_consolidated.png', x, y, 90, 70)
                x += 90
                if (i % 2 != 0):
                    x = 10
                    y += 70

            pdf.output("media/Consolidated_Reports.pdf", "F")
            print("pdf has been Created!!!!!!!")


    except Exception as e:
        print("Error>>>>>>", e)
    return Response(
        {
            'Code': HTTP_200_OK,
            "status": "success",
            "message": "report submitted Successfully",
            "date": consolidate_report_details
        })


@csrf_exempt
def smart_report_new(request):
    if request.method == 'POST':
        data = request.POST
        from_date = data['from_date']
        to_date = data['to_date']
        user_id = data['user_id']
        report_type = data['type']
        email_type = data['email']
        if not report_type:
            return Response(
                {
                    "Code": HTTP_400_BAD_REQUEST,
                    "status": "error",
                    "message": "Type Required"
                },
                status=HTTP_400_BAD_REQUEST
            )
        if type == 'Smart':
            if not from_date:
                return Response(
                    {
                        "Code": HTTP_400_BAD_REQUEST,
                        "status": "error",
                        "message": "Please Enter Start Date and Time"
                    },
                    status=HTTP_400_BAD_REQUEST
                )
            if not to_date:
                return Response(
                    {
                        "Code": HTTP_400_BAD_REQUEST,
                        "status": "error",
                        "message": "Please Enter To Date and Time"
                    },
                    status=HTTP_400_BAD_REQUEST
                )
            else:
                d = date.today()
                from_date = get_first_day(d)
                to_date = get_last_day(d)
        if not id:
            return Response(
                {
                    "Code": HTTP_400_BAD_REQUEST,
                    "status": "error",
                    "message": "User ID Required"
                },
                status=HTTP_400_BAD_REQUEST
            )
        try:
            user = Users.objects.filter(id=user_id).values()
            stats = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
                created_date__date__range=(from_date, to_date)).aggregate(
                speed=Round(Avg('speed_avg')),
                total_distance=Round(Sum('distance')),
                timein_traffic=Round(Max('time_in_traffic')),
                excelent_driving=Round(Avg('excellent_driving')),
                fair_driving=Round(Avg('fair_driving')),
                risky_driving=Round(Avg('risky_driving'))
            )
            if stats:
                today = timezone.now()

                ## Graph values for Excellent Driving
                date_diff = get_date_Difference(from_date, to_date)
                if not date_diff:
                    params = {
                        'today': today,
                        'users': user,
                        'record': stats,
                        'from': from_date,
                        'to': to_date,
                        'type': report_type,
                        'email_type': email_type,
                    }
                    print('params', params)

                    return Render.render('smartreport.html', params)

                if (date_diff == 'years'):
                    excellent_data = Trip.objects.filter(user_id=user_id,
                                                         created_date__date__range=[from_date, to_date]).annotate(
                        year=TruncYear('created_date')).values('year').annotate(
                        average=Avg('excellent_driving')).values('year', 'average')

                elif (date_diff == 'months'):
                    excellent_data = Trip.objects.filter(user_id=user_id,
                                                         created_date__date__range=[from_date, to_date]).annotate(
                        month=TruncMonth('created_date')).values('month').annotate(
                        average=Avg('excellent_driving')).values('month', 'average')

                elif (date_diff == 'weeks'):
                    excellent_data = Trip.objects.filter(user_id=user_id,
                                                         created_date__date__range=[from_date, to_date]).annotate(
                        week=TruncWeek('created_date')).values('week').annotate(
                        average=Avg('excellent_driving')).values('week', 'average')

                else:
                    excellent_data = Trip.objects.filter(user_id=user_id,
                                                         created_date__date__range=[from_date, to_date]).annotate(
                        day=TruncDay('created_date')).values('day').annotate(
                        average=Avg('excellent_driving')).values('day', 'average')

                import matplotlib
                matplotlib.use('Agg')
                import matplotlib.pyplot as plt
                date_list = []
                average_list = []
                for data in excellent_data:
                    if ('year' in str(excellent_data[0])):
                        date_list.append(str(data['year']).replace('00:00:00', '').replace('-01', ''))
                        average_list.append(round(data['average'], 2))
                    if ('week' in str(excellent_data[0])):
                        date_list.append(str(data['week']).replace('00:00:00', '').replace('-01', ''))
                        average_list.append(round(data['average'], 2))
                    if ('month' in str(excellent_data[0])):
                        date_list.append(str(data['month']).replace('00:00:00', '').replace('-01', ''))
                        average_list.append(round(data['average'], 2))
                    if ('day' in str(excellent_data[0])):
                        date_list.append(str(data['day']).replace('00:00:00', '').replace('-01', ''))
                        average_list.append(round(data['average'], 2))

                plt.style.use('ggplot')
                x = date_list
                y = average_list
                x_pos = [i for i, _ in enumerate(x)]
                plt.xticks(rotation=90)

                for i in range(len(date_list)):  # your number of bars
                    plt.text(x=x_pos[i] - .3,  # takes your x values as horizontal positioning argument
                             y=average_list[i],  # takes your y values as vertical positioning argument
                             s=average_list[i],  # the labels you want to add to the data
                             size=10,
                             rotation=90)

                attr = "excellent_driving"
                ## deleting image
                path = 'media/' + attr + 'smart_report' + '.png'
                if os.path.isfile(path):
                    os.remove(path)
                plt.bar(x_pos, y, color='#6b93f2')
                plt.xlabel(attr.replace('_', ' ').upper(), color='black', size=16)
                plt.ylabel("Average")
                plt.title('')
                plt.xticks(x_pos, x, size=8)
                image_ticks = str(time.time()).replace('.', '')
                plt.savefig('media/' + attr + 'smart_report' + '.png', dpi=(200), bbox_inches="tight")
                plt.close()

                params = {
                    'today': today,
                    'users': user,
                    'record': stats,
                    'from': from_date,
                    'to': to_date,
                    'type': report_type,
                    'email_type': email_type,
                    'graph_image': BASE_DIR + '/media/' + attr + 'smart_report' + '.png'
                }
                print('params', params)

                return Render.render('smartreport.html', params)

                return Response(
                    {
                        'Code': HTTP_200_OK,
                        "status": "success",
                        'data': params,

                    })
            else:
                return Response(
                    {
                        'data': [],
                        'Code': HTTP_200_OK,
                        "status": "success"
                    },
                    status=HTTP_200_OK
                )
        except ObjectDoesNotExist:
            return "No Record Found"


def calculate_driving_score_new(acc_x, acc_y, acc_z, gyro_y, gyro_x):
    try:
        test = [[acc_x, acc_y, acc_z, gyro_y, gyro_x]]

        #label = model_new.predict(test)
        # print('Predicted', label)
        return label
    except FileNotFoundError:
        return 'File Not found'



BASE_DIR = os.path.dirname(os.path.dirname(__file__))


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def smart_report(request):
    print("Api called!")
    # if request.method == 'POST':
    #     data = request.POST
    #     from_date = data['from_date']
    #     to_date = data['to_date']
    #     user_id = data['user_id']
    #     report_type = data['type']
    #     email_type = data['email']

    from_date = request.data.get("from_date", "")
    to_date = request.data.get("to_date", "")
    user_id = request.data.get("user_id", "")
    report_type = request.data.get("type", "")
    email_type = request.data.get("email", "")
    print (from_date, to_date,user_id,report_type,email_type)
    if not report_type:
        print("not report type")
        return Response(
            {
                "Code": HTTP_400_BAD_REQUEST,
                "status": "error",
                "message": "Type Required"
            },
            status=HTTP_400_BAD_REQUEST
            )
    if report_type == 'Smart':
        print("smart")
        if not from_date:
            return Response(
                {
                    "Code": HTTP_400_BAD_REQUEST,
                    "status": "error",
                    "message": "Please Enter Start Date and Time"
                },
                status=HTTP_400_BAD_REQUEST
            )
        if not to_date:
            return Response(
                {
                    "Code": HTTP_400_BAD_REQUEST,
                    "status": "error",
                    "message": "Please Enter To Date and Time"
                },
                status=HTTP_400_BAD_REQUEST
            )
        else:
            d = date.today()
            # from_date = get_first_day(d)
            # to_date = get_last_day(d)
        if not id:
            return Response(
                {
                    "Code": HTTP_400_BAD_REQUEST,
                    "status": "error",
                    "message": "User ID Required"
                },
                status=HTTP_400_BAD_REQUEST
            )
        try:
            print("inside try")
            user = Users.objects.filter(id=user_id).values()
            stats = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
                created_date__date__range=(from_date, to_date)).aggregate(
                speed=Round(Avg('speed_avg')),
                total_distance=Round(Sum('distance')),
                timein_traffic=Round(Max('time_in_traffic')),
                excelent_driving=Round(Avg('excellent_driving')),
                fair_driving=Round(Avg('fair_driving')),
                risky_driving=Round(Avg('risky_driving'))
            )

#
            if stats:
                trip_ids = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
                    created_date__date__range=(from_date, to_date)).values_list('id', flat=True)
                morning = 0
                night = 0
                total_count = 0
                for trip_id in trip_ids:
                    # increase the count of the every of the variables
                    variable = Trip.objects.filter(id = trip_id).values_list('time_in_traffic', flat=True)

                    if variable == 'Early Morning':
                        morning += 1
                    elif variable == 'Morning':
                        morning += 1
                    elif variable == 'Afternoon':
                        morning += 1
                    elif variable == 'Evening':
                        morning += 1
                    else:
                        night += 1
                    total_count += 1
                # get the percentage of each of the variable
                morning_percentage = (morning * 100) / total_count
                night_percentage = (night * 100) / total_count

                stats['timein_traffic'] = {"Day": int(morning_percentage), "Night": int(night_percentage)}
                today = timezone.now()

                ## Graph values for Excellent Driving
                date_diff = get_date_Difference(from_date, to_date)
                if not date_diff:
                    params = {
                        'today': today,
                        'users': user,
                        'record': stats,
                        'from': from_date,
                        'to': to_date,
                        'type': report_type,
                        'email_type': email_type,
                    }
                    #print('params', params)
                    #return Response({"data": params})
                    return Render.render('smartreport.html', params)

                if (date_diff == 'years'):
                    excellent_data = Trip.objects.filter(user_id=user_id,
                                                         created_date__date__range=[from_date, to_date]).annotate(
                        year=TruncYear('created_date')).values('year').annotate(
                        average=Avg('excellent_driving')).values('year', 'average')

                elif (date_diff == 'months'):
                    excellent_data = Trip.objects.filter(user_id=user_id,
                                                         created_date__date__range=[from_date, to_date]).annotate(
                        month=TruncMonth('created_date')).values('month').annotate(
                        average=Avg('excellent_driving')).values('month', 'average')

                elif (date_diff == 'weeks'):
                    excellent_data = Trip.objects.filter(user_id=user_id,
                                                         created_date__date__range=[from_date, to_date]).annotate(
                        week=TruncWeek('created_date')).values('week').annotate(
                        average=Avg('excellent_driving')).values('week', 'average')

                else:
                    excellent_data = Trip.objects.filter(user_id=user_id,
                                                         created_date__date__range=[from_date, to_date]).annotate(
                        day=TruncDay('created_date')).values('day').annotate(
                        average=Avg('excellent_driving')).values('day', 'average')

                import matplotlib
                matplotlib.use('Agg')
                import matplotlib.pyplot as plt
                date_list = []
                average_list = []
                for data in excellent_data:
                    if ('year' in str(excellent_data[0])):
                        date_list.append(str(data['year']).replace('00:00:00', '').replace('-01', ''))
                        average_list.append(round(data['average'], 2))
                    if ('week' in str(excellent_data[0])):
                        date_list.append(str(data['week']).replace('00:00:00', '').replace('-01', ''))
                        average_list.append(round(data['average'], 2))
                    if ('month' in str(excellent_data[0])):
                        date_list.append(str(data['month']).replace('00:00:00', '').replace('-01', ''))
                        average_list.append(round(data['average'], 2))
                    if ('day' in str(excellent_data[0])):
                        date_list.append(str(data['day']).replace('00:00:00', '').replace('-01', ''))
                        average_list.append(round(data['average'], 2))

                plt.style.use('ggplot')
                x = date_list
                y = average_list
                x_pos = [i for i, _ in enumerate(x)]
                plt.xticks(rotation=90)

                for i in range(len(date_list)):  # your number of bars
                    plt.text(x=x_pos[i] - .3,  # takes your x values as horizontal positioning argument
                             y=average_list[i],  # takes your y values as vertical positioning argument
                             s=average_list[i],  # the labels you want to add to the data
                             size=10,
                             rotation=90)

                attr = "excellent_driving"
                ## deleting image
                path = 'media/' + attr + 'smart_report' + '.png'
                if os.path.isfile(path):
                    os.remove(path)
                plt.bar(x_pos, y, color='#6b93f2')
                plt.xlabel(attr.replace('_', ' ').upper(), color='black', size=16)
                plt.ylabel("Average")
                plt.title('')
                plt.xticks(x_pos, x, size=8)
                image_ticks = str(time.time()).replace('.', '')
                plt.savefig('media/' + attr + 'smart_report' + '.png', dpi=(200), bbox_inches="tight")
                plt.close()

                params = {
                    'today': today,
                    'users': user,
                    'record': stats,
                    'from': from_date,
                    'to': to_date,
                    'type': report_type,
                    'email_type': email_type,
                    'graph_image': BASE_DIR + '/media/' + attr + 'smart_report' + '.png'
                }
                #print('params', params)

                return Render.render('smartreport.html', params)

                return Response(
                    {
                        'Code': HTTP_200_OK,
                        "status": "success",
                        'data': params,

                    })
            else:
                return Response(
                    {
                        'data': [],
                        'Code': HTTP_200_OK,
                        "status": "success"
                    },
                    status=HTTP_200_OK
                )
        except ObjectDoesNotExist:
            return "No Record Found"

def add_duration(time1 , time2):
    time3 = [0,0,0]
    try:
        time1 = time1.split(':')
        time2 = time2.split(':')
        time3[2] = int(time1[2]) + int(time2[2])
        if time3[2] > 59:
            time3[1] = time3[2] // 60
            time3[1] = time3[2] // 60
            time3[2] = time3[2] % 60
        time3[1] = int(time3[1]) + int(time1[1]) + int(time2[1])
        if time3[1] > 59:
            time3[0] =  time3[1] // 60
            time3[1] =  time3[1] % 60
        time3[0] = time3[0] + int(time1[0]) + int(time2[0])
        return(str(time3[0])+':'+str(time3[1])+':'+str(time3[2]))
    except Exception as e:
        return 0

def get_average_values_six_months(field, user_id, to_date, from_date):
    value_list=[]
    current_date = datetime.today()
    previous_date = date.today() - relativedelta(months=+5)
    trips = Trip.objects.filter(user_id=user_id, created_date__date__range= [from_date, to_date]).filter(usertype='Driver').annotate(
            month=TruncMonth('created_date')).values('month').annotate(
            average=Avg(field)).values('month', 'average')
    for trip in trips:
        value_dict={'X': str(trip['month'].month) + '/' + str(trip['month'].year) , 'Y': round(trip['average'], 1) }
        value_list.append(value_dict)
    return value_list


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def monthly_report(request):
    if request.method == 'POST':
        print('ok')
        data = request.POST

        #from_date = data['from_date']
        from_date = request.POST.get('from_date', False)
        print(from_date)
        #to_date = data['to_date']
        to_date = request.POST.get('to_date', False)
        #user_id = data['user_id']
        user_id =  request.POST.get('user_id', False)
        #report_type = data['type']
        report_type = request.POST.get('type', False)
        #email_type = data['email']
        email_type = request.POST.get('email', False)
        if to_date == from_date:
            return Response(
                {
                    "Code": HTTP_400_BAD_REQUEST,
                    "status": "error",
                    "message": "Please select a Month Slot"
                },
                status=HTTP_400_BAD_REQUEST
            )

        if not report_type:
            return Response(
                {
                    "Code": HTTP_400_BAD_REQUEST,
                    "status": "error",
                    "message": "Type Required"
                },
                status=HTTP_400_BAD_REQUEST
            )
        if report_type == 'Smart':
            if not from_date:
                return Response(
                    {
                        "Code": HTTP_400_BAD_REQUEST,
                        "status": "error",
                        "message": "Please Enter Start Date and Time"
                    },
                    status=HTTP_400_BAD_REQUEST
                )
            if not to_date:
                return Response(
                    {
                        "Code": HTTP_400_BAD_REQUEST,
                        "status": "error",
                        "message": "Please Enter To Date and Time"
                    },
                    status=HTTP_400_BAD_REQUEST
                )
            else:
                d = date.today()
                # from_date = get_first_day(d)
                # to_date = get_last_day(d)
        if not id:
            return Response(
                {
                    "Code": HTTP_400_BAD_REQUEST,
                    "status": "error",
                    "message": "User ID Required"
                },
                status=HTTP_400_BAD_REQUEST
            )
        try:
            user = Users.objects.filter(id=user_id).values()
            stats = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
                created_date__date__range=(from_date, to_date)).aggregate(
                speed=Round(Avg('speed_avg')),
                total_distance=Round(Sum('distance')),
                timein_traffic=Round(Max('time_in_traffic')),
                excelent_driving=Round(Avg('excellent_driving')),
                fair_driving=Round(Avg('fair_driving')),
                risky_driving=Round(Avg('risky_driving'))
            )
            if stats:
                today = timezone.now()

                ## Graph values for Excellent Driving
                date_diff = get_date_Difference(from_date, to_date)
                if not date_diff:
                    params = {
                        'today': today,
                        'users': user,
                        'record': stats,
                        'from': from_date,
                        'to': to_date,
                        'type': report_type,
                        'email_type': email_type,
                    }
                    print('params', params)

                    return Render.render('monthlyreport.html', params)

                excellent_data = Trip.objects.filter(user_id=user_id,
                                                     created_date__date__range=[from_date, to_date]).annotate(
                    month=TruncMonth('created_date')).values('month').annotate(
                    average=Avg('excellent_driving')).values('month', 'average')

                monthly_distance = Trip.objects.filter(user_id=user_id,
                                                       created_date__date__range=[from_date, to_date]).annotate(
                    month=TruncMonth('created_date')).values('month').annotate(
                    sum=Sum('distance')).values('month', 'sum')
                # print("monthly_distance>>>>>>>>>>>>>>",monthly_distance)
                month_names = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov',
                               'Dec']
                for data in monthly_distance:
                    data['month'] = month_names[data['month'].month - 1]

                    ## Creating graph images
                hard_braking = []
                lane_change = []
                phone_distraction = []
                hard_acceleration = []
                right_turns = []
                left_turns = []
                frequent_braking = []
                speed_too_high_curve = []
                phone_distraction = []
                accident_detection = []
                excellent_driving = []
                hard_braking = get_average_values_monthwise('hard_braking', user_id)
                lane_change = get_average_values_monthwise('lane_change', user_id)
                phone_distraction = get_average_values_monthwise('phone_distraction', user_id)
                hard_acceleration = get_average_values_monthwise('hard_acceleration', user_id)
                right_turns = get_average_values_monthwise('right_turns', user_id)
                left_turns = get_average_values_monthwise('left_turns', user_id)
                frequent_braking = get_average_values_monthwise('frequent_braking', user_id)
                speed_too_high_curve = get_average_values_monthwise('speed_too_high_curve', user_id)
                excellent_driving = get_average_values_monthwise('excellent_driving', user_id)

                visualization_details = {'hard_braking': hard_braking,
                                         'lane_change': lane_change,
                                         'phone_distraction': phone_distraction,
                                         'hard_acceleration': hard_acceleration,
                                         'right_turns': right_turns,
                                         'left_turns': left_turns,
                                         'frequent_braking': frequent_braking,
                                         'speed_too_high_curve': speed_too_high_curve,
                                         'excellent_driving': excellent_driving
                                         }
                monthly_graph_images = []
                for k, v in visualization_details.items():

                    ## deleting files
                    path = 'media/monthly_' + str(k) + '.png'
                    if os.path.isfile(path):
                        os.remove(path)
                    date_list = []
                    average_list = []
                    for data in v:
                        date_list.append(data['X'])
                        average_list.append(data['Y'])

                    import matplotlib
                    matplotlib.use('Agg')
                    import matplotlib.pyplot as plt
                    plt.style.use('ggplot')

                    x = date_list
                    y = average_list
                    x_pos = [i for i, _ in enumerate(x)]
                    plt.xticks(rotation=90)

                    for i in range(len(date_list)):  # your number of bars
                        plt.text(x=x_pos[i] - .3,  # takes your x values as horizontal positioning argument
                                 y=average_list[i],  # takes your y values as vertical positioning argument
                                 s=average_list[i],  # the labels you want to add to the data
                                 size=10,
                                 rotation=90)

                    plt.bar(x_pos, y, color='#6b93f2')
                    plt.xlabel(k.replace('_', ' ').upper(), color='black', size=16)
                    plt.ylabel("Average")
                    plt.title('')
                    plt.xticks(x_pos, x, size=8)
                    plt.savefig('media/monthly_' + str(k) + '.png', dpi=(200), bbox_inches="tight")
                    monthly_graph_images.append(BASE_DIR + '/media/monthly_' + str(k) + '.png')
                    plt.close()

                params = {
                    'today': today,
                    'users': user,
                    'record': stats,
                    'from': from_date,
                    'to': to_date,
                    'type': report_type,
                    'email_type': email_type,
                    'monthly_distance': monthly_distance,
                    'monthly_graph_images': monthly_graph_images

                }
                return Render.render('monthlyreport.html', params)

                return Response(
                    {
                        'Code': HTTP_200_OK,
                        "status": "success",
                        'data': params,

                    })
            else:
                return Response(
                    {
                        'data': [],
                        'Code': HTTP_200_OK,
                        "status": "success"
                    },
                    status=HTTP_200_OK
                )
        except ObjectDoesNotExist:
            return "No Record Found"

@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def trip_day_night(request):
    user_id = request.data.get('user_id')
    start_date = request.data.get('start_date')
    end_date = request.data.get('end_date')

    time_in_traffic = Trip.objects.filter(user_id = user_id).filter(created_date__date__range=(start_date, end_date)).values_list('time_in_traffic', flat=True)

    total_count = 0
    day_count = 0
    night_count = 0

    for day_night in time_in_traffic:
        print(day_night)
        if(day_night == 'Day'):
            day_count += 1
        elif(day_night == 'Night'):
            night_count += 1
        total_count += 1

    day_ratio = (day_count/total_count)*100
    night_ratio = (night_count/total_count)*100

    print(total_count)
    print(day_ratio)
    print(night_ratio)

    return Response({"data":"Collected"})


@api_view(["GET"])
@permission_classes((AllowAny,))
def change_total_time_to_day_night(request, trip_id):
    #make it the group_by function
    # total_time = Sensordata.objects.filter(trip_id = trip_id, type = 'Moving').filter(speed__lt = 4).annotate(Count('timeInMillis', distinct=True))
    # print("TOTAL TIME")
    # print(total_time[0].timeInMillis__count)
    # return str(timedelta(seconds=total_time[0].timeInMillis__count))
    #get the trip start time and check out the Day and night
    trip_ids  = Trip.objects.filter(user_id = trip_id).values_list('id', flat=True)
    for ids in trip_ids:
        start_time = Trip.objects.filter(id = ids).values_list('trip_start_time', flat=True)
        print(start_time[0])
        print(type(start_time[0]))

        check_time = "20:00:00"
        check_time2 = "06:00:00"

        trip_datetime = datetime.strptime(datetime.strftime(start_time[0],"%Y-%m-%d"), "%Y-%m-%d")
        trip_datetime_check = datetime.strptime(check_time, "%H:%M:%S")
        trip_datetime_check_2 = datetime.strptime(check_time2, "%H:%M:%S")

        # I am supposing that the date must be the same as now
        start_time2 = start_time[0].replace(hour=start_time[0].time().hour, minute=start_time[0].time().minute,
                                  second=start_time[0].time().second, microsecond=0)
        trip_datetime2 = trip_datetime.replace(hour=trip_datetime_check.time().hour, minute=trip_datetime_check.time().minute,
                                  second=trip_datetime_check.time().second, microsecond=0)
        trip_datetime3 = trip_datetime.replace(hour=trip_datetime_check_2.time().hour, minute=trip_datetime_check_2.time().minute,
                                  second=trip_datetime_check_2.time().second, microsecond=0)

        if (start_time2 > trip_datetime2 or start_time2 < trip_datetime3):
            Trip.objects.filter(id=ids).update(time_in_traffic='Night')
        else:
            Trip.objects.filter(id=ids).update(time_in_traffic='Day')
    return Response({"data":"data change"})


@csrf_exempt
@api_view(["POST"])
def create_trip_user_flag_one(request):
    # trip_start_time = datetime.datetime.strptime(request.data.get("trip_start_time", ""), '%Y-%m-%d %H:%M:%S')
    trip_time = request.data.get("trip_start_time", "")
    trip_end_time = request.data.get("trip_end_time", "")
    print('trip_time', trip_time)
    # trip_start_time = trip_time / 1000.0
    # trip_start_end =  (trip_time - 300000 ) / 1000.0
    user_id = request.data.get("user_id", "")
    usertype = request.data.get("usertype", "")
    app_version_name = request.data.get("app_version_name", "")
    device_id = request.data.get("device_id", "")
    mobile_model = request.data.get("mobile_model", "")
    mobile_version = request.data.get("mobile_version", "")

    distance = request.data.get("distance", 0)
    speed_max = request.data.get("speed_max", 0)
    speed_avg = request.data.get("speed_avg", 0)
    speed_min = request.data.get("speed_min", 0)
    excellent_driving = request.data.get("excellent_driving", 0)
    risky_driving = request.data.get("risky_driving", 0)
    road_type = request.data.get("road_type", 'Highway')
    road_slope = request.data.get("road_slope", 0)
    acceleration_max = request.data.get("acceleration_max", 0)
    acceleration_avg = request.data.get("acceleration_avg", 0)
    phone_distraction = request.data.get("phone_distraction", 0)
    hard_braking = request.data.get("hard_braking", 0)
    over_speeding = request.data.get("over_speeding", 0)
    hard_acceleration = request.data.get("hard_acceleration", 0)
    acceleration = request.data.get("acceleration", 0)
    lane_change = request.data.get("lane_change", 0)
    turns =request.data.get("turns", 0)
    duration =request.data.get("duration", 0)
    time_in_traffic =request.data.get("time_in_traffic", 0)
    speed_limit = request.data.get("speed_limit", 0)
    right_turns = request.data.get("right_turns", 0)
    left_turns = request.data.get("left_turns", 0)
    frequent_braking = request.data.get("frequent_braking", 0)
    speed_too_high_curve = request.data.get("speed_too_high_curve", 0)
    has_user_changed = request.data.get("has_user_changed", False)
    event_acc = request.data.get("event_acc", False)
    event_turn = request.data.get("event_turn", False)
    event_braking = request.data.get("event_braking", False)
    event_other = request.data.get("event_other", False)
    events_acc_hard = request.data.get("events_acc_hard", False)
    events_freq_breaking = request.data.get("events_freq_breaking", False)
    events_excesive_breaking = request.data.get("events_excesive_breaking", False)
    event_sudden_lane_change = request.data.get("event_sudden_lane_change", False)
    event_sudden_turn = request.data.get("event_sudden_turn", False)
    event_lane_change = request.data.get("event_lane_change", False)


    TripCreated = Trip.objects.create(
        trip_start_time=datetime.fromtimestamp(float(trip_time) / 1000.0).strftime('%Y-%m-%d %H:%M:%S.%f'),
        trip_end_time=datetime.fromtimestamp(float(trip_end_time) / 1000.0).strftime('%Y-%m-%d %H:%M:%S.%f'),
        user_id=user_id,
        usertype=usertype,
        app_version_name=app_version_name,
        device_id=device_id,
        mobile_model=mobile_model,
        mobile_version=mobile_version,
        flag=1,
        finished=1,
        status=1,
        distance = distance,
        speed_max = speed_max,
        speed_avg = speed_avg,
        speed_min = speed_min,
        excellent_driving = excellent_driving,
        risky_driving = risky_driving,
        road_slope= road_slope,
        road_type = road_type,
        acceleration_max = acceleration_max,
        acceleration_avg = acceleration_avg,
        phone_distraction = phone_distraction,
        hard_braking = hard_braking,
        over_speeding = over_speeding,
        hard_acceleration = hard_acceleration,
        acceleration =  acceleration,
        lane_change = lane_change,
        turns = turns,
        duration = duration,
        time_in_traffic = time_in_traffic,
        speed_limit = speed_limit,
        right_turns = right_turns,
        left_turns = left_turns,
        frequent_braking = frequent_braking,
        speed_too_high_curve = speed_too_high_curve,
        has_user_changed = has_user_changed,
        #is_confirmed = is_confirmed,
        event_acc = event_acc,
        event_turn = event_turn,
        event_braking = event_braking,
        events_acc_hard = events_acc_hard,
        events_freq_breaking = events_freq_breaking,
        events_excesive_breaking = events_excesive_breaking,
        event_sudden_lane_change = event_sudden_lane_change,
        event_sudden_turn = event_sudden_turn,
        event_other = event_other,
        event_lane_change = event_lane_change


    )

    TripID = TripCreated.id
    # else:
    #    tripList = Sensordata.objects.filter(user_id=user_id).filter(~Q(trip_id=0)).filter(timeInMillis__range=( (trip_time - 300000)  , trip_time )).order_by('-timeInMillis').distinct().values('trip_id')
    #    print(tripList)
    # for trips in tripList:
    #      print(trips['trip_id'])
    #    TripID = tripList[0]['trip_id']


    return Response(
        {
            'data': TripID,
            'Code': HTTP_200_OK,
            "status": "success"
        },
        status=HTTP_200_OK
    )

    tripList = Trip.objects.filter(user_id=user_id).filter(finished=0).order_by('id').count()

    if tripList > 0:
        tripList = Trip.objects.filter(user_id=user_id).filter(finished=0).order_by('-id').values('id',
                                                                                                  'trip_start_time')
        TripID = tripList[0]['id']
        # sensorCount = Sensordata.objects.filter(user_id=user_id).filter(trip_id=TripID).order_by('id').values('timeInMillis').count()
        sensorData = Sensordata.objects.filter(user_id=user_id).filter(trip_id=TripID).filter(type='Moving').order_by('id').values(
            'timeInMillis')

        # print('ddddddddddddd sensorCount',sensorData)

        if len(sensorData) == 0:

            print('No Sensor Data')

            trip_stated_time = time.mktime(
                datetime.strptime(tripList[0]['trip_start_time'].strftime('%Y-%m-%d %H:%M:%S.%f'),
                                  '%Y-%m-%d %H:%M:%S.%f').timetuple())
            print('trip_stated_time')
            print(trip_stated_time)
            # convert milliseconds date and trip date to a specific format
            fmt = '%Y-%m-%d %H:%M:%S'
            d1 = datetime.fromtimestamp(int(trip_time) / 1000).strftime('%Y-%m-%d %H:%M:%S')
            d2 = datetime.fromtimestamp(trip_stated_time).strftime('%Y-%m-%d %H:%M:%S')
            print('d1')
            print(d1)
            print('d2')
            print(d2)

            # convert date to time and calculate minutes
            date_format = "%Y-%m-%d %H:%M:%S"
            time1 = datetime.strptime(d1, date_format)
            time2 = datetime.strptime(d2, date_format)
            print('time1')
            print(time1)
            print('time2')
            print(time2)

            diff = time1 - time2
            diff_minutes = int(diff.seconds / 60)
            print('diff_minutes')
            print(diff_minutes)

            if time2 >= time1:
                TripID = tripList[0]['id']
            elif diff_minutes <= 5:
                TripID = tripList[0]['id']
            else:
                print('end time function over here')
                print(datetime.fromtimestamp(float(trip_stated_time) / 1000.0).strftime('%Y-%m-%d %H:%M:%S.%f'))
                Trip.objects.filter(user_id=user_id).filter(id=tripList[0]['id']).filter(finished=0).update(
                    finished=1,
                    trip_end_time=d2,
                )


                TripCreated = Trip.objects.create(
                    trip_start_time=d1,
                    user_id=user_id,
                    usertype=usertype,
                    app_version_name=app_version_name,
                    device_id=device_id,
                    mobile_model=mobile_model,
                    mobile_version=mobile_version,
                    flag = 1
                )

                TripID = TripCreated.id


        else:
            sensorList = Sensordata.objects.filter(user_id=user_id).filter(trip_id=TripID).filter(type='Moving').order_by('-id').values(
                'timeInMillis')
            sensorTime = int(sensorList[0]['timeInMillis']) / 1000  # 12:10

            print('Sensor Data found')

            # convert milliseconds date and trip date to a specific format
            fmt = '%Y-%m-%d %H:%M:%S'
            d1 = datetime.fromtimestamp(int(trip_time) / 1000).strftime('%Y-%m-%d %H:%M:%S')
            d2 = datetime.fromtimestamp(sensorTime).strftime('%Y-%m-%d %H:%M:%S')
            print('d1 is time sent by mobile')
            print(d1)
            print('d2 is last sensor record ')
            print(d2)

            # convert date to time and calculate minutes
            date_format = "%Y-%m-%d %H:%M:%S"
            time1 = datetime.strptime(d1, date_format)
            time2 = datetime.strptime(d2, date_format)
            print('time1 is time sent by mobile')
            print(time1)
            print('time2 is last sensor record')
            print(time2)

            diff = time1 - time2
            diff_minutes = int(diff.seconds / 60)
            print('diff_minutes')
            print(diff_minutes)

            # print('ddddddddddddd',trip_time , int(sensorTime), int(trip_time) - int(sensorTime))

            # if sensorTime  > time.mktime(datetime.strptime( tripList[0]['created_date'].strftime('%Y-%m-%d %H:%M:%S.%f'), '%Y-%m-%d %H:%M:%S.%f').timetuple()) :

            if time2 >= time1:
                TripID = tripList[0]['id']
            elif diff_minutes <= 5:
                TripID = tripList[0]['id']
            else:
                TripID = tripList[0]['id']
                Trip.objects.filter(user_id=user_id).filter(id=tripList[0]['id']).filter(finished=0).update(
                    finished=1,
                    trip_end_time=d2
                )

                Sensordata.objects.filter(trip_id=0).filter(user_id=user_id).update(trip_id=TripID)

                TripCreated = Trip.objects.create(
                    trip_start_time=d1,
                    user_id=user_id,
                    usertype=usertype,
                    app_version_name=app_version_name,
                    device_id=device_id,
                    mobile_model=mobile_model,
                    mobile_version=mobile_version,
                    flag = 1
                )

                TripID = TripCreated.id


    else:
        # sensorList = Sensordata.objects.filter(user_id=user_id).filter(~Q(trip_id=0)).filter(timeInMillis__range=( (trip_time - 300000)  , trip_time )).order_by('id').count()
        # print('sensorList',sensorList)
        # if sensorList == 0:
        # print('sssssssssssssssss',trip_time)
        TripCreated = Trip.objects.create(
            trip_start_time=datetime.fromtimestamp(float(trip_time) / 1000.0).strftime('%Y-%m-%d %H:%M:%S.%f'),
            user_id=user_id,
            usertype=usertype,
            app_version_name=app_version_name,
            device_id=device_id,
            mobile_model=mobile_model,
            mobile_version=mobile_version,
            flag = 1
        )

        TripID = TripCreated.id
        # else:
        #    tripList = Sensordata.objects.filter(user_id=user_id).filter(~Q(trip_id=0)).filter(timeInMillis__range=( (trip_time - 300000)  , trip_time )).order_by('-timeInMillis').distinct().values('trip_id')
        #    print(tripList)
        # for trips in tripList:
        #      print(trips['trip_id'])
        #    TripID = tripList[0]['trip_id']

    return Response(
        {
            'data': TripID,
            'Code': HTTP_200_OK,
            "status": "success"
        },
        status=HTTP_200_OK
    )

def driving_style_data(user_id, from_date, to_date):
    totals = Trip.objects.filter(user_id=user_id).filter(status=1).filter(
        created_date__date__range=(from_date, to_date)).aggregate(
        hard_acceleration=Round(Sum('hard_acceleration')),
        turns=Round(Sum('turns')),
        frequent_braking=Round(Sum('frequent_braking')),
        right_turns=Round(Sum('right_turns')),
        left_turns=Round(Sum('left_turns')),
        acceleration=Round(Sum('acceleration')),
        lane_change=Round(Sum('lane_change')),
        speed_too_high_curve=Round(Sum('speed_too_high_curve'))
    )

    #get these values for per 100km
# @csrf_exempt
# @api_view(["GET"])
def calculate_dss(trip_id):
    sensor_records = Sensordata.objects.filter(trip_id= trip_id).order_by('-id').values()
    # trip_traffic = Trip.objects.filter(id=trip_id).values_list('time_in_traffic', flat=True)
    # print("triptraffic{}".format(trip_traffic[0]))
    # scores = DssWeight.objects.filter(name=trip_traffic[0]).values()
    # print("score {}".format(scores))
    # print(scores[0]['weight_acc'])
    # return Response({"data":"df"})
    print("sensor_records")
    print(len(sensor_records))
    list_acc = []
    list_braking = []
    list_lane_change = []
    list_turn = []
    list_other = []
    simple_list_turn = []
    simple_list_acc = []
    simple_list_brake = []
    simple_list_lane_change = []
    list_freq_brake = []
    list_left_turn = []
    list_right_turn = []


    event_acc = 0
    event_braking = 0
    event_lane_change = 0
    event_turn = 0
    event_other = 0
    event_simple_acc = 0
    event_simple_brake = 0
    event_simple_lane_change = 0
    event_freq_brake = 0
    event_simple_turn = 0
    event_left_turn = 0
    event_right_turn = 0

    percentage_score_acc = 0
    percentage_score_turn = 0
    percentage_score_braking = 0
    percentage_score_lane_change = 0
    percentage_score_other = 0

    accident_label = 0
    #make the loop of the sensor data to get the timings and the records of the sensor data
    for sensor_record in sensor_records:
        label = calculate_driving_score(sensor_record['acc_x'], sensor_record['acc_y'], sensor_record['acc_z'], sensor_record['gyro_x'], sensor_record['gyro_y'], sensor_record['gyro_z'])
        if label == 'Sudden Turns':
            short_list_sudden_turn = []
            short_list_sudden_turn.append(sensor_record['id'])
            short_list_sudden_turn.append(sensor_record['created_at'])
            list_turn.append(short_list_sudden_turn)


        if label == 'Excessive Break':
            short_list_excessive = []
            short_list_excessive.append(sensor_record['id'])
            short_list_excessive.append(sensor_record['created_at'])
            list_braking.append(short_list_excessive)

        if label == 'Aggressive Left Turn':
            short_list_aggressive_left = []
            short_list_aggressive_left.append(sensor_record['id'])
            short_list_aggressive_left.append(sensor_record['created_at'])
            list_left_turn.append(short_list_aggressive_left)

        if label == 'Aggressive Right Turn':
            short_list_aggressive_right = []
            short_list_aggressive_right.append(sensor_record['id'])
            short_list_aggressive_right.append(sensor_record['created_at'])
            list_right_turn.append(short_list_aggressive_right)

        if label == 'Aggressive Acceleration':
            short_list_agg_acc = []
            short_list_agg_acc.append(sensor_record['id'])
            short_list_agg_acc.append(sensor_record['created_at'])
            list_acc.append(short_list_agg_acc)

        if label == 'Frequent Braking':
            short_list_freq = []
            short_list_freq.append(sensor_record['id'])
            short_list_freq.append(sensor_record['created_at'])
            list_freq_brake.append(short_list_freq)

        if label == 'Lane Change':
            short_list_lane_change = []
            short_list_lane_change.append(sensor_record['id'])
            short_list_lane_change.append(sensor_record['created_at'])
            simple_list_lane_change.append(short_list_lane_change)

        if label == 'Sudden Lane Change':
            short_list_sudden_lane_change = []
            short_list_sudden_lane_change.append(sensor_record['id'])
            short_list_sudden_lane_change.append(sensor_record['created_at'])
            list_lane_change.append(short_list_sudden_lane_change)

        if label == 'Distracted Driving':
            short_list_distracted =  []
            short_list_distracted.append(sensor_record['id'])
            short_list_distracted.append(sensor_record['created_at'])
            list_other.append(short_list_distracted)

        if label == 'Accident':
            accident_label += 1
        if label == 'Acceleration':
            short_list_acc = []
            short_list_acc.append(sensor_record['id'])
            short_list_acc.append(sensor_record['created_at'])
            simple_list_acc.append(short_list_acc)

        if label == 'Turns':
            short_list_turn = []
            short_list_turn.append(sensor_record['id'])
            short_list_turn.append(sensor_record['created_at'])
            simple_list_turn.append(short_list_turn)

        if label == 'Break':
            short_list_brake = []
            short_list_brake.append(sensor_record['id'])
            short_list_brake.append(sensor_record['created_at'])
            simple_list_brake.append(short_list_brake)

    #-----kurtosis value braking ----
    event_braking = get_event_kurtosis(simple_list_brake, "braking")
    avg_braking = get_acc_value_mean(event_braking['list'])
    kurtosis_value_braking = calculate_kurtosis_value(avg_braking)

    #---kurtosis value acceleration----
    event_acc = get_event_kurtosis(simple_list_acc, "agg acc")
    avg_acc_list = get_acc_value_mean(event_acc['list'])
    kurtosis_value = calculate_kurtosis_value(avg_acc_list)

    #--kurtosis value turns ---------
    event_turn = get_event_kurtosis(simple_list_turn, "turnss")
    avg_event_turn = get_turns_value_mean(event_turn['list'])
    kurtosis_value_turn = calculate_kurtosis_value(avg_event_turn)

    #---kurtosis value lane change ---
    event_lane_change = get_event_kurtosis(simple_list_lane_change, "Sudden Lane")
    avg_event_lane_change = get_lane_change_value_mean((event_lane_change['list']))
    kurtosis_value_lane_change = calculate_kurtosis_value((avg_event_lane_change))

    #--kurtosis value other --
    event_other = get_event_kurtosis(list_other, "other")
    avg_event_other = get_distracted_value_mean(event_other['list'])
    kurtosis_value_other = calculate_kurtosis_value(avg_event_other)

    return [kurtosis_value, kurtosis_value_braking, kurtosis_value_turn, kurtosis_value_lane_change, kurtosis_value_other]




def get_event(timings):
    #pass an array of timings and make the events of that particular
    event = 0
    print("timings")
    print(len(timings))
    k=0

    if(len(timings)>0):
        event = 1
        for i in range(0, len(timings)):
            # get  the difference between consequtive timings
            time_difference = timings[k]-timings[i]
            seconds = int(time_difference.seconds)
            if seconds >= 20:
                event += 1
                k=i


    return event


def get_event_sudden(timings):
    #pass an array of timings and make the events of that particular
    event = 0
    print("timings")
    print(len(timings))
    k=0

    if(len(timings)>0):
        event = 1
        for i in range(0, len(timings)):
            # get  the difference between consequtive timings
            time_difference = timings[k]-timings[i]
            seconds = int(time_difference.seconds)
            if seconds >= 10:
                event += 1
                k=i


    return event




def get_event_kurtosis(timings, event_data):
    #pass an array of timings and make the events of that particular
    print("EVENT{}".format(event_data))
    event = 0
    print("Event kurtosis length")
    print(len(timings))
    k=0
    list_ids = []
    list_short_id = []
    if(len(timings)>0):
        event = 1
        for i in range(0, len(timings)):
            # get  the difference between consequtive timings
            list_short_id.append(timings[i][0])
            #print("list short id")
            #print(list_short_id)
            time_difference = timings[k][1]-timings[i][1]
            seconds = int(time_difference.seconds)
            if seconds >= 30:
                event += 1
                k=i
                list_ids.append(list_short_id)
                list_short_id = []
    #check out if the list is empty or not
    #this is to check out if the
    if (len(list_ids)==0 and len(list_short_id)>0):
        print("int the list")
        list_ids.append(list_short_id)
    return {"event":event, "list":list_ids}

@csrf_exempt
@api_view(["GET"])
def get_weights(request):
    print("Weight api")

    dss_data = {}
    #data_event = DssWeight.objects.filter(name = event).values()
    dss_weight = DssWeight.objects.values_list('name', flat=True)
    print(dss_weight)
    for name in dss_weight:
        name_data = DssWeight.objects.filter(name = name).values().first()
        dss_data[name] = name_data

    print(dss_data)
    return Response(
        {
            'data': dss_data,
            'Code': HTTP_200_OK,
            "status": "success"
        },
        status=HTTP_200_OK
    )

@csrf_exempt
@api_view(["PUT"])
@permission_classes((AllowAny,))
def update_weights(request, weight_id):
    #update the weights of the data
    weight_acc = request.data.get('weight_acc')
    weight_turn = request.data.get('weight_turn')
    weight_brake = request.data.get('weight_braking')
    weight_lane_change = request.data.get('weight_lane_change')
    weight_other = request.data.get('weight_other')

    dss_weight = DssWeight.objects.filter(id = weight_id).update(weight_acc = weight_acc,
                                                                 weight_turn = weight_turn,
                                                                 weight_braking= weight_brake,
                                                                 weight_lane_change= weight_lane_change,
                                                                 weight_other = weight_other)
    return Response({"data": dss_weight,
                     "code":HTTP_200_OK,
                      "message":"Success"})


def get_acc_value_mean(list_data):
    #access the list
    magnitude_list = []
    print("magnitude list")
    print(len(list_data))
    for data in list_data:
        result = Sensordata.objects.filter(id__in=data).aggregate(Avg('acc_magnitude'))
        magnitude_list.append(result['acc_magnitude__avg'])

    return magnitude_list

def get_turns_value_mean(list_data):
    # access the list
    magnitude_list = []
    print("magnitude list")
    print(len(list_data))
    for data in list_data:
        result = Sensordata.objects.filter(id__in=data).aggregate(Avg('gyro_z'))
        magnitude_list.append(result['gyro_z__avg'])

    return magnitude_list

def get_lane_change_value_mean(list_data):
    magnitude_list = []
    print("magnitude list")
    print(len(list_data))
    for data in list_data:
        result = Sensordata.objects.filter(id__in=data).aggregate(Avg('gyro_y'))
        magnitude_list.append(result['gyro_y__avg'])

    return magnitude_list

def get_distracted_value_mean(list_data):
    magnitude_list = []
    print("magnitude list")
    print(len(list_data))
    for data in list_data:
        result = Sensordata.objects.filter(id__in=data).aggregate(Avg('gyro_x'))
        magnitude_list.append(result['gyro_x__avg'])

    return magnitude_list


def calculate_kurtosis_value(list):
    #mean of the list

    mean = np.mean(list)
    kurtosis_value = 0
    total_upper = 0
    total_lower = 0
    list_length = len(list)
    # make the loop to get the kurtosis value
    for value in list:
        total_upper += (value - mean)**4
        total_lower += (value - mean) ** 2  # get the kurtosis value for the each trip

    if total_lower==0:
        return 0

    kurtosis_value = list_length * (total_upper / (total_lower) ** 2)
    return kurtosis_value

# @csrf_exempt
# @api_view(["GET"])
def count_overspeeding(sensors_records):
    total_overspeeding = []
    i = 0
    score = 100
    for trip_data in sensors_records:
        if float(trip_data['speed']) > 22:
            total_overspeeding.append(trip_data['created_at'])

    return total_overspeeding

def get_over_speeding_event(timings):
    # pass an array of timings and make the events of that particular
    event = 0
    print("timings")
    print(len(timings))
    k = 0

    if (len(timings) > 0):
        event = 1
        for i in range(0, len(timings)):
            # get  the difference between consequtive timings
            time_difference = timings[k] - timings[i]
            seconds = int(time_difference.seconds)
            if seconds >= 10:
                event += 1
                k = i

    return event

# @csrf_exempt
# @api_view(["POST"])
def user_driving_style(from_date, to_date, user_id):
    # from_date = request.data.get("from_date", "")
    # to_date = request.data.get("to_date", "")
    # user_id = request.data.get("user_id", "")


    #get the total number of events between the selected time period
    total_event = Trip.objects.filter(user_id=user_id).filter(status=1).filter(usertype='Driver').filter(
        created_date__date__range=(from_date, to_date)).aggregate(
        distracted_driving=Round(Sum('event_other')),
        hard_acceleration=Round(Sum('events_acc_hard')),
        hard_braking=Round(Sum('events_excesive_breaking')),
        sudden_turn=Round(Sum('event_sudden_turn')),
        frequent_braking=Round(Sum('events_freq_breaking')),
        lane_change=Round(Sum('event_sudden_lane_change')),
        total_distance=Round(Sum('distance')),
        score_distracted=Round(Avg('phone_distraction')),
        score_hard_acc=Round(Avg('hard_acceleration')),
        score_hard_braking=Round(Avg('hard_braking')),
        score_sudden_turn=Round(Avg('turns')),
        score_frequent_braking=Round(Avg('frequent_braking')),
        score_lane_change=Round(Avg('lane_change')),


    )

    header_array = ['Event','Distracted driving', 'Hard acceleration', 'Hard braking', 'Sudden Turn', 'Frequent braking', 'Sudden Lane Change']
    total_array = ['Error Count',round(total_event['distracted_driving'],1), round(total_event['hard_acceleration'],1), round(total_event['hard_braking'],1), round(total_event['sudden_turn'],1),round(total_event['frequent_braking'],1), round(total_event['lane_change'],1)]
    score_array = ['Score',round(total_event['score_distracted'],1), round(total_event['score_hard_acc'],1), round(total_event['score_hard_braking'],1), round( total_event['score_sudden_turn'],1), round( total_event['score_frequent_braking'],1), round(total_event['score_lane_change'],1)]
    #check out the total distance
    distracted_driving_hundred = total_event['distracted_driving']/100
    hard_acceleration_hundred = total_event['hard_acceleration']/100
    hard_braking_hundred = total_event['hard_braking']/100
    sudden_turn_hundred = total_event['sudden_turn']/100
    frequent_braking_hundred = total_event['frequent_braking']/100
    lane_change_hundred = total_event['lane_change']/100

    hundred = []
    hundred = ['Per 100 Km', round(distracted_driving_hundred,1), round(hard_acceleration_hundred,1), round(hard_braking_hundred,1), round(sudden_turn_hundred,1), round(frequent_braking_hundred,1), round(lane_change_hundred,1)]

    if type(from_date).__name__=='str':
        date_time_obj_1 = datetime.strptime(from_date, '%Y-%m-%d')
        date_time_obj_2 = datetime.strptime(to_date, '%Y-%m-%d')
    else:
        date_time_obj_1= from_date
        date_time_obj_2 = to_date

    diff = date_time_obj_2 - date_time_obj_1
    days, seconds = diff.days, diff.seconds

    hours = days * 24 + seconds

    #get the events according to the hours
    distracted_driving_hour = total_event['distracted_driving'] / hours
    hard_acceleration_hour = total_event['hard_acceleration'] / hours
    hard_braking_hour = total_event['hard_braking'] / hours
    sudden_turn_hour = total_event['sudden_turn'] / hours
    frequent_braking_hour = total_event['frequent_braking'] / hours
    lane_change_hour = total_event['lane_change'] / hours


    hour = []
    hour = ['Per hour driving', round(distracted_driving_hour,1), round(hard_acceleration_hour, 1), round(hard_braking_hour,1), round(sudden_turn_hour,1), round(frequent_braking_hour,1), round(lane_change_hour,1)]


    event_table_array  = [total_array, hundred, hour,score_array]
    #return Response({"data":result_array})
    #get the event count of for the time event chart
    total_event_emorning = Trip.objects.filter(user_id=user_id, time_in_traffic='Early Morn').filter(status=1,usertype='Driver').filter(
        created_date__date__range=(from_date, to_date)).aggregate(
        hard_acceleration=Round(Sum('events_acc_hard')),
        hard_braking=Round(Max('events_excesive_breaking')),
        sudden_turn=Round(Avg('event_sudden_turn')),
        turn=Round(Avg('event_turn')),
        frequent_braking=Round(Avg('events_freq_breaking')),
        lane_change=Round(Avg('event_sudden_lane_change')),


    )


    total_event_morning = Trip.objects.filter(user_id=user_id, time_in_traffic='Morning').filter(status=1, usertype='Driver').filter(
        created_date__date__range=(from_date, to_date)).aggregate(
        hard_acceleration=Round(Sum('events_acc_hard')),
        hard_braking=Round(Max('events_excesive_breaking')),
        sudden_turn=Round(Avg('event_sudden_turn')),
        turn=Round(Avg('event_turn')),
        frequent_braking=Round(Avg('events_freq_breaking')),
        lane_change=Round(Avg('event_sudden_lane_change')),
        speed_to_high=Round(Avg('event_other'))

    )

    total_event_after = Trip.objects.filter(user_id=user_id, time_in_traffic='Afternoon').filter(status=1, usertype='Driver').filter(
        created_date__date__range=(from_date, to_date)).aggregate(
        hard_acceleration=Round(Sum('events_acc_hard')),
        hard_braking=Round(Max('events_excesive_breaking')),
        sudden_turn=Round(Avg('event_sudden_turn')),
        turn=Round(Avg('event_turn')),
        frequent_braking=Round(Avg('events_freq_breaking')),
        lane_change=Round(Avg('event_sudden_lane_change')),
        speed_to_high=Round(Avg('event_other'))

    )

    total_event_eve = Trip.objects.filter(user_id=user_id, time_in_traffic='Evening').filter(status=1,usertype='Driver').filter(
        created_date__date__range=(from_date, to_date)).aggregate(
        hard_acceleration=Round(Sum('events_acc_hard')),
        hard_braking=Round(Max('events_excesive_breaking')),
        sudden_turn=Round(Avg('event_sudden_turn')),
        turn=Round(Avg('event_turn')),
        frequent_braking=Round(Avg('events_freq_breaking')),
        lane_change=Round(Avg('event_sudden_lane_change')),


    )

    total_event_night = Trip.objects.filter(user_id=user_id, time_in_traffic='Night').filter(status=1,usertype='Driver').filter(
        created_date__date__range=(from_date, to_date)).aggregate(
        hard_acceleration=Round(Sum('events_acc_hard')),
        hard_braking=Round(Max('events_excesive_breaking')),
        sudden_turn=Round(Avg('event_sudden_turn')),
        turn=Round(Avg('event_turn')),
        frequent_braking=Round(Avg('events_freq_breaking')),
        lane_change=Round(Avg('event_sudden_lane_change')),


    )


    early_morning = 0
    morning  = 0
    afternoon =0
    evening = 0
    night = 0
    if total_event_emorning['hard_acceleration']:
        early_morning = float(total_event_emorning['hard_acceleration']) + float(total_event_emorning['hard_braking']) + float(total_event_emorning['sudden_turn']) + float(total_event_emorning['turn']) + float(total_event_emorning['frequent_braking']) + float(total_event_emorning['lane_change'])
    if total_event_morning['hard_acceleration']:
        morning = float(total_event_morning['hard_acceleration']) + float(total_event_morning['hard_braking']) + float(total_event_morning['sudden_turn']) + float(total_event_morning['turn']) + float(total_event_morning['frequent_braking'])+ float(total_event_morning['lane_change'])
    if total_event_after['hard_acceleration']:
        afternoon = float(total_event_after['hard_acceleration']) + float(total_event_after['hard_braking']) + float(total_event_after['sudden_turn']) + float(total_event_after['turn']) + float(total_event_after['frequent_braking'])+ float(total_event_after['lane_change'])
    if total_event_eve['hard_acceleration']:
        evening = float(total_event_eve['hard_acceleration']) + float(total_event_eve['hard_braking']) + float(total_event_eve['sudden_turn']) + float(total_event_eve['turn']) + float(total_event_eve['frequent_braking'])+ float(total_event_eve['lane_change'])
    if total_event_night['hard_acceleration']:
        night = float(total_event_night['hard_acceleration']) + float(total_event_night['hard_braking']) + float(total_event_night['sudden_turn']) + float(total_event_night['turn']) + float(total_event_night['frequent_braking'])+ float(total_event_night['lane_change'])

    day_list = []
    day_name_list = ['Early Morning', 'Morning', 'Afternoon', 'Evening', 'Night']
    day_list = [round(early_morning,1), round(morning,1), round(afternoon, 1), round(evening, 1), round(night, 1)]

    ha=get_records_hard_acc(total_event_emorning['hard_acceleration'], total_event_morning['hard_acceleration'],total_event_after['hard_acceleration'], total_event_eve['hard_acceleration'], total_event_night['hard_acceleration'])
    hb=get_records_hard_braking(total_event_emorning['hard_braking'], total_event_morning['hard_braking'],total_event_after['hard_braking'], total_event_eve['hard_braking'], total_event_night['hard_braking'])
    st=get_records_sudden_turn(total_event_emorning['sudden_turn'], total_event_morning['sudden_turn'],total_event_after['sudden_turn'], total_event_eve['sudden_turn'], total_event_night['sudden_turn'])
    sl=get_records_sudden_lane(total_event_emorning['lane_change'], total_event_morning['lane_change'],total_event_after['lane_change'], total_event_eve['lane_change'], total_event_night['lane_change'])
    fb=get_records_frequent(total_event_emorning['frequent_braking'], total_event_morning['frequent_braking'],total_event_after['frequent_braking'], total_event_eve['frequent_braking'], total_event_night['frequent_braking'])


    result_api = [header_array, event_table_array, day_name_list, day_list,  list((ha, hb, st, sl, fb))]
    return result_api

def get_records_hard_acc(a1, a2, a3, a4, a5):
    return list((a1, a2, a3, a4, a5))

def get_records_hard_braking(a1, a2, a3, a4, a5):
    return list((a1, a2, a3, a4, a5))

def get_records_sudden_turn(a1, a2, a3, a4, a5):
    return list((a1, a2, a3, a4, a5))

def get_records_sudden_lane(a1, a2, a3, a4, a5):
    return list((a1, a2, a3, a4, a5))

def get_records_frequent(a1, a2, a3, a4, a5):
    return list((a1, a2, a3, a4, a5))


def get_frequent_braking(braking_evnet, acceleration_event):
    score_frequent_braking = 0
    #get the frequent braking score

    division_score = braking_evnet/acceleration_event

    if division_score <= 0.6:
        score_frequent_braking = 100
        return score_frequent_braking
    elif 0.6<division_score <= 0.8:
        score_frequent_braking = 90
        return score_frequent_braking
    elif 0.8 < division_score < 0.9:
        score_frequent_braking = 80
        return score_frequent_braking
    elif 0.9<=division_score < 1.0:
        score_frequent_braking = 70
        return score_frequent_braking
    elif 1.0<=division_score < 1.1:
        score_frequent_braking = 60
        return score_frequent_braking
    elif 1.1<=division_score < 1.2:
        score_frequent_braking = 50
        return score_frequent_braking
    elif 1.2<=division_score < 1.3:
        score_frequent_braking = 40
        return score_frequent_braking
    elif 1.3<=division_score < 1.4:
        score_frequent_braking = 30
        return score_frequent_braking
    elif 1.4<=division_score < 1.5:
        score_frequent_braking = 20
        return score_frequent_braking
    elif 1.5<division_score :
        score_frequent_braking = 0
        return score_frequent_braking

    return score_frequent_braking

@csrf_exempt
@api_view(["GET"])
def get_absolute_skill_score(request, user_id):

    #get all trip id of the user
    trip_id = Trip.objects.filter(user_id=user_id).values_list('id',flat=True).order_by('-id')[:2]
    print(trip_id)
    list_kurtosis_acc = []
    list_kurtosis_braking = []
    list_kurtosis_turn = []
    list_kurtosis_lane_change = []
    list_kurtosis_other = []

    for id in trip_id:
        kurtosis_values = calculate_dss(id)
        list_kurtosis_acc.append(kurtosis_values[0])
        list_kurtosis_braking.append(kurtosis_values[1])
        list_kurtosis_turn.append(kurtosis_values[2])
        list_kurtosis_lane_change.append(kurtosis_values[3])
        list_kurtosis_other.append(kurtosis_values[4])





    absolute_skill_score = 0
    if np.std(list_kurtosis_acc)!=0:
        absolute_skill_score = 1/np.std(list_kurtosis_acc)
    if np.std(list_kurtosis_braking)!=0:
        absolute_skill_score+=(1/np.std(list_kurtosis_braking))
    if np.std(list_kurtosis_lane_change)!=0:
        absolute_skill_score+=(1/np.std(list_kurtosis_lane_change))
    if np.std(list_kurtosis_turn)!=0:
        absolute_skill_score+=(1/np.std(list_kurtosis_turn))
    if np.std(list_kurtosis_other)!=0:
        absolute_skill_score+=(1/np.std(list_kurtosis_other))


    #absolute_skill_score = (1/np.std(list_kurtosis_acc))+(1/np.std(list_kurtosis_braking))+(1/np.std(list_kurtosis_lane_change))+(1/np.std(list_kurtosis_turn))+(1/np.std(list_kurtosis_other))
    absolute_agression_score = np.mean(list_kurtosis_acc) + np.mean(list_kurtosis_braking) +np.mean(list_kurtosis_lane_change) +np.mean(list_kurtosis_turn)+np.mean(list_kurtosis_other)

    #return Response({"data":round(absolute_skill_score, 2)})
    print("absolute score")
    print(absolute_agression_score)
    print(absolute_skill_score)
    #---------update the absolute skill score------
    update_score = Users.objects.filter(id= user_id).update(absolute_skill_score=round(absolute_skill_score, 2))
    print("updated_data_score")
    update_score_data = Users.objects.filter(id=user_id).update(absolute_agression_score=round(absolute_agression_score,2))
    return Response({"data":[absolute_skill_score, absolute_agression_score]})


def get_relative_score(user_id):
    #get absolute skill score of all driver
    ass_user = Users.objects.filter(id= user_id).values_list('absolute_skill_score', flat=True)
    ass_all = Users.objects.aggregate(Avg('absolute_skill_score'))
    ass_std = np.std(Users.objects.values_list('absolute_skill_score', flat=True))


    rss = 0
    if ass_std!=0:
        rss = (ass_user[0]-ass_all['absolute_skill_score__avg'])/ass_std

    aas_user = Users.objects.filter(id= user_id).values_list('absolute_agression_score', flat=True)
    aas_all = Users.objects.filter(id=user_id).aggregate(Avg('absolute_agression_score'))
    aas_std = np.std(Users.objects.values_list('absolute_agression_score', flat=True))


    ras = 0
    if aas_std!=0:
        ras = (aas_user[0]-aas_all['absolute_agression_score__avg'])/aas_std

    return [round(rss,1), round(ras,1)]


#check out the gyroscope value
def get_event_without_gyro_sudden_turn(request):
    #check out the bearing value and the rate of change of the value
    i =0
    # i = 0
    k = 1
    j = 0

    total_values = 0
    total_records = 0
    #get the bearing data as dictonary with timeinmillis as key and bearing value as a angle
    for value in bearing_array:
         previous_time = int((value[i]['timeinmillis']/1000)%60)
         second_time = int((value[k]['timieinmillis']/1000)%60)

         from_bearing_angle = value[j]['bearing']
         destination_bearing_angle = value[j+1]['bearing']

         #check out the difference in consequtive time
         time_difference = second_time - previous_time

         #get the average value
         bearing_difference = abs(float(destination_bearing_angle)-float(from_bearing_angle))
         total_values+=bearing_difference


         if time_difference.seconds>=4:
             i+=1
             k=i+1
             j = i+1
             #take the average of the values
             angle_average = total_values/k
             if angle_average >= 22.5:
                 total_records+=1
         k+=1
         j+=1

    return Response({"data":total_records})

#@api_view(["GET"])
def get_record_turn(data_sensor):
    #get the data from the Sensor data
    #data_sensor = Sensordata.objects.filter(trip_id= trip_id).values()

    #set turn count to zero
    turn_count = 0
    sudden_turn_count = 0
    lane_change = 0
    sudden_lane_change = 0
    left_turn = 0
    right_turn = 0
    speed_too_high=0

    print(data_sensor[0]['bearing'])
    for i in range(0, len(data_sensor)):
        if i>=len(data_sensor)-1:
            break
        previous_value = data_sensor[i]['bearing']
        next_value = data_sensor[i+1]['bearing']

        #check the difference of previous value and next value
        difference = abs(float(next_value)-float(previous_value))
        if 90 > difference >= 45:
            if previous_value > next_value:
                left_turn += 1
                turn_count+=1
            else:
                right_turn += 1
                turn_count+=1
        elif difference>=90:
            sudden_turn_count+=1
        elif 22 > difference >= 15:
            lane_change+=1
        elif 30>difference>=22:
            sudden_lane_change+=1
        elif 30<difference<=45:
            speed_too_high+=1
    print("here in the without gyro part function")
    print({"turn":turn_count, "sudden_turn":sudden_turn_count, "lane_change":lane_change, "sudden_lane":sudden_lane_change, "left_turn":left_turn, "right_turn":right_turn})
    return {"turn":turn_count, "sudden_turn":sudden_turn_count,"speed_to_high":speed_too_high, "lane_change":lane_change, "sudden_lane":sudden_lane_change, "left_turn":left_turn, "right_turn":right_turn}







#@api_view(["GET"])
def send_message(user_id, lat, long):
    #get the avg of the altitude
    #send the message to the users emergency number
    e_number = Users.objects.filter(id = user_id).values_list('e_number', flat=True)
    #user_number = Users.objects.filter(id = user_id).values_list('mobile', flat= True)

    # Your Account SID from twilio.com/console
    account_sid = "AC739ca1da97c99801912ef484bda7c12d"
    # Your Auth Token from twilio.com/console
    auth_token = "549067f916496382dd354cb6b7f5becb"
    location = "http://maps.google.com/maps?q={},{}".format(lat, long)
    client = Client(account_sid, auth_token)

    #replace +91 from the number
    replaced_number = str(e_number).replace("+91", "", 1)
    print("replaced_number")
    print(replaced_number)
    message = client.messages.create(
        to="+91{}".format(replaced_number),
        from_="+14243224345",
        body="Emergency!Please help me there is an accident. My location is {}".format(location))

    print(message.sid)
    return Response({"data":"send"})

def get_score_data(trip_id):
    dss_data = {}
    # data_event = DssWeight.objects.filter(name = event).values()
    dss_weight = DssWeight.objects.values_list('name', flat=True)
    # print(dss_weight)
    for name in dss_weight:
        name_data = DssWeight.objects.filter(name=name).values().first()
        dss_data[name] = name_data

    stats = Trip.objects.filter(id=trip_id).filter(status=1).values()
    stats_data = stats[0]

    dss_new = dss_data['Early Morning']
    # check out the trip timing also
    if stats_data['time_in_traffic'] == 'Morning':
        dss_new = dss_data['Morning']
    elif stats_data['time_in_traffic'] == 'Afternoon':
        dss_new = dss_data['Afternoon']
    elif stats_data['time_in_traffic'] == 'Evening':
        dss_new = dss_data['Evening']
    else:
        dss_new = dss_data['Night']


    excellent_total=0
    fair_total=0
    risky_total=0

    if 100 >= stats_data['hard_acceleration'] >= 80:
        excellent_total = dss_new['weight_acc']
    elif 80 > stats_data['hard_acceleration'] >= 50:
        fair_total = dss_new['weight_acc']
    else:
        risky_total = dss_new['weight_acc']

    if 100 >= stats_data['hard_braking'] >= 80:
        excellent_total += dss_new['weight_braking']
    elif 80 > stats_data['hard_braking'] >= 50:
        fair_total += dss_new['weight_braking']
    else:
        risky_total += dss_new['weight_braking']

    if 100 >= stats_data['turns'] >= 80:
        excellent_total += dss_new['weight_turn']
    elif 80 > stats_data['turns'] >= 50:
        fair_total += dss_new['weight_turn']
    else:
        risky_total += dss_new['weight_turn']

    if 100 >= stats_data['lane_change'] >= 80:
        excellent_total += dss_new['weight_lane_change']
    elif 80 > stats_data['lane_change'] >= 50:
        fair_total += dss_new['weight_lane_change']
    else:
        risky_total += dss_new['weight_lane_change']

    if 100 >= stats_data['phone_distraction'] >= 80:
        excellent_total += dss_new['weight_other']
    elif 80 > stats_data['phone_distraction'] >= 50:
        fair_total += dss_new['weight_other']
    else:
        risky_total += dss_new['weight_other']

    driving_score = excellent_total * 100

    return driving_score