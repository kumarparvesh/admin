from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_200_OK
)
from .models import Social
from rest_framework.response import Response
from django.core.exceptions import (
    ValidationError,
)


@csrf_exempt
@api_view(["POST"])
def send_invitation(request):
    user_id = request.data.get("user_id", "")
    mobile_number = request.data.get("mobile_number", "")
    if not mobile_number:
        return Response(
            {'Code': HTTP_400_BAD_REQUEST, "status": "error", "message": "Please Send the Valid Number"},
            status=HTTP_400_BAD_REQUEST)
    if not user_id:
        return Response(
            {'Code': HTTP_400_BAD_REQUEST, "status": "error", "message": "Please Send the Valid User Id"},
            status=HTTP_400_BAD_REQUEST)
    try:
        new_circle = Social.objects.create(
            user_id=user_id,
            invitation=mobile_number
        )
        return Response(
            {
                "Code": HTTP_200_OK,
                "status": "success",
                "message": "Invitation Send Successfully",
                "contact": mobile_number
            }
        )
    except ValidationError as e:
        return Response(data=str(e), status=HTTP_400_BAD_REQUEST)

