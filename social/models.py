from django.db import models
from users.models import User


# Create Trips Models
class Social(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True)
    invitation = models.CharField(max_length=10, blank=True, default='0')
    status = models.CharField(max_length=10, blank=True, default='pending')
