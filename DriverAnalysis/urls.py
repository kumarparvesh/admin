from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from rest_framework import routers
from users import views
from companies import views as companyViews
from trip import views as tripView
from sensordata import views as sensor
from digi import views as digiMap
from social import views as socialViews
from rest_framework_swagger.views import get_swagger_view
from django.conf.urls.static import static
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt

schema_view = get_swagger_view(title='Driver Analysis')

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)

urlpatterns = [
    url(r'^$', companyViews.index),
    path('', companyViews.index),
    path('api/', schema_view),
    path('admin/', admin.site.urls),
    path('api/login', views.login),
    path('api/signup', views.signup),
    path('api/invite', views.invite),
    path('api/companies', companyViews.index),
    path('api/trip/smart-report', tripView.smart_report),
    path('api/trip/admin-report', tripView.admin_report),
    path('api/trip/ml', tripView.manually_calculate),
    #path('api/trip/ml-deleted', tripView.manually_calculate_deleted),
    path('api/trip/calculate-distance', tripView.calculate_distance),
    path('api/trip/latest-trip', tripView.latest_trip_driving_stats),
    path('api/trip/trips-particular-date', tripView.trips_of_a_particular_date),
    path('api/trip/daily-trip-record', tripView.daily_trip_record),
    path('api/trip/daily-trip-record', tripView.daily_trip_record),
    path('api/trip/monthly-chart', tripView.monthly_chart),
    path('api/trip/trip_details', tripView.trip_details),
    path('api/trip/trip-detail', tripView.trip_detail),
    path('api/trip/trip_more_details', tripView.trip_more_details),
    path('api/trip/driving_style_analysis', tripView.driving_style_analysis),
    path('api/sensordata/heartbeat', sensor.heart_beat),
    path('api/social/send-invitation', socialViews.send_invitation),
    path('api/friend', views.circle_list),
    path('api/user-list', views.user_list),
    path('api/emergency-details', views.emergency_details),
    path('api/get-emergency-details', views.get_emergency_details),
    path('api/update-profile', views.update_profile),
    path('api/trip/pattern-analysis', tripView.pattern_analysis),
    path('api/change-password', views.change_password),
    path('api/get-user-details', views.get_user_details),
    path('api/get-search-user-details', views.get_search_user_details),
    path('api/digi/add', digiMap.add_new),
    path('api/digi/get', csrf_exempt(digiMap.get_attributes_distance)),
    path('api/trip/calculate', tripView.calculate_ranks),
    path('api/trip/get-all-trips', tripView.get_all_trips),
    path('api/reset', views.reset),
    path('api/reset-token', views.reset_token),
    path('api/trip/trip-map-view', tripView.trip_more_details_web),
    path('api/trip/awareness-report', tripView.awareness_report),
    path('api/trip/driver-passenger-detection', tripView.driver_detection),
    path('api/trip/update-trip-user', tripView.update_trip_user),
    path('api/trip/create-trip-user', tripView.create_trip_user),
    path('api/trip/create-trip-user-flag-one', csrf_exempt(tripView.create_trip_user_flag_one)),
    path('api/trip/close-trip-user', tripView.close_trip_user),
    path('api/trip/close-trip', tripView.close_trip),
    path('api/trip/close-trip-user-test', tripView.close_trip_user_test),
    path('api/trip/unmoving-trip',  tripView.distracted_driving),
    path('api/trip/set-confirmed/<int:pk>', tripView.set_user_confirmed),
    path('api/trip/trip_visualization',tripView.trip_visualization),
    path('api/trip/count-distraction/<int:trip_id>', tripView.check_distracted_data),
    path('api/trip/trip_visualization', tripView.trip_visualization),
    path('api/trip/consolidate_report', csrf_exempt(tripView.consolidate_report)),
    # path('api/trip/smart-report-new', tripView.smart_report_new),
    path('api/trip/monthly-report', tripView.monthly_report),
    path('api/trip/day-time/', tripView.trip_day_night),
    path('api/trip/calculate-dss/<int:user_id>', tripView.get_absolute_skill_score),
    path('api/trip/day-time-change/<int:trip_id>', tripView.total_time_in_traffic),
    path('api/trip/day-time-change/<int:trip_id>', tripView.change_total_time_to_day_night),
    path('api/trip/dss-weight/', tripView.get_weights),
    path('api/trip/dss-weight/<int:weight_id>', csrf_exempt(tripView.update_weights)),
    path('api/trip/count-overspeeding/<int:trip_id>', tripView.count_overspeeding),
    path('api/trip/user-driving-style-test/', tripView.user_driving_style),
    path('api/trip/delete-user/<int:pk>', views.delete_user),
    path('api/trip/trip_count/<int:trip_id>', tripView.get_record_turn),
    path('api/trip/check-acc/<int:user_id>', tripView.get_relative_score),




]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
